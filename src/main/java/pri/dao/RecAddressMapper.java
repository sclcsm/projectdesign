package pri.dao;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import pri.pojo.RecAddress;
import pri.pojo.RecAddressExample;

public interface RecAddressMapper {
    long countByExample(RecAddressExample example);

    int deleteByExample(RecAddressExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(RecAddress record);

    int insertSelective(RecAddress record);

    List<RecAddress> selectByExample(RecAddressExample example);

    RecAddress selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") RecAddress record, @Param("example") RecAddressExample example);

    int updateByExample(@Param("record") RecAddress record, @Param("example") RecAddressExample example);

    int updateByPrimaryKeySelective(RecAddress record);

    int updateByPrimaryKey(RecAddress record);
}