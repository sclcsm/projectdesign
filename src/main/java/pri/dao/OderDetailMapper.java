package pri.dao;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import pri.pojo.OderDetail;
import pri.pojo.OderDetailExample;

public interface OderDetailMapper {
    long countByExample(OderDetailExample example);

    int deleteByExample(OderDetailExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(OderDetail record);

    int insertSelective(OderDetail record);

    List<OderDetail> selectByExample(OderDetailExample example);

    OderDetail selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") OderDetail record, @Param("example") OderDetailExample example);

    int updateByExample(@Param("record") OderDetail record, @Param("example") OderDetailExample example);

    int updateByPrimaryKeySelective(OderDetail record);

    int updateByPrimaryKey(OderDetail record);
}