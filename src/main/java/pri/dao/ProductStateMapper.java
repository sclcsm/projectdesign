package pri.dao;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import pri.pojo.ProductState;
import pri.pojo.ProductStateExample;

public interface ProductStateMapper {
    long countByExample(ProductStateExample example);

    int deleteByExample(ProductStateExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(ProductState record);

    int insertSelective(ProductState record);

    List<ProductState> selectByExample(ProductStateExample example);

    ProductState selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") ProductState record, @Param("example") ProductStateExample example);

    int updateByExample(@Param("record") ProductState record, @Param("example") ProductStateExample example);

    int updateByPrimaryKeySelective(ProductState record);

    int updateByPrimaryKey(ProductState record);
}