package pri.dao;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import pri.pojo.ExpressCom;
import pri.pojo.ExpressComExample;

public interface ExpressComMapper {
    long countByExample(ExpressComExample example);

    int deleteByExample(ExpressComExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(ExpressCom record);

    int insertSelective(ExpressCom record);

    List<ExpressCom> selectByExample(ExpressComExample example);

    ExpressCom selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") ExpressCom record, @Param("example") ExpressComExample example);

    int updateByExample(@Param("record") ExpressCom record, @Param("example") ExpressComExample example);

    int updateByPrimaryKeySelective(ExpressCom record);

    int updateByPrimaryKey(ExpressCom record);
}