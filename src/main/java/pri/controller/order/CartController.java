package pri.controller.order;

import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import pri.pojo.Cart;
import pri.pojo.CartInfo;
import pri.pojo.User;
import pri.service.order.CartService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;


@Controller
@RequestMapping("/cart")
public class CartController {
    @Autowired
    private CartService cs;

    @RequestMapping("/list")
    public String showCart(Model model){
        //获取到ServletRequestAttributes 里面有 RequestContextHolder
        ServletRequestAttributes attrs = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        //获取到Request对象
        HttpServletRequest request = attrs.getRequest();
        //获取到Session对象
        HttpSession session = request.getSession();
        //获取到Response对象
        HttpServletResponse response = attrs.getResponse();
        List<Cart> c_list;
        List<CartInfo> c_info_list;

//        User test = new User();
//        test.setUserId("1515");
//        session.setAttribute("user",test);
        //进入购物车页面后先验证是否登录
        User user = (User)session.getAttribute("user");
        if (user==null){
            //未登录 购物车数据从session获取
//            List<Cart> test = new ArrayList<Cart>();
//            Cart c = new Cart();
//            c.setCount(10);
//            c.setProductId("14");
//            test.add(c);
//            session.setAttribute("cart",test);
            c_list = (List<Cart>) session.getAttribute("cart");
            System.out.println(c_list);


        }else {
            //登录就从数据库获取
            c_list = cs.getCartList(user.getUserId());
        }

        //如果用户没有购物车数据
        if (c_list==null){
            model.addAttribute("total",0);
            return "front/order/cart";
        }

        c_info_list = cs.getCartInfoByList(c_list);
        session.setAttribute("cartlist",c_info_list);
        model.addAttribute("total",cs.getTotalByCartList(c_info_list));
        return "front/order/cart";
    }

    @RequestMapping("/delete")
    @ResponseBody
    public String delete(){
        //获取到ServletRequestAttributes 里面有 RequestContextHolder
        ServletRequestAttributes attrs = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        //获取到Request对象
        HttpServletRequest request = attrs.getRequest();
        //获取到Session对象
        HttpSession session = request.getSession();
        //获取到Response对象
        HttpServletResponse response = attrs.getResponse();
        //赋值
        String id = request.getParameter("id");
        //先生成个json对象
        JSONObject json = JSONUtil.createObj();
        //先判断登录状态
        User user = (User)session.getAttribute("user");
        if (user==null){
            //未登录在session中删除
            List<Cart> cl = (List<Cart>)session.getAttribute("cart");
            if (!cl.isEmpty()){
                for (Cart c: cl) {
                    if (c.getProductId().equals(id)){
                        cl.remove(c);
                        json.put("code", "0");
                        json.put("msg", "删除成功");
                        break;
                    }
                }
            }else{
                json.put("code", "1");
                json.put("msg", "删除失败");
            }
            session.setAttribute("cart",cl);
            return json.toString();
        }

        //已登录在数据库中删除
        if (cs.deleteProductById(id,user.getUserId())){
            //删除成功
            json.put("code", "0");
            json.put("msg", "删除成功");
        }else{
            //删除失败
            json.put("code", "1");
            json.put("msg", "删除失败");
        }

        return json.toString();
    }

    @RequestMapping("/update")
    @ResponseBody
    public String updateCart(){
        //获取到ServletRequestAttributes 里面有 RequestContextHolder
        ServletRequestAttributes attrs = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        //获取到Request对象
        HttpServletRequest request = attrs.getRequest();
        //获取到Session对象
        HttpSession session = request.getSession();
        //获取到Response对象
        HttpServletResponse response = attrs.getResponse();


        //先获取字符串json
        String jsonStr = request.getParameter("data");
        //转换为json对象
        JSONObject jsonObject = JSONUtil.parseObj(jsonStr);
        //先生成个json对象
        JSONObject json = JSONUtil.createObj();
        //先判断登录状态
        User user = (User)session.getAttribute("user");
        if (user==null){
            //未登录状态
            List<Cart> cl = (List<Cart>)session.getAttribute("cart");
            if (!cl.isEmpty()){
                for (Cart c: cl) {
                    c.setCount(Integer.valueOf((String) jsonObject.get(c.getProductId())));
                }
                json.put("code", "0");
                json.put("msg", "更新成功");
            }else {
                json.put("code", "1");
                json.put("msg", "更新失败");
            }
            session.setAttribute("cart",cl);
            return json.toString();
        }

        //已登录在数据库中更新
        if (cs.updateProductByIdAndCount(jsonObject,user.getUserId())){
            //删除成功
            json.put("code", "0");
            json.put("msg", "更新成功");
        }else{
            //删除失败
            json.put("code", "1");
            json.put("msg", "更新失败");
        }

        return json.toString();
    }

    @RequestMapping("/add")
    @ResponseBody
    public String addCart(){
        //获取到ServletRequestAttributes 里面有 RequestContextHolder
        ServletRequestAttributes attrs = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        //获取到Request对象
        HttpServletRequest request = attrs.getRequest();
        //获取到Session对象
        HttpSession session = request.getSession();
        //获取到Response对象
        HttpServletResponse response = attrs.getResponse();
        //获取添加的商品ID
        String id = request.getParameter("id");

        //先生成个json对象
        JSONObject json = JSONUtil.createObj();
        //判断登录状态
        User user = (User)session.getAttribute("user");
        if (user==null){
            //未登录
            List<Cart> cl;
            if (session.getAttribute("cart")==null){
                cl = new ArrayList<Cart>();
            }else {
                cl = (List<Cart>)session.getAttribute("cart");
            }

            if (!cl.isEmpty()){
                for (Cart c:cl) {
                    if (c.getProductId().equals(id)){
                        c.setCount(c.getCount()+1);
                        json.put("code", "0");
                        json.put("msg", "添加成功");
                        session.setAttribute("cart",cl);
                        //调用一次更新
                        List<CartInfo> c_info_list = cs.getCartInfoByList(cl);
                        session.setAttribute("cartlist",c_info_list);
                        return json.toString();
                    }
                }
            }

            Cart c = new Cart();
            c.setProductId(id);
            c.setCount(1);
            cl.add(c);
            session.setAttribute("cart",cl);
            //调用一次更新
            List<CartInfo> c_info_list = cs.getCartInfoByList(cl);
            session.setAttribute("cartlist",c_info_list);
            json.put("code", "0");
            json.put("msg", "添加成功");
            return json.toString();
        }

        //已登录
        if (cs.addProductById(id,user.getUserId())){
            json.put("code", "0");
            json.put("msg", "添加成功");
        }else {
            json.put("code", "1");
            json.put("msg", "添加失败");
        }
        //调用一次更新
        List<Cart> cl = cs.getCartList(user.getUserId());
        List<CartInfo> c_info_list = cs.getCartInfoByList(cl);
        session.setAttribute("cartlist",c_info_list);
        json.put("code", "0");
        json.put("msg", "添加成功");
        return json.toString();

    }
}
