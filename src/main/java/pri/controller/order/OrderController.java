package pri.controller.order;

import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.alipay.api.domain.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import pri.common.Common;
import pri.pojo.CartInfo;
import pri.pojo.OderDetail;
import pri.pojo.RecAddress;
import pri.pojo.User;
import pri.service.order.CartService;
import pri.service.order.OrderService;
import pri.service.order.RecAddressService;
import pri.service.product.ProductService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

@Controller
@RequestMapping("/order")
public class OrderController {
    @Autowired
    private CartService cs;

    @Autowired
    private RecAddressService ras;

    @Autowired
    private OrderService os;


    @RequestMapping("/pre")
    public String pre(){
        //获取到ServletRequestAttributes 里面有 RequestContextHolder
        ServletRequestAttributes attrs = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        //获取到Request对象
        HttpServletRequest request = attrs.getRequest();
        //获取到Session对象
        HttpSession session = request.getSession();
        //获取到Response对象
        HttpServletResponse response = attrs.getResponse();

        //获取一下用户
        User user = (User)session.getAttribute("user");
        if (user == null){
            return "redirect:/login/login";
        }

        //首先获取数据库的购物车内容
        List<CartInfo> cil = cs.getCartInfoByList(cs.getCartList(user.getUserId()));
        //List<CartInfo> cil = cs.getCartInfoByList(cs.getCartList("1515"));
        //返回给前端
        request.setAttribute("realcartlist",cil);
        request.setAttribute("total",cs.getTotalByCartList(cil));
        return "front/order/checkout";
    }

    @RequestMapping("/add")
    @ResponseBody
    public String addOrder(){
        //获取到ServletRequestAttributes 里面有 RequestContextHolder
        ServletRequestAttributes attrs = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        //获取到Request对象
        HttpServletRequest request = attrs.getRequest();
        //获取到Session对象
        HttpSession session = request.getSession();
        //获取到Response对象
        HttpServletResponse response = attrs.getResponse();
        //先生成个json对象
        JSONObject json = JSONUtil.createObj();

        //获取下user
        User user = (User) session.getAttribute("user");
        //获取下json
        String jsonStr = request.getParameter("content");
        //转换为json对象
        JSONObject jsonObject = JSONUtil.parseObj(jsonStr);
        //获取下支付方式
        String com = (String) jsonObject.get("com");

        //先插入地址
        int newaddressid = ras.insertNewAddress(jsonObject,user.getUserId());
        System.out.println("address_id:"+newaddressid);

        //插入订单 并与之关联
        //首先获取数据库的购物车内容
        List<CartInfo> cil = cs.getCartInfoByList(cs.getCartList(user.getUserId()));
        String oid = os.insertOrder(cil,user.getUserId(),newaddressid);



        //下单成功后清空购物车
        cs.clearCart(user.getUserId());

        //页面重定向到支付页面
        json.put("code", "0");
        String localUrl = request.getScheme() +"://" + request.getServerName() + ":" +request.getServerPort()+"/"+request.getContextPath();
        json.put("msg", localUrl+"/order/pay?oid="+oid+"&com="+com);
        json.put("oid",oid);
        return json.toString();

    }

    @RequestMapping("/pay")
    @ResponseBody
    public String payOrderOnline(String oid,String com) throws IOException {

        if (com.equals("1")){
            return os.payTheOrder(oid, Common.PAY_ALI);
        }else {
            return os.payTheOrder(oid, Common.PAY_ALI);
        }

    }

    @RequestMapping("/myorder")
    public String showMyOrder(){
        return "redirect:/user/account?to=order";
    }

    @RequestMapping("/payinquery")
    @ResponseBody
    public String payInquery(HttpServletRequest request,HttpServletResponse response,HttpSession session){
        //先生成个json对象
        JSONObject json = JSONUtil.createObj();

        String oid = request.getParameter("oid");
        String com = request.getParameter("com");
        if (com.equals("1")){
            int payCom = Common.PAY_ALI;
        }
        if(os.inqueryPayOrder(oid,Common.PAY_ALI)){
            //支付成功
            json.put("code","0");
            json.put("msg","支付成功");
            return json.toString();
        }else {
            //支付失败或未支付
            json.put("code","1");
            json.put("msg","支付失败");
            return json.toString();
        }
    }


    @RequestMapping("/detail")
    public String showDetail(){
        //获取到ServletRequestAttributes 里面有 RequestContextHolder
        ServletRequestAttributes attrs = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        //获取到Request对象
        HttpServletRequest request = attrs.getRequest();
        //获取到Session对象
        HttpSession session = request.getSession();
        //获取到Response对象
        HttpServletResponse response = attrs.getResponse();


        try {
            //获取订单号
            String oid = request.getParameter("oid");

            //通过oid获取订单列表
            List<OderDetail> odl = os.getAllDetailByOrderId(oid);

            //通过地址ID获取联系人信息
            RecAddress ra = os.getRecAddressById(odl.get(0).getRecAddress());

            //获取商品总价
            float total = 0;
            for (OderDetail od:odl) {
                total+=od.getProductFinPrice();
            }

            request.setAttribute("orderList",odl);
            request.setAttribute("recAddress",ra);
            request.setAttribute("ptotal",total);
        }catch (Exception e){
            e.printStackTrace();
        }

        return "front/order/order_detail";
    }

    @RequestMapping("/confirm")
    @ResponseBody
    public String confirmRec(){
        //获取到ServletRequestAttributes 里面有 RequestContextHolder
        ServletRequestAttributes attrs = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        //获取到Request对象
        HttpServletRequest request = attrs.getRequest();
        //获取到Session对象
        HttpSession session = request.getSession();
        //获取到Response对象
        HttpServletResponse response = attrs.getResponse();
        //先生成个json对象
        JSONObject json = JSONUtil.createObj();

        //获取订单号
        String oid = request.getParameter("oid");
        os.changeOrderStatus(oid,Common.ORDER_STATUS_4);
        return "success";

    }
    @RequestMapping("aa")
    public String aa(){
        return "front/order/discuss";
    }
    //    跳转到评论页面
    @RequestMapping("/mark")
    public String doMark(int id, String orderId,Model model){
        //获取到ServletRequestAttributes 里面有 RequestContextHolder
        ServletRequestAttributes attrs = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        //获取到Request对象
        HttpServletRequest request = attrs.getRequest();
        //获取到Session对象
        HttpSession session = request.getSession();
        //获取到Response对象
        HttpServletResponse response = attrs.getResponse();
        //先生成个json对象
        JSONObject json = JSONUtil.createObj();

        model.addAttribute("productId",id);
        model.addAttribute("orderId",orderId);

        return "front/order/discuss";
    }
}
