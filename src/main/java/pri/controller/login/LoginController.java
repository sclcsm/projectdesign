package pri.controller.login;

import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.MultipartFile;
import pri.common.Common;
import pri.pojo.Cart;
import pri.pojo.User;
import pri.service.Login.LoginService;
import pri.service.order.CartService;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.net.URLDecoder;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/login")
public class LoginController {
    @Autowired
    private LoginService loginService;

    @Autowired
    private CartService cartService;

    //主页面

    @RequestMapping("/home")
    public String mainPage(){

        return "front/login/home";
    }

    //跳转到登录界面
    @RequestMapping("/login")
    public String login(){
        //获取到ServletRequestAttributes 里面有 RequestContextHolder
        ServletRequestAttributes attrs = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        //获取到Request对象
        HttpServletRequest request = attrs.getRequest();
        //获取到Session对象
        HttpSession session = request.getSession();
        //获取到Response对象
        HttpServletResponse response = attrs.getResponse();
        if (session.getAttribute("user")!=null){
            return "redirect:/product/home";
        }

        Cookie[] cookies = null;
        // 获取cookies的数据,是一个数组
        cookies = request.getCookies();
        String username = "";
        String password = "";
        if(cookies !=null){
            for(int i = 0;i < cookies.length;i++){  //遍历cookie对象集合
                if(cookies[i].getName().equals("username")){
                    username = URLDecoder.decode(cookies[i].getValue().split("#")[0]);//获取用户名
                    System.out.println("yonghuming:"+username);
                }

                if(cookies[i].getName().equals("password")){
                    password = URLDecoder.decode(cookies[i].getValue().split("#")[0]);//获取密码
                    System.out.println("mima:"+password);
                }
            }
        }

        if (!username.isEmpty() && !password.isEmpty()){
            System.out.println("zheli");
            User user = new User();
            user.setUserName(username);
            user.setPassword(password);
            User loginUser = loginService.selectByUserNameAndPassword(user);
            if (loginUser!=null){
                session.setAttribute("user",loginUser);
                //DJY 合并购物车
                List<Cart> cl = (List<Cart>)session.getAttribute("cart");
                if (cl!=null && !cl.isEmpty()){
                    cartService.mergeSessionToDb(cl,loginUser.getUserId());
                }
                return "redirect:/product/home";
            }else{
                return "redirect:/login/logout";
            }
        }
        return "front/login/login";
    }

    //    用户登录
    @RequestMapping("/doLogin")
    @ResponseBody
    public Map<String,Object> doLogin(String data,HttpServletRequest request,HttpServletResponse response,HttpSession session){

        Map<String,Object> jsonMap = new HashMap<>();
        //先获取字符串json
        //转换为json对象
        JSONObject jsonObject = JSONUtil.parseObj(data);
        System.out.println("str:"+data);
        User user = new User();
        user.setUserName((String) jsonObject.get("userName"));
        user.setPassword((String)jsonObject.get("password"));
        User loginUser = loginService.selectByUserNameAndPassword(user);
        if (loginUser!=null){
            session.setAttribute("user",loginUser);
            //DJY 合并购物车
            List<Cart> cl = (List<Cart>)session.getAttribute("cart");
            if (cl!=null && !cl.isEmpty()){
                cartService.mergeSessionToDb(cl,loginUser.getUserId());
                System.out.println("ll:"+cl);
            }
            //上面的语句别动哈
            System.out.println(loginUser);

            //成功后判断加不加cookie
            if ((boolean)jsonObject.get("rememberMe")){
                System.out.println("hello");
                Cookie cookieu = new Cookie("username",loginUser.getUserName());
                cookieu.setMaxAge(60*2);
                Cookie cookiep = new Cookie("password",loginUser.getPassword());
                cookiep.setMaxAge(60*2);
                response.addCookie(cookieu);
                response.addCookie(cookiep);
            }
            jsonMap.put("code","1");
        }else{
            jsonMap.put("code","0");
        }
        return jsonMap;

    }





    @RequestMapping("/logout")
    public String logout(){
        //获取到ServletRequestAttributes 里面有 RequestContextHolder
        ServletRequestAttributes attrs = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        //获取到Request对象
        HttpServletRequest request = attrs.getRequest();
        //获取到Session对象
        HttpSession session = request.getSession();
        //获取到Response对象
        HttpServletResponse response = attrs.getResponse();

        //清空所有session
        Enumeration em = session.getAttributeNames();
        while(em.hasMoreElements()){
            session.removeAttribute(em.nextElement().toString());
        }

        //清除cookie
        Cookie[] cookies = null;
        cookies = request.getCookies();
        for(int i=0;i<cookies.length;i++){
            cookies[i].setMaxAge(0);
            response.addCookie(cookies[i]);
        }

        return "redirect:/login/login";
    }


    //修改密码
    @RequestMapping("/doChange")
    public String doChange(User user){
        if(loginService.updateUser(user)>0){
            return "/front/login/login";
        }
        return "/front/login/register";

    }

    //跳转到注册界面
    @RequestMapping("/register")
    public String register(){
        return "front/login/register";
    }

    //注册用户

    @RequestMapping("/doRegister")
    @ResponseBody
//    User user,String code,HttpSession session
    public String doRegister(){
        //获取到ServletRequestAttributes 里面有 RequestContextHolder
        ServletRequestAttributes attrs = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        //获取到Request对象
        HttpServletRequest request = attrs.getRequest();
        //获取到Session对象
        HttpSession session = request.getSession();
        //获取到Response对象
        HttpServletResponse response = attrs.getResponse();
        //先获取字符串json
        String jsonStr = request.getParameter("content");
        //转换为json对象
        JSONObject jsonObject = JSONUtil.parseObj(jsonStr);
        //创建Json对象
        JSONObject jsonMap = new JSONObject();

        String code = (String) jsonObject.get("code");
        if (session.getAttribute("code").equals(code)){
            User user = new User();
            user.setUserName((String) jsonObject.get("username"));
            user.setNickName((String) jsonObject.get("nickname"));
            user.setPassword((String) jsonObject.get("password"));
            user.setEmail((String) jsonObject.get("email"));
            String id = Common.getUUID(12);
            user.setUserId(id);
            if(loginService.insertUser(user)){
                session.setAttribute("user",user);
                jsonMap.put("code","1");
                jsonMap.put("msg","注册成功");
                List<Cart> cl = (List<Cart>)session.getAttribute("cart");
                if (cl!=null && !cl.isEmpty()){
                    cartService.mergeSessionToDb(cl,id);
                    System.out.println("ll:"+cl);
                }
            }else {
                jsonMap.put("code","0");
                jsonMap.put("msg","验证码不正确");
            }
        }
        return jsonMap.toString();
    }
    //发送验证码
    @RequestMapping(value = "/verify")
    @ResponseBody
    public String doVerify(){
        //获取到ServletRequestAttributes 里面有 RequestContextHolder
        ServletRequestAttributes attrs = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        //获取到Request对象
        HttpServletRequest request = attrs.getRequest();
        //获取到Session对象
        HttpSession session = request.getSession();
        //获取到Response对象
        HttpServletResponse response = attrs.getResponse();
        //创建Json对象
        JSONObject jsonObject = new JSONObject();
        //获取Email
        String email = request.getParameter("data");
        //        System.out.println("ojbk");

        if (loginService.isExist(email)){
    //            System.out.println(email);
            session.setAttribute("code",loginService.getVerify(email));
            jsonObject.put("code","0");
            jsonObject.put("msg","发送成功，请检查邮箱！");
        } else {
            jsonObject.put("code","1");
            jsonObject.put("msg","该邮箱已经被注册");
        }
        return jsonObject.toString();

    }


//上传头像文件
    @RequestMapping("upload")
    @ResponseBody
    public String upload(String name, MultipartFile avatar){
        //获取到ServletRequestAttributes 里面有 RequestContextHolder
        ServletRequestAttributes attrs = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        //获取到Request对象
        HttpServletRequest request = attrs.getRequest();
        //获取到Session对象
        HttpSession session = request.getSession();
        //获取到Response对象
        HttpServletResponse response = attrs.getResponse();



        System.out.println("上传文件名为"+avatar.getOriginalFilename()+" 大小为"+avatar.getSize());

        try {
            avatar.transferTo(new File("F:/test/img/avatar.jpg"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "true";
    }

}
