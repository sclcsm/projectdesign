package pri.controller.discount;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import pri.pojo.Coupon;
import pri.pojo.User;
import pri.service.discount.CouponService;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/coupon")
public class CouponController {
    @Autowired
    private CouponService couponService;

    @RequestMapping("/queryCoupons/{userId}")  //g根据userId查询优惠券信息
    public String queryCoupons(HttpSession session,@PathVariable String userId){
        List<Coupon> list=couponService.selectByUserId(userId);
        //System.out.println(couponService.findAll());

        System.out.println("登录用户："+(User)session.getAttribute("user"));

        List<Coupon> list1=new ArrayList<>();
        List<Coupon> list2=new ArrayList<>();
        List<Coupon> list3=new ArrayList<>();

        if (list!=null){
            for(Coupon coupon:list){
                switch (coupon.getStatus()){
                    case 1:list1.add(coupon);break;
                    case 2:list2.add(coupon);break;
                    case 3:list3.add(coupon);break;
                }
            }

        }

        session.setAttribute("list1",list1);
        session.setAttribute("list2",list2);
        session.setAttribute("list3",list3);

        return "front/discount/userCoupons";
    }

    @RequestMapping("/useCoupon/{couponId}")
    public String useCoupon(@PathVariable Integer couponId,HttpSession session){
            User user=(User)session.getAttribute("user");
            couponService.useCoupon(couponId);
            return "redirect:/coupon/queryCoupons/"+user.getUserId();
    }

    @RequestMapping("/getCoupon/{couponId}/{productId}")
    public String getCoupon(@PathVariable Integer couponId,HttpSession session,@PathVariable Integer productId,@RequestParam String page){
        User user=(User)session.getAttribute("user");
//        System.out.println("商品ID:"+productId);
//        System.out.println("用户:"+user);
//        System.out.println("优惠券ID:"+couponId);

                couponService.getCoupon(couponId,user.getUserId());
            if (page.equals("showCoupons")){
                return  "redirect:/coupon/showCoupons?productId="+productId;
            }else{
                return "redirect:/product/detail/"+productId;
            }
    }


    @RequestMapping("/showCoupons")
    public String showCoupons(HttpSession session,@RequestParam Integer productId){
        session.setAttribute("list1",couponService.selectByProduct(productId,0));
        session.setAttribute("productId",productId);

        return "front/discount/moreCoupons";
    }


}
