package pri.controller.product;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import pri.common.Common;
import pri.pojo.Page;
import pri.pojo.ProductComment;
import pri.pojo.ProductInfo;
import pri.pojo.User;
import pri.service.discount.CouponService;
import pri.service.order.OrderService;
import pri.service.product.ProductService;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
@RequestMapping("/product")
public class ProductController {
    @Autowired
    ProductService productService;
    @Autowired
    CouponService couponService;
    @Autowired
    OrderService orderService;

    @RequestMapping("/home")
    public String all(Model model, HttpSession session){
        User loginUser = (User) session.getAttribute("user");
        String userId = null;
        if(loginUser!=null){
            userId = loginUser.getUserId();

        }

        productService.getHome(model,userId);
       /* else{
            Model homeModel = (Model) session.getAttribute("homeModel");
            if(homeModel == null){
                productService.getHome(model,null);
                session.setAttribute("homeModel",model);
            }else{
                model = homeModel;
                System.out.println(model);
            }
        }
*/
        return "/front/product/home";
    }

    @RequestMapping("/list")
    public String query(ProductInfo product, Page page,Model model,HttpSession session){
        System.out.println("接收到一个/list请求,参数为："+product);

   /*     ProductInfo queryProduct = (ProductInfo) session.getAttribute("queryProduct");
        if(queryProduct != null){
            queryProduct = productService.transQuery(queryProduct,product);
        }else{
            queryProduct = product;
        }*/

        User user = (User) session.getAttribute("user");
        if(user!=null){
            model.addAttribute("collections",productService.getCollections(user.getUserId(),5));
            product.setUserId(user.getUserId());
        }


        model.addAttribute("queryProduct",product);
        model.addAttribute("products",productService.findByExample(product, page));
        model.addAttribute("categories",productService.hotCategory());
        model.addAttribute("mastLook",productService.mastLook());
        model.addAttribute("page",page);


        return "/front/product/product-list";
    }

    @RequestMapping("/detail/{id}")
    public String detail(@PathVariable Integer id, Model model,  HttpSession session){
        User loginUser = (User) session.getAttribute("user");
        String userId = null;
        if(loginUser!=null){
            userId = loginUser.getUserId();
        }

        model.addAttribute("product",productService.findById(id,userId));
        model.addAttribute("coupons",couponService.selectByProduct(id,4));

        return "/front/product/detail";
    }

    @GetMapping("/collection")
    public String collections(Model model, Page page, HttpSession session){
        User loginUser = (User) session.getAttribute("user");
        if(loginUser == null){
            return "redirect:list";
        }
        List<ProductInfo> collections = productService.getCollections(loginUser.getUserId(), 100);
        model.addAttribute("collections",collections);
        page.setTotalRows(collections.size());
        model.addAttribute("page",page);

        return "/front/product/collection";
    }

    @PostMapping("/addComment")
    @ResponseBody
    public Integer addComment(@RequestBody ProductComment productComment, HttpSession session){
        User loginUser = (User) session.getAttribute("user");
        if(loginUser == null){
            return 0;
        }
        String orderId = productComment.getUserId();

        Integer integer = productService.addComment(productComment, loginUser.getUserId());
        if(integer>0){
            orderService.changeOrderStatus(orderId, Common.ORDER_STATUS_5);
        }

        return integer;
    }

    @PostMapping("/collection")
    @ResponseBody
    public Integer collection(Integer productId,  HttpSession session){
        User loginUser = (User) session.getAttribute("user");
        if(loginUser!=null){
            return productService.collection(productId,loginUser.getUserId());
        }

        return 200;
    }

    @PostMapping("/cancelCollection")
    @ResponseBody
    public Integer cancelCollection(Integer productId, HttpSession session){
        User loginUser = (User) session.getAttribute("user");
        if(loginUser!=null){
            return productService.cancelCollection(productId,loginUser.getUserId());
        }

        return 200;
    }



    @GetMapping("/add")
    public String add(Model model){

        return "/front/product/add";
    }
    @PostMapping("/add")
    public String add(@ModelAttribute ProductInfo product){
        Integer add = productService.add(product);

        if(add>0){
            return "/front/product/addSuccess";
        }

        return "/front/product/add";
    }

    @GetMapping("/update")
    public String update(Model model){

        return "/front/product/update";
    }
    @PostMapping("/update")
    public String update(@ModelAttribute ProductInfo product){
        boolean update = productService.update(product);

        if(update){
            return "/front/product/updateSuccess";
        }

        return "/front/product/update";
    }

    @GetMapping("/delete")
    public String delete(Model model){

        return "/front/product/delete";
    }
    @PostMapping("/delete")
    public String delete(Integer id){
        boolean delete = productService.delete(id);

        if(delete){
            return "/front/product/deleteDelete";
        }

        return "/front/product/delete";
    }
}
