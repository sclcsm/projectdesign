package pri.controller.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import pri.pojo.User;
import pri.service.order.OrderService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/user")
public class UserController {
    @Autowired
    OrderService os;

    @RequestMapping("/account")
    public String myAccount(){
        //获取到ServletRequestAttributes 里面有 RequestContextHolder
        ServletRequestAttributes attrs = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        //获取到Request对象
        HttpServletRequest request = attrs.getRequest();
        //获取到Session对象
        HttpSession session = request.getSession();
        //获取到Response对象
        HttpServletResponse response = attrs.getResponse();

        //获取User
        User user = (User) session.getAttribute("user");

        //从其他控制器传过来 判断一下默认展开的板块
        String toWhere;
        if (request.getParameter("to")!=null){
            toWhere = request.getParameter("to");
        }else {
            toWhere = "index";
        }
        String runJS = "";
        switch (toWhere){
            case "order":
                runJS = "$(\"a[href*=#orders]\").click()";
                break;
            case "index":
                runJS = "";
                break;
        }
        request.setAttribute("runJs",runJS);

        //获取页面应该显示的所有数据
        request.setAttribute("orderlist",os.getAllOrderByUserId(user.getUserId()));


        return "front/myaccount";
    }
}
