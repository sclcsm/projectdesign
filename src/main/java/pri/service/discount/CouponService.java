package pri.service.discount;

import pri.pojo.Coupon;

import java.util.List;

public interface CouponService {

    /**
     *
     * @param productId  传入商品ID
     * @param number  显示优惠券张数
     * @return
     */
    public List<Coupon> selectByProduct(Integer productId,Integer number);

    //public List<Coupon> findAll();

    public List<Coupon> selectByUserId(String userId);

    public void useCoupon(Integer couponId);

    public void getCoupon(Integer couponId, String userId);

    public int getDiscountByProductId(Integer productId);
}
