package pri.service.discount;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pri.dao.CategoryMapper;
import pri.dao.CouponMapper;
import pri.dao.ProductMapper;
import pri.pojo.Coupon;
import pri.pojo.CouponExample;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class CouponServiceImpl implements CouponService {
    @Autowired
    private CouponMapper couponMapper;
    @Autowired
    private ProductMapper productMapper;
    @Autowired
    private CategoryMapper categoryMapper;
    @Override
    public List<Coupon> selectByProduct(Integer productId,Integer number) {
        List<Coupon> coupons=new ArrayList<>();
        CouponExample couponExample=new CouponExample();

        //查询单品券
        couponExample.createCriteria().andScopeEqualTo(2).andTargetIdEqualTo(productId);
        coupons=couponMapper.selectByExample(couponExample);
        couponExample.clear();

        //查询品类券,并收集
        Integer categoryId=productMapper.selectByPrimaryKey(productId).getCategoryId();
        couponExample.createCriteria().andScopeEqualTo(1).andTargetIdEqualTo(categoryId);
        coupons.addAll(couponMapper.selectByExample(couponExample));
        couponExample.clear();

        //查询全品券,并收集
        couponExample.createCriteria().andScopeEqualTo(0).andUserIdEqualTo("");
        coupons.addAll(couponMapper.selectByExample(couponExample));
        couponExample.clear();

        if(coupons!=null){
            for (Coupon coupon:coupons){
                switch (coupon.getScope()){
                    case 0:coupon.setTarget("全品券");break;
                    case 1:coupon.setTarget("品类券");break;
                    case 2:coupon.setTarget("单品券");break;
                }
            }
        }

        Collections.sort(coupons,(o1,o2)-> o2.getPreferentialSum()-o1.getPreferentialSum());
        if (number<=coupons.size()&&number!=0){
            coupons = coupons.subList(0,number);
        }

        return coupons;
    }

   /* @Override
    public List<Coupon> findAll() {
        CouponExample couponExample=new CouponExample();
        List<Coupon> coupons=couponMapper.selectByExample(couponExample);
        if(coupons!=null){
            for (Coupon coupon:coupons){
                switch (coupon.getScope()){
                    case 0:coupon.setTarget("全品券");break;
                    case 1:coupon.setTarget("品类券");break;
                    case 2:coupon.setTarget("单品券");break;
                }
            }
        }
        return coupons;
    }*/

    @Override
    public List<Coupon> selectByUserId(String userId) {
        CouponExample couponExample=new CouponExample();
        couponExample.createCriteria().andUserIdEqualTo(userId);
        List<Coupon> coupons=couponMapper.selectByExample(couponExample);
        if(coupons!=null){
            for (Coupon coupon:coupons){
                switch (coupon.getScope()){
                    case 0:coupon.setTarget("全品券");break;
                    case 1:coupon.setTarget("品类券");break;
                    case 2:coupon.setTarget("单品券");break;
                }
            }
        }

        Collections.sort(coupons,(o1,o2)-> o2.getPreferentialSum()-o1.getPreferentialSum());
        return coupons;
    }

    @Override
    public void useCoupon(Integer couponId) {
        Coupon coupon=new Coupon();
        coupon.setCouponId(couponId);
        coupon.setStatus(2);
        //System.out.println("优惠券:"+coupon);
        couponMapper.updateByPrimaryKeySelective(coupon);
    }

    @Override
    public void getCoupon(Integer couponId, String userId) {
        Coupon coupon=new Coupon();
        coupon.setCouponId(couponId);
        coupon.setUserId(userId);
        couponMapper.updateByPrimaryKeySelective(coupon);
    }

    @Override
    public int getDiscountByProductId(Integer productId) {
        return 0;
    }
}
