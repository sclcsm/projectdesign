package pri.service.order;

import cn.hutool.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pri.common.Common;
import pri.dao.OderDetailMapper;
import pri.dao.OrderListMapper;
import pri.dao.RecAddressMapper;
import pri.pojo.*;

import java.util.List;

@Service()
@Transactional
public class RecAddressServiceImpl implements RecAddressService{
    @Autowired
    RecAddressMapper ram;
    @Autowired
    OrderListMapper olm;
    @Autowired
    OderDetailMapper odm;

    @Override
    public int insertNewAddress(JSONObject jsonObject,String user_id) {
        RecAddress ra = new RecAddress();
        ra.setUserId(user_id);
        ra.setCountry("中国");
        ra.setProvince((String) jsonObject.get("province"));
        ra.setCity((String) jsonObject.get("city"));
        ra.setStreet((String) jsonObject.get("street"));
        ra.setDetail((String) jsonObject.get("detail"));
        ra.setRecName((String) jsonObject.get("rec_name"));
        ra.setRecPhone((String) jsonObject.get("rec_phone"));
        int res = ram.insertSelective(ra);
        //成功插入后用Example找回主键。。。
        RecAddressExample rae = new RecAddressExample();
        rae.createCriteria().andUserIdEqualTo(user_id);
        rae.setOrderByClause("id desc");
        List<RecAddress> ral = ram.selectByExample(rae);
        RecAddress ran = ral.get(0);
        return ran.getId();
    }


}
