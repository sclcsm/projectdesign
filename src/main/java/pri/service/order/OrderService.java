package pri.service.order;

import pri.pojo.CartInfo;
import pri.pojo.OderDetail;
import pri.pojo.OrderList;
import pri.pojo.RecAddress;

import java.util.List;

public interface OrderService {

    String insertOrder(List<CartInfo> cil, String user_id, int rec_id);

    String payTheOrder(String order_id,int payCom);

    boolean inqueryPayOrder(String oid,int payCom);

    void changeOrderStatus(String oid,String status);

    List<OrderList> getAllOrderByUserId(String user_id);

    List<OderDetail> getAllDetailByOrderId(String oid);

    RecAddress getRecAddressById(String id);

}
