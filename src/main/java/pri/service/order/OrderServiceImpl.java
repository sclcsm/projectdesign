package pri.service.order;

import cn.hutool.core.util.URLUtil;
import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.request.AlipayTradePagePayRequest;
import com.alipay.api.request.AlipayTradeQueryRequest;
import com.alipay.api.response.AlipayTradeQueryResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pri.common.Common;
import pri.dao.OderDetailMapper;
import pri.dao.OrderListMapper;
import pri.dao.RecAddressMapper;
import pri.pojo.*;
import pri.service.product.ProductService;

import java.util.Date;
import java.util.List;


@Service()
@Transactional
public class OrderServiceImpl implements OrderService {
    @Autowired
    RecAddressMapper ram;
    @Autowired
    OrderListMapper olm;
    @Autowired
    OderDetailMapper odm;
    @Autowired
    ProductService productService;

    @Override
    public String insertOrder(List<CartInfo> cil, String user_id, int rec_id) {
        //先生成订单号 并插入订单列表
        String oid = Common.getUUID(10);
        OrderList ol = new OrderList();
        ol.setOrderId(oid);
        ol.setUserId(user_id);
        ol.setStatus(Common.ORDER_STATUS_1);
        ol.setCreatetime1(new Date());

        float total = 0;
        //循环遍历一次 不断插入order_detail表
        for (CartInfo ci:cil) {
            OderDetail od = new OderDetail();
            od.setOrderId(oid);
            od.setProductName(ci.getProduct_name());
            //这个地方要写一个getAttrByProductId();
            od.setProductAttr("AAAA");
            od.setProductCount(ci.getCount());
            //1为未付款
            od.setOrderStatus(Common.ORDER_STATUS_1);
            od.setRecAddress(String.valueOf(rec_id));
            od.setProductId(ci.getProduct_id());

            //增加热度
            productService.addHeat(Integer.valueOf(ci.getProduct_id()),10*ci.getCount());

            od.setProductOriPrice(ci.getSum());
            //这个地方要写一个getDiscountByProductId() 与总价相减
            od.setProductFinPrice(ci.getSum());
            total = total + ci.getSum();
            odm.insertSelective(od);
        }
        //得到总价后插入到list表
        ol.setTotal(String.valueOf(total));
        olm.insertSelective(ol);
        return oid;
    }

    @Override
    public String payTheOrder(String order_id,int payCom) {
        //通过orderid获取总金额
        OderDetailExample ode = new OderDetailExample();
        ode.createCriteria().andOrderIdEqualTo(order_id);
        List<OderDetail> odl = odm.selectByExample(ode);
        float total = 0;
        String body = "";
        for (OderDetail od:odl) {
            total+=od.getProductFinPrice();
            //body+=od.getProductName()+";";
        }

        if (payCom == Common.PAY_ALI){
            //支付宝调用接口
            //这个地方填你的沙盒网关地址
            String serverUrl = "https://openapi.alipaydev.com/gateway.do";
            //你的appid
            String appid = "2016100100639757";
            //填应用私钥
            String privateKey = "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCrZ9a0eKa7A69PFVliegB8+fL95PXSriLKZzlqCW5tI0N9kK2IXCvxNAqWeCURUwDS/DHcEburNy4gqNb2numQ3L19UBSsrjedUEiPel7aD7xhZ0/rLfLiAtFwVg49vx8N4e/K/sItYExHngWcQOJfAXvYL++s0oEgi3COG6gc9Il+k+mkr/ZXEJJa3KjCDEXc/951vqb0TrCRKrsTck6R3cwIx0NwPcCD2UW4qrR7LFIkSpnlN2VlFArtbgzYo7uSOCGGbYA55Mp46+5JhGXj099wHB65dbOChfd69ipCFIUb1bXBsFvrrrzPNRfVqnRLrzX0st+5XigOnMnawPsXAgMBAAECggEAYgZ4m62EbN+mmquy+sabEN5BQUJ384zzCJ4QD7xlErVgG6dqFtnfp4oDpBoB2O6HQiJIwXrCwSBFmRf2GAS5vsb+93Oc30SjOax4UtXA+SSWr33ODsmwZeEGtiAA9P8gW2gjIwyfMCQqe6q7uqjAzWSj1vdTMpsQjfQw4pSeiC/Auw/tYgrfNI+nfq00d2Iw/7gq6sUvasB6Uy5+rDEJrwArtLsWsHFocXJ//Wtiig8jF18B1C9iSbWDUey5CTIHoPFoeIa0cQVaQfoIpLi0DUd8zIpntkBx9w6fRDmyTnsOJSYyQJixGnKBbPuD/PkL+XC44ABVVR9SrlENetajgQKBgQDn7hCrRlDXQBgVzzaQtL2DjZ2wv4WZqrAXLFuHBXVJHgA2pjrMICGCbzTNJqfblG5MRFywPn03IDIYGvy4EvFaN13L2IszSq0w7F4ypMKw/Vcdth5TkRPeFjyUBom4Qv2ZICidzaI+r0SVdh0qFz+GOXWt1uIza290SeWqu4p7/wKBgQC9McIUVcp8Qk9XIHZ6mH2/upWGRGbhyueDIRoNBYVQa/wAO27TlHenP3ZJzAS0lOBbnnQNQaFiLAWKg23sT1/5S5m81L2L/mVCLigifT2vZS3WvIKnsLbNHuiuyYzNvW1jphONY2V51WOR9YBKCBiTyOrGrVpH8Y4KgIz3Xsng6QKBgBW4zELeUQ/35smUIK4o318E0nINXfvoapmhA+l/b16ruqVjqDoaAElxFObp6eq82h+2f1/aWmvhTg/H50duLsCeUuCWpWLmaZYdbS0dFUbyU0hrGCgHfG7eckoC2r8xNwMyadttuPZZ//hnmY+pob92fjkL2R+rvkKL8UCuU+gNAoGAFptWfIWPz29A+cE4RwfnprxN16BexsYvCb0v1rR9LJHqp8BySrfn4EU2BjVproZHlMsHRPo9pXRrKRTxUpZdenVtJmIpUAwS32iShSR/9XBanNkjp5P3E+WImjIdMAxdgNW8al2hJ4C3CB7UuIPlI+uTGjBmpEgZ1+lqbriV2NkCgYEA55ObS+2c61L87E17dtxFGs8WALkmjFirJl1zkQbX76aCG95JMh4gVgcqDaiCdmJaVrqXMfURvrjvlYTHN1Nq2wwe7tFLSjk1RnaLk8Kroj2w3YnoheFVTe6pJOSy72wUFYKUxmIOpb1mC3y6wQGzpe30pC0m8To1YmU0nyJaV+A=";
            //填支付宝公钥
            String alipayPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAoZ3+mbVnwkmY52WZycPOQu5cqGzJQDouv/72NekDr/lxbYq0Okmg8Mnfezw06M5WhmO3XkxvPYkb08pH8HrR1PovLdUpNib52v41Zv2d1HUyfqjZGULzaCyOJhBQnTeCu9/cPRcBqSTMe500sJThQ+KMBQi/ZVQRYGaepLaWVzFCM14qnO6qBwubV2bZP3O5UHxNgVJqWd+JNAQnV50K3rmbqjF11R9PkntczMOteZZDTu/8sdXsqFbxQ33AUPlprxxD00X6vRTCIsOakYPP5dsBwoVMu33L7w3Ir4qAZbcZnGcjeF8dlY1a1uptHYNb0VWAWw1+dEQFRj3doSeGZQIDAQAB";
            //如果你一直调试不好的话可以就用我的appid 用我的沙盒账号进行付款 用户名dafydf8632@sandbox.com 密码111111 支付密码111111
            AlipayClient alipayClient = new DefaultAlipayClient(serverUrl,appid,privateKey,"json","utf-8",alipayPublicKey,"RSA2");
            AlipayTradePagePayRequest alipayRequest = new AlipayTradePagePayRequest();//创建API对应的request
            //alipayRequest.setReturnUrl("http://localhost:8080/alipay/return_url.jsp");
            //alipayRequest.setNotifyUrl("http://localhost:8080/alipay/notify_url.jsp");//在公共参数中设置回跳和通知地址
            alipayRequest.setBizContent("{" +
                    "\"out_trade_no\":\""+order_id+"\"," +
                    "    \"product_code\":\"FAST_INSTANT_TRADE_PAY\"," +
                    "    \"total_amount\":"+total+"," +
                    "    \"subject\":\"Welcome to SBT OnlineShop\"," +
                    "    \"body\":\"Hello World!\"," +
                    "    \"passback_params\":\"merchantBizType%3d3C%26merchantBizNo%3d2016010101111\"," +
                    "    \"extend_params\":{" +
                    "    \"sys_service_provider_id\":\"2088511833207846\"" +
                    "    }"+
                    "  }");//填充业务参数
            String form="";
            try {
                form = alipayClient.pageExecute(alipayRequest).getBody(); //调用SDK生成表单
            } catch (AlipayApiException e) {
                e.printStackTrace();
            }
            return form;
        }

        return "if后面可以写微信支付等等";

    }

    @Override
    public boolean inqueryPayOrder(String oid,int payCom) {
        if (payCom == Common.PAY_ALI){
            AlipayClient alipayClient = new DefaultAlipayClient("https://openapi.alipaydev.com/gateway.do","2016100100639757","MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCrZ9a0eKa7A69PFVliegB8+fL95PXSriLKZzlqCW5tI0N9kK2IXCvxNAqWeCURUwDS/DHcEburNy4gqNb2numQ3L19UBSsrjedUEiPel7aD7xhZ0/rLfLiAtFwVg49vx8N4e/K/sItYExHngWcQOJfAXvYL++s0oEgi3COG6gc9Il+k+mkr/ZXEJJa3KjCDEXc/951vqb0TrCRKrsTck6R3cwIx0NwPcCD2UW4qrR7LFIkSpnlN2VlFArtbgzYo7uSOCGGbYA55Mp46+5JhGXj099wHB65dbOChfd69ipCFIUb1bXBsFvrrrzPNRfVqnRLrzX0st+5XigOnMnawPsXAgMBAAECggEAYgZ4m62EbN+mmquy+sabEN5BQUJ384zzCJ4QD7xlErVgG6dqFtnfp4oDpBoB2O6HQiJIwXrCwSBFmRf2GAS5vsb+93Oc30SjOax4UtXA+SSWr33ODsmwZeEGtiAA9P8gW2gjIwyfMCQqe6q7uqjAzWSj1vdTMpsQjfQw4pSeiC/Auw/tYgrfNI+nfq00d2Iw/7gq6sUvasB6Uy5+rDEJrwArtLsWsHFocXJ//Wtiig8jF18B1C9iSbWDUey5CTIHoPFoeIa0cQVaQfoIpLi0DUd8zIpntkBx9w6fRDmyTnsOJSYyQJixGnKBbPuD/PkL+XC44ABVVR9SrlENetajgQKBgQDn7hCrRlDXQBgVzzaQtL2DjZ2wv4WZqrAXLFuHBXVJHgA2pjrMICGCbzTNJqfblG5MRFywPn03IDIYGvy4EvFaN13L2IszSq0w7F4ypMKw/Vcdth5TkRPeFjyUBom4Qv2ZICidzaI+r0SVdh0qFz+GOXWt1uIza290SeWqu4p7/wKBgQC9McIUVcp8Qk9XIHZ6mH2/upWGRGbhyueDIRoNBYVQa/wAO27TlHenP3ZJzAS0lOBbnnQNQaFiLAWKg23sT1/5S5m81L2L/mVCLigifT2vZS3WvIKnsLbNHuiuyYzNvW1jphONY2V51WOR9YBKCBiTyOrGrVpH8Y4KgIz3Xsng6QKBgBW4zELeUQ/35smUIK4o318E0nINXfvoapmhA+l/b16ruqVjqDoaAElxFObp6eq82h+2f1/aWmvhTg/H50duLsCeUuCWpWLmaZYdbS0dFUbyU0hrGCgHfG7eckoC2r8xNwMyadttuPZZ//hnmY+pob92fjkL2R+rvkKL8UCuU+gNAoGAFptWfIWPz29A+cE4RwfnprxN16BexsYvCb0v1rR9LJHqp8BySrfn4EU2BjVproZHlMsHRPo9pXRrKRTxUpZdenVtJmIpUAwS32iShSR/9XBanNkjp5P3E+WImjIdMAxdgNW8al2hJ4C3CB7UuIPlI+uTGjBmpEgZ1+lqbriV2NkCgYEA55ObS+2c61L87E17dtxFGs8WALkmjFirJl1zkQbX76aCG95JMh4gVgcqDaiCdmJaVrqXMfURvrjvlYTHN1Nq2wwe7tFLSjk1RnaLk8Kroj2w3YnoheFVTe6pJOSy72wUFYKUxmIOpb1mC3y6wQGzpe30pC0m8To1YmU0nyJaV+A=","json","utf-8","MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAoZ3+mbVnwkmY52WZycPOQu5cqGzJQDouv/72NekDr/lxbYq0Okmg8Mnfezw06M5WhmO3XkxvPYkb08pH8HrR1PovLdUpNib52v41Zv2d1HUyfqjZGULzaCyOJhBQnTeCu9/cPRcBqSTMe500sJThQ+KMBQi/ZVQRYGaepLaWVzFCM14qnO6qBwubV2bZP3O5UHxNgVJqWd+JNAQnV50K3rmbqjF11R9PkntczMOteZZDTu/8sdXsqFbxQ33AUPlprxxD00X6vRTCIsOakYPP5dsBwoVMu33L7w3Ir4qAZbcZnGcjeF8dlY1a1uptHYNb0VWAWw1+dEQFRj3doSeGZQIDAQAB","RSA2");
            AlipayTradeQueryRequest request = new AlipayTradeQueryRequest();
            request.setBizContent("{" +
                    "    \"out_trade_no\":\""+oid+"\"" +
                    "  }");
            AlipayTradeQueryResponse response = null;
            try {
                response = alipayClient.execute(request);
            } catch (AlipayApiException e) {
                e.printStackTrace();
            }
            if(response.isSuccess()){
                System.out.println("调用成功");
                String res = response.getTradeStatus();
                if (res.equals("TRADE_SUCCESS")){
                    changeOrderStatus(oid,Common.ORDER_STATUS_2);
                    return true;
                }else {
                    return false;
                }
            } else {
                System.out.println("调用失败");
                return false;
            }
        }

        return false;

    }


    @Override
    public void changeOrderStatus(String oid, String status) {
        System.out.println(oid);
        //改list表
        OrderListExample ole = new OrderListExample();
        ole.createCriteria().andOrderIdEqualTo(oid);
        OrderList ol = new OrderList();
        ol.setStatus(status);
        Date now = new Date();
        //修改状态需要更新时间
        switch (status){
            case Common.ORDER_STATUS_2:
                ol.setCreatetime2(now);
                break;
            case Common.ORDER_STATUS_3:
                ol.setCreatetime3(now);
                break;
            case Common.ORDER_STATUS_4:
                ol.setCreatetime4(now);
                break;
            case Common.ORDER_STATUS_5:
                ol.setCreatetime5(now);
                break;
        }
        olm.updateByExampleSelective(ol,ole);

        //改detail表
        OderDetailExample ode = new OderDetailExample();
        ode.createCriteria().andOrderIdEqualTo(oid);
        OderDetail od = new OderDetail();
        od.setOrderStatus(status);
        odm.updateByExampleSelective(od,ode);
    }


    @Override
    public List<OrderList> getAllOrderByUserId(String user_id) {
        OrderListExample ole = new OrderListExample();
        ole.createCriteria().andUserIdEqualTo(user_id);
        ole.setOrderByClause("createtime1 desc");
        return olm.selectByExample(ole);
    }

    @Override
    public List<OderDetail> getAllDetailByOrderId(String oid) {
        OderDetailExample ode = new OderDetailExample();
        ode.createCriteria().andOrderIdEqualTo(oid);
        return odm.selectByExample(ode);
    }

    @Override
    public RecAddress getRecAddressById(String id) {
        return ram.selectByPrimaryKey(Integer.valueOf(id));
    }
}
