package pri.service.order;

import cn.hutool.json.JSONObject;
import pri.pojo.Cart;
import pri.pojo.CartInfo;

import java.util.List;

public interface CartService {
    List<Cart> getCartList(String user_id);

    List<CartInfo> getCartInfoByList(List<Cart> c_list);

    float getTotalByCartList(List<CartInfo> c_info_list);

    boolean deleteProductById(String pro_id,String user_id);

    boolean updateProductByIdAndCount(JSONObject jsonObject, String user_id);

    boolean addProductById(String pro_id,String user_id);

    boolean mergeSessionToDb(List<Cart> carts,String user_id);

    boolean clearCart(String user_id);

}
