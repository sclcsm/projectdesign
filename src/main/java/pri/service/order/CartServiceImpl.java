package pri.service.order;

import cn.hutool.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.core.convert.converter.ConverterFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pri.dao.CartMapper;
import pri.dao.ProductImageMapper;
import pri.dao.ProductMapper;
import pri.pojo.*;

import java.util.ArrayList;
import java.util.List;

@Service()
@Transactional
public class CartServiceImpl implements CartService{
    @Autowired
    private CartMapper cm;
    @Autowired
    private ProductMapper pm;
    @Autowired
    private ProductImageMapper pi;

    @Override
    public List<CartInfo> getCartInfoByList(List<Cart> c_list) {
        List<CartInfo> cil = new ArrayList<>();
        for (Cart c:c_list){
            CartInfo ci = new CartInfo();
            //
            ProductExample pe = new ProductExample();
            pe.createCriteria().andProductIdEqualTo(Integer.valueOf(c.getProductId()));
            List<Product> pl = pm.selectByExample(pe);
            //
            Product p = pl.get(0);
            ci.setProduct_name(p.getName());
            ci.setPrice(String.valueOf(p.getPrice()));
            //
            ProductImageExample pie = new ProductImageExample();
            pie.createCriteria().andProductIdEqualTo(Integer.valueOf(c.getProductId()));
            List<ProductImage> pil = pi.selectByExample(pie);
            ProductImage pi1 = pil.get(0);
            ci.setThumbnail(pi1.getUrl());
            //
            ci.setCount(c.getCount());
            ci.setSum(c.getCount()*p.getPrice());
            ci.setProduct_id(c.getProductId());
            cil.add(ci);
        }
        return cil;
    }

    @Override
    public float getTotalByCartList(List<CartInfo> c_info_list) {
        float total = 0;
        for (CartInfo ci:c_info_list) {
            total+=ci.getSum();
        }
        return total;
    }

    @Override
    public List<Cart> getCartList(String user_id) {
        CartExample ce = new CartExample();
        ce.createCriteria().andUserIdEqualTo(user_id);
        return cm.selectByExample(ce);
    }

    @Override
    public boolean deleteProductById(String pro_id,String user_id) {
        CartExample ce = new CartExample();
        ce.createCriteria().andProductIdEqualTo(pro_id).andUserIdEqualTo(user_id);
        int res = cm.deleteByExample(ce);
        if (res!=0){
            //删除成功
            return true;
        }else {
            //删除失败
            return false;
        }
    }

    @Override
    public boolean updateProductByIdAndCount(JSONObject jsonObject, String user_id) {
        int res = 0;
        for (String key:jsonObject.keySet()) {
            Cart c = new Cart();
            c.setCount(Integer.valueOf((String) jsonObject.get(key)));
            CartExample ce = new CartExample();
            ce.createCriteria().andUserIdEqualTo(user_id).andProductIdEqualTo(key);
            res = cm.updateByExampleSelective(c,ce);
        }
        if (res==0){
            return false;
        }else {
            return true;
        }
    }

    @Override
    public boolean addProductById(String pro_id, String user_id) {
        CartExample ce = new CartExample();
        ce.createCriteria().andUserIdEqualTo(user_id).andProductIdEqualTo(pro_id);
        List<Cart> cl = cm.selectByExample(ce);
        if (cl.isEmpty()){
            //如果购物车无此商品
            Cart c = new Cart();
            c.setProductId(pro_id);
            c.setCount(1);
            c.setUserId(user_id);
            int res = cm.insertSelective(c);
            if (res!=0){
                return true;
            }else {
                return false;
            }
        }else {
            //如果已经有此商品
            Cart c = cl.get(0);
            c.setCount(c.getCount()+1);
            int res = cm.updateByPrimaryKey(c);
            if (res!=0){
                return true;
            }else {
                return false;
            }
        }
    }

    @Override
    public boolean mergeSessionToDb(List<Cart> carts, String user_id) {
        for (Cart c:carts) {
            //合并时分为两种情况
            CartExample ce = new CartExample();
            ce.createCriteria().andProductIdEqualTo(c.getProductId()).andUserIdEqualTo(user_id);
            List<Cart> cl = cm.selectByExample(ce);
            if (cl == null || cl.isEmpty()){
                //第一种是购物车没有本产品
                //就与用户相绑定
                c.setUserId(user_id);
                int res = cm.insertSelective(c);
            }else{
                //第二种是购物车有本产品
                //添加一个数量就行了
                Cart cc = cl.get(0);
                cc.setCount(cc.getCount()+c.getCount());
                int res = cm.updateByPrimaryKey(cc);
            }
        }
        return true;
    }

    @Override
    public boolean clearCart(String user_id) {
        CartExample ce = new CartExample();
        ce.createCriteria().andUserIdEqualTo(user_id);
        int res = cm.deleteByExample(ce);
        if (res==0){
            return false;
        }else {
            return true;
        }

    }
}
