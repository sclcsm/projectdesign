package pri.service.Login;

import pri.pojo.User;

import java.util.List;

public interface LoginService {
    User selectByUserNameAndPassword(User user);
    //用户注册
    boolean insertUser(User user);
    boolean isExist(String email);
    int updateUser(User user);
    //    发送验证码
    String getVerify(String email);
    List<User> findAll();



}
