package pri.service.Login;

import cn.hutool.extra.mail.MailUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pri.common.Common;
import pri.dao.UserMapper;
import pri.pojo.User;
import pri.pojo.UserExample;

import java.util.List;

@Service
public class LoginServiceImpl implements LoginService{
   @Autowired
    private UserMapper userMapper;

    //登录验证
    @Override
    public User selectByUserNameAndPassword(User user) {
        UserExample userExample=new UserExample();
        userExample.createCriteria().andUserNameEqualTo(user.getUserName()).andPasswordEqualTo(user.getPassword());
        List<User> users= userMapper.selectByExample(userExample);
        return (users.isEmpty()||users==null)?null:users.get(0);
//        return  null;
    }
    //注册用户，信息插入
    @Override
    public boolean insertUser(User user) {
        int res = userMapper.insertSelective(user);
        if (res!=0){
            return true;
        }else {
            return false;
        }
    }

//该邮箱是否已经被注册
    @Override
    public boolean isExist(String email) {
        UserExample userExample=new UserExample();
        userExample.createCriteria().andEmailEqualTo(email);
        List<User> users = userMapper.selectByExample(userExample);
        System.out.println(users);
        return users.isEmpty()?true:false;
    }

    @Override
    public int updateUser(User user) {
        UserExample userExample=new UserExample();
        userExample.createCriteria().andUserNameEqualTo(user.getUserName()).andPasswordEqualTo(user.getPassword());
        user.setPassword(user.getPassword());
        return userMapper.updateByExampleSelective(user,userExample);
    }


    @Override
    public String getVerify(String email) {
        String code=Common.getUUID(6);
        MailUtil.send(email, "SBT注册验证码", "<!DOCTYPE html>\n" +
                "<html class=\" js flexbox canvas canvastext webgl no-touch geolocation postmessage websqldatabase indexeddb hashchange history draganddrop websockets rgba hsla multiplebgs backgroundsize borderimage borderradius boxshadow textshadow opacity cssanimations csscolumns cssgradients cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage sessionstorage webworkers applicationcache svg inlinesvg smil svgclippaths js flexbox canvas canvastext webgl no-touch geolocation postmessage websqldatabase indexeddb hashchange history draganddrop websockets rgba hsla multiplebgs backgroundsize borderimage borderradius boxshadow textshadow opacity cssanimations csscolumns cssgradients cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage sessionstorage webworkers applicationcache svg inlinesvg smil svgclippaths\" lang=\"zxx\" style=\"\"><head>\n" +
                "    <meta charset=\"utf-8\">\n" +
                "    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n" +
                "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n" +
                "    <meta name=\"description\" content=\"meta description\">\n" +
                "\n" +
                "    <title>Product Compare</title>\n" +
                "\n" +
                "    <!--=== Favicon ===-->\n" +
                "<link rel=\"shortcut icon\" href=\"http://sbt.sclcsm.com/assets/img/favicon.ico\" type=\"image/x-icon\">\n" +
                "\n" +
                "<!--== Google Fonts ==-->\n" +
                "<link href=\"https://fonts.googleapis.com/css?family=Poppins:300,400,400i,500,600,700\" rel=\"stylesheet\">\n" +
                "\n" +
                "<!--=== Bootstrap CSS ===-->\n" +
                "<link href=\"http://sbt.sclcsm.com/assets/css/vendor/bootstrap.min.css\" rel=\"stylesheet\">\n" +
                "<!--=== Font-Awesome CSS ===-->\n" +
                "<link href=\"http://sbt.sclcsm.com/assets/css/vendor/font-awesome.css\" rel=\"stylesheet\">\n" +
                "<!--=== Plugins CSS ===-->\n" +
                "<link href=\"http://sbt.sclcsm.com/assets/css/plugins.css\" rel=\"stylesheet\">\n" +
                "<!--=== Helper CSS ===-->\n" +
                "<link href=\"http://sbt.sclcsm.com/assets/css/helper.min.css\" rel=\"stylesheet\">\n" +
                "<!--=== Main Style CSS ===-->\n" +
                "<link href=\"http://sbt.sclcsm.com/assets/css/style.css\" rel=\"stylesheet\">\n" +
                "\n" +
                "<!-- Modernizer JS -->\n" +
                "<script src=\"http://sbt.sclcsm.com/assets/js/vendor/modernizr-2.8.3.min.js\"></script>\n" +
                "\n" +
                "    <!--[if lt IE 9]>\n" +
                "    <script src=\"http://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js\"></script>\n" +
                "    <script src=\"http://oss.maxcdn.com/respond/1.4.2/respond.min.js\"></script>\n" +
                "    <![endif]-->\n" +
                "</head>\n" +
                "\n" +
                "<body>\n" +
                "\n" +
                " \n" +
                "<!--== Start Header Section ==-->\n" +
                "<header id=\"header-area\">\n" +
                "    <!-- Start PreHeader Area -->\n" +
                "    \n" +
                "    <!-- End PreHeader Area -->\n" +
                "\n" +
                "    <!-- Start Header Middle Area -->\n" +
                "    \n" +
                "    <!-- End Header Middle Area -->\n" +
                "\n" +
                "    <!-- Start Main Menu Area -->\n" +
                "    \n" +
                "    <!-- End Main Menu Area -->\n" +
                "</header>\n" +
                "<!--== End Header Section ==-->\n" +
                "\n" +
                "<!--== Start Page Breadcrumb ==-->\n" +
                "\n" +
                "<!--== End Page Breadcrumb ==-->\n" +
                "\n" +
                "<!--== Page Content Wrapper Start ==-->\n" +
                "<div id=\"page-content-wrapper\">\n" +
                "    <div class=\"container\">\n" +
                "        <div class=\"member-area-from-wrap\" style=\"\n" +
                "    /* margin: auto; */\n" +
                "\">\n" +
                "            <div class=\"row\" style=\"\n" +
                "    /* margin: auto; */\n" +
                "\">\n" +
                "                <!-- Login Content Start -->\n" +
                "                <div class=\"col-lg-5\" style=\"\n" +
                "    margin: auto;\n" +
                "\">\n" +
                "                    <div class=\"login-reg-form-wrap  pr-lg-50\" style=\"\n" +
                "    text-align: center;\n" +
                "\">\n" +
                "                        <h2>您的验证码为:</h2>\n" +
                "<h2>"+code+"</h2>\n" +
                "\n" +
                "                        \n" +
                "                    </div>\n" +
                "                </div>\n" +
                "                <!-- Login Content End -->\n" +
                "\n" +
                "                <!-- Register Content Start -->\n" +
                "                \n" +
                "                <!-- Register Content End -->\n" +
                "            </div>\n" +
                "        </div>\n" +
                "    </div>\n" +
                "</div>\n" +
                "<!--== Page Content Wrapper End ==-->\n" +
                "\n" +
                "<!--== Start Newsletter Area ==-->\n" +
                "\n" +
                "<!--== End Newsletter Area ==-->\n" +
                "\n" +
                "<!--== Start Footer Area ==-->\n" +
                "<footer id=\"footer-area\">\n" +
                "    <!-- Footer Widget Area Start -->\n" +
                "    \n" +
                "    <!-- Footer Widget Area End -->\n" +
                "\n" +
                "    <!-- Footer Bottom Area -->\n" +
                "    \n" +
                "    <!-- Footer Bottom Area -->\n" +
                "</footer>\n" +
                "<!--== End Footer Area ==-->\n" +
                "\n" +
                "<!-- Scroll to Top Start -->\n" +
                "<a href=\"#\" class=\"scrolltotop\"><i class=\"fa fa-angle-up\"></i></a>\n" +
                "<!-- Scroll to Top End -->\n" +
                "\n" +
                "<!--== Product Quick View Modal Area Wrap ==-->\n" +
                "<div class=\"modal\" id=\"quickView\" tabindex=\"-1\" role=\"dialog\">\n" +
                "    <div class=\"modal-dialog modal-dialog-centered\" role=\"document\">\n" +
                "        <div class=\"modal-content\">\n" +
                "            <div class=\"modal-body\">\n" +
                "                <div class=\"quick-view-content single-product-page-content\">\n" +
                "                    <div class=\"row\">\n" +
                "                        <!-- Product Thumbnail Start -->\n" +
                "                        <div class=\"col-lg-5 col-md-6\">\n" +
                "                            <div class=\"product-thumbnail-wrap\">\n" +
                "                                <div class=\"product-thumb-carousel owl-carousel owl-loaded owl-drag\">\n" +
                "                                    \n" +
                "\n" +
                "                   \n" +
                "                    </div>\n" +
                "                </div>\n" +
                "            </div>\n" +
                "        </div>\n" +
                "    </div>\n" +
                "</div>\n" +
                "<!--== Product Quick View Modal Area End ==-->\n" +
                "\n" +
                "\n" +
                "<!--=======================Javascript============================-->\n" +
                "<!--=== Jquery Min Js ===-->\n" +
                "<script src=\"http://sbt.sclcsm.com/assets/js/vendor/jquery-3.3.1.min.js\"></script>\n" +
                "<!--=== Jquery Migrate Min Js ===-->\n" +
                "<script src=\"http://sbt.sclcsm.com/assets/js/vendor/jquery-migrate-1.4.1.min.js\"></script>\n" +
                "<!--=== Popper Min Js ===-->\n" +
                "<script src=\"http://sbt.sclcsm.com/assets/js/vendor/popper.min.js\"></script>\n" +
                "<!--=== Bootstrap Min Js ===-->\n" +
                "<script src=\"http://sbt.sclcsm.com/assets/js/vendor/bootstrap.min.js\"></script>\n" +
                "<!--=== Ajax Mail Js ===-->\n" +
                "<script src=\"http://sbt.sclcsm.com/assets/js/ajax-mail.js\"></script>\n" +
                "<!--=== Plugins Min Js ===-->\n" +
                "<script src=\"http://sbt.sclcsm.com/assets/js/plugins.js\"></script>\n" +
                "\n" +
                "<!--=== Active Js ===-->\n" +
                "<script src=\"http://sbt.sclcsm.com/assets/js/active.js\"></script>\n" +
                "\n" +
                "\n" +
                "</body></html>", true);
        return code;
    }

    @Override
    public List<User> findAll() {

        return userMapper.selectByExample(new UserExample());
    }


//    查询所有用户


}
