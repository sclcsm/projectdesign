package pri.service.product;

import org.springframework.ui.Model;
import pri.pojo.Category;
import pri.pojo.Page;
import pri.pojo.ProductComment;
import pri.pojo.ProductInfo;

import java.util.List;

public interface ProductService {
    void getHome(Model model,String userId);

    Integer add(ProductInfo product);
    boolean delete(Integer id);
    boolean update(ProductInfo product);

    int countByExample(ProductInfo product);

    ProductInfo findById(Integer id,String userId);
    List<ProductInfo> findByExample(ProductInfo product, Page page);

    Integer collection(Integer productId,String userId);

    Integer cancelCollection(Integer productId, String userId);

    List<ProductInfo> getCollections(String userId,int number);

    List<Category> hotCategory();

    ProductInfo transQuery(ProductInfo old,ProductInfo newo);

    List<ProductInfo> mastLook();

    void addHeat(Integer productId,int number);

    Integer addComment(ProductComment productComment, String userId);
}
