package pri.pojo;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public class ProductInfo {
    private Integer productId;

    private String name;

    private Float price;

    private Float concessionalPrice;

    private Integer stock;

    private String depict;

    private Integer categoryId;

    private String createTime;

    private Integer stateId;

    private String msg;

    private String updateTime;

    private String color;

    private Integer type;

    private String detail;


    private String categoryName;

    private List<ProductInfo> otherColorProducts;

    private List<ProductImage> images;

    private List<ProductComment> productComments;

    private Map<String,Integer> productCommentsByScore;

    private BigDecimal avgScore;

    private Integer xing;

    private Float lowPrice;

    private Float highPrice;

    private Integer orderBy;

    private Integer isCollection;

    private  Integer canComent;

    private Integer isCart;

    private String userId;


    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public Integer getCanComent() {
        return canComent;
    }

    public void setCanComent(Integer canComent) {
        this.canComent = canComent;
    }

    public Integer getXing() {
        return xing;
    }

    public void setXing(Integer xing) {
        this.xing = xing;
    }

    public BigDecimal getAvgScore() {
        return avgScore;
    }

    public void setAvgScore(BigDecimal avgScore) {
        this.avgScore = avgScore;
    }

    public List<ProductComment> getProductComments() {
        return productComments;
    }

    public void setProductComments(List<ProductComment> productComments) {
        this.productComments = productComments;
    }

    public Map<String, Integer> getProductCommentsByScore() {
        return productCommentsByScore;
    }

    public void setProductCommentsByScore(Map<String, Integer> productCommentsByScore) {
        this.productCommentsByScore = productCommentsByScore;
    }

    public List<ProductInfo> getOtherColorProducts() {
        return otherColorProducts;
    }

    public void setOtherColorProducts(List<ProductInfo> otherColorProducts) {
        this.otherColorProducts = otherColorProducts;
    }

    public Integer getIsCart() {
        return isCart;
    }

    public void setIsCart(Integer isCart) {
        this.isCart = isCart;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Integer getIsCollection() {
        return isCollection;
    }

    public void setIsCollection(Integer isCollection) {
        this.isCollection = isCollection;
    }

    public Integer getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(Integer orderBy) {
        this.orderBy = orderBy;
    }

    public List<ProductImage> getImages() {
        return images;
    }

    public void setImages(List<ProductImage> images) {
        this.images = images;
    }

    public Float getLowPrice() {
        return lowPrice;
    }

    public void setLowPrice(Float lowPrice) {
        this.lowPrice = lowPrice;
    }

    public Float getHighPrice() {
        return highPrice;
    }

    public void setHighPrice(Float highPrice) {
        this.highPrice = highPrice;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Float getConcessionalPrice() {
        return concessionalPrice;
    }

    public void setConcessionalPrice(Float concessionalPrice) {
        this.concessionalPrice = concessionalPrice;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public String getDepict() {
        return depict;
    }

    public void setDepict(String depict) {
        this.depict = depict == null ? null : depict.trim();
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime == null ? null : createTime.trim();
    }

    public Integer getStateId() {
        return stateId;
    }

    public void setStateId(Integer stateId) {
        this.stateId = stateId;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg == null ? null : msg.trim();
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime == null ? null : updateTime.trim();
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color == null ? null : color.trim();
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail == null ? null : detail.trim();
    }

    @Override
    public String toString() {
        return "ProductInfo{" +
                "productId=" + productId +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", concessionalPrice=" + concessionalPrice +
                ", stock=" + stock +
                ", depict='" + depict + '\'' +
                ", categoryId=" + categoryId +
                ", createTime='" + createTime + '\'' +
                ", stateId=" + stateId +
                ", msg='" + msg + '\'' +
                ", updateTime='" + updateTime + '\'' +
                ", color='" + color + '\'' +
                ", type=" + type +
                ", detail='" + detail + '\'' +
                ", images=" + images +
                ", lowPrice=" + lowPrice +
                ", highPrice=" + highPrice +
                '}';
    }
}
