package pri.pojo;

public class CartInfo {
    private String thumbnail; //缩略图
    private String product_name;//产品名
    private String price; //价格
    private int count; //数量
    private float sum;//
    private String product_id;

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    @Override
    public String toString() {
        return "CartInfo{" +
                "thumbnail='" + thumbnail + '\'' +
                ", product_name='" + product_name + '\'' +
                ", price='" + price + '\'' +
                ", count=" + count +
                ", sum=" + sum +
                '}';
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public float getSum() {
        return sum;
    }

    public void setSum(float sum) {
        this.sum = sum;
    }
}
