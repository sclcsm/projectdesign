package pri.pojo;

public class OderDetail {
    private Integer id;

    private String orderId;

    private String productName;

    private String productAttr;

    private Integer productCount;

    private String orderStatus;

    private String expressNum;

    private String expressComId;

    private String recAddress;

    private String discountId;

    private Float productOriPrice;

    private Float productFinPrice;

    private String productId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId == null ? null : orderId.trim();
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName == null ? null : productName.trim();
    }

    public String getProductAttr() {
        return productAttr;
    }

    public void setProductAttr(String productAttr) {
        this.productAttr = productAttr == null ? null : productAttr.trim();
    }

    public Integer getProductCount() {
        return productCount;
    }

    public void setProductCount(Integer productCount) {
        this.productCount = productCount;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus == null ? null : orderStatus.trim();
    }

    public String getExpressNum() {
        return expressNum;
    }

    public void setExpressNum(String expressNum) {
        this.expressNum = expressNum == null ? null : expressNum.trim();
    }

    public String getExpressComId() {
        return expressComId;
    }

    public void setExpressComId(String expressComId) {
        this.expressComId = expressComId == null ? null : expressComId.trim();
    }

    public String getRecAddress() {
        return recAddress;
    }

    public void setRecAddress(String recAddress) {
        this.recAddress = recAddress == null ? null : recAddress.trim();
    }

    public String getDiscountId() {
        return discountId;
    }

    public void setDiscountId(String discountId) {
        this.discountId = discountId == null ? null : discountId.trim();
    }

    public Float getProductOriPrice() {
        return productOriPrice;
    }

    public void setProductOriPrice(Float productOriPrice) {
        this.productOriPrice = productOriPrice;
    }

    public Float getProductFinPrice() {
        return productFinPrice;
    }

    public void setProductFinPrice(Float productFinPrice) {
        this.productFinPrice = productFinPrice;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId == null ? null : productId.trim();
    }
}