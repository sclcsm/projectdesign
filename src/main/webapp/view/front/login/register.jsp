<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="meta description">
    <title>注册页面</title>
    <!--=== Favicon ===-->
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/assets/img/favicon.ico" type="image/x-icon"/>
    <!--== Google Fonts ==-->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,400i,500,600,700" rel="stylesheet">
    <!--=== Bootstrap CSS ===-->
    <link href="${pageContext.request.contextPath}/assets/css/vendor/bootstrap.min.css" rel="stylesheet">
    <!--=== Font-Awesome CSS ===-->
    <link href="${pageContext.request.contextPath}/assets/css/vendor/font-awesome.css" rel="stylesheet">
    <!--=== Plugins CSS ===-->
    <link href="${pageContext.request.contextPath}/assets/css/plugins.css" rel="stylesheet">
    <!--=== Helper CSS ===-->
    <link href="${pageContext.request.contextPath}/assets/css/helper.min.css" rel="stylesheet">
    <!--=== Main Style CSS ===-->
    <link href="${pageContext.request.contextPath}/assets/css/style.css" rel="stylesheet">
    <!-- Modernizer JS -->
    <script src="${pageContext.request.contextPath}/assets/js/vendor/modernizr-2.8.3.min.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js/login/jquery-1.10.2.min.js"></script>

</head>
<body >
<%@include file="../head.jsp"%>

<div id="page-content-wrapper">
    <div class="container">
        <div class="member-area-from-wrap" style="position: relative">
            <div class="alert alert-success" role="alert" id="successMsg" style="display: none;position: absolute;width: 600px;text-align: center;left: 0;right: 0;top: 5px;margin: auto"></div>
            <div class="alert alert-danger" role="alert" id="errorMsg" style="display: none;position: absolute;width: 600px;text-align: center;left: 0;right: 0;top: 5px;margin: auto"></div>
            <div class="row">
                <div class="col-lg-7 mt-30 mt-lg-0" style="margin: auto;">
                    <div class="login-reg-form-wrap" style="text-align: center;">
                        <h2>Singup Form</h2>
                        <form  method="post">
                            <div class="single-input-item">
                                <input  type="number" placeholder="请输入您的登录账号" name="userName" id="userName" required/>
                            </div>



                            <div class="single-input-item">
                                <input type="password" placeholder="请输入你的密码" name="password" id="password" required />
                            </div>

                            <div class="single-input-item">
                                <input  type="text" placeholder="请输入您的昵称" name="nickName" id="nickName" required/>
                            </div>

                            <div class="single-input-item">
                                <input  type="email" placeholder="请输入您的邮箱" name="email"  id="email"/>
                            </div>


                            <div class="single-input-item" style="float: left;width: 380px" >
                                <input  type="number" placeholder="请输入验证码" name="code" id="code"/>
                            </div>

                            <div class="" style="float: left;margin: 0;width: 115px">
                                <div class="single-input-item">
                                    <input type="button" onclick="send()"  value="发送验证码" id="emid"/>
                                </div>

                            </div>

                            <%--<div class="row">--%>
                            <%--<div class="col-lg-6">--%>
                            <%--<div class="single-input-item">--%>
                            <%--<input type="text" placeholder="请输入验证码"  />--%>
                            <%--</div>--%>
                            <%--</div>--%>

                            <div class="single-input-item">
                                <button class="btn" type="button" style="margin-top: 30px;width: 500px" id="reg">注册</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<%@include file="../foot.jsp"%>
</body>
<!--=======================Javascript============================-->
<!--=== Jquery Min Js ===-->
<script src="${pageContext.request.contextPath}/assets/js/vendor/jquery-3.3.1.min.js"></script>
<!--=== Jquery Migrate Min Js ===-->
<script src="${pageContext.request.contextPath}/assets/js/vendor/jquery-migrate-1.4.1.min.js"></script>
<!--=== Popper Min Js ===-->
<script src="${pageContext.request.contextPath}/assets/js/vendor/popper.min.js"></script>
<!--=== Bootstrap Min Js ===-->
<script src="${pageContext.request.contextPath}/assets/js/vendor/bootstrap.min.js"></script>
<!--=== Ajax Mail Js ===-->
<script src="${pageContext.request.contextPath}/assets/js/ajax-mail.js"></script>
<!--=== Plugins Min Js ===-->
<script src="${pageContext.request.contextPath}/assets/js/plugins.js"></script>
<!--=== Active Js ===-->
<script src="${pageContext.request.contextPath}/assets/js/active.js"></script>

<script type="text/javascript" >
    var time = 0;
    var handle = 0;
    function send(){
        $('#emid').attr("disabled",true);
        time = 60;
        handle = setInterval("djs()",1000);

        $.ajax({
            //请求方式
            type : "POST",
            //请求地址
            url : "${pageContext.request.contextPath}/login/verify",
            //数据，json字符串
            data : {data:$("#email").val()},
            dataType:"json",
            //请求成功
            success : function(result) {
                if (result.code == 0){
                    showSuccess("发送成功，请检查邮箱！");
                } else {
                    showError("该邮箱已经被注册!")
                }
            },
            //请求失败，包含具体的错误信息
            error : function(e){
                showError("发送失败，请检查网络！");
            }
        });
    }

    

    function djs() {
        var now = --time;
        if (now <= 0){
            clearInterval(handle);
            $("#emid").val("发送验证码");
            $('#emid').attr("disabled",false);

        }else {
            $('#emid').val(now+"s后重发");
        }

    }

    $(".btn").click(function () {
        $("#reg").attr("disabled",true);
        console.log("ojbk");
        var obj = {"code":$("#code").val(),"username":$("#userName").val(),"password":$("#password").val(),"email":$("#email").val(),"nickname":$("#nickName").val()};
        $.ajax({
            url:"${pageContext.request.contextPath}/login/doRegister",
            type:"post",
            data:{content:JSON.stringify(obj)},
            dataType:"json",
            success:function(data){
                if(data.code=="1"){
                    showSuccess("注册成功！");
                    window.location.href="${pageContext.request.contextPath}/product/home";
                }
                else{
                    $("#reg").attr("disabled",false);
                    showError("验证码不正确！");
                }
            },
            error:function(){
                $("#reg").attr("disabled",false);
                showError("	网络繁忙！");
            }
        });
    });

    function showSuccess(msg) {
        $("#successMsg").html(msg);
        $("#successMsg").fadeIn();
        setTimeout(function () {
            $("#successMsg").fadeOut();
        },3000);
    }

    function showError(msg) {
        $("#errorMsg").html(msg);
        $("#errorMsg").fadeIn();
        setTimeout(function () {
            $("#errorMsg").fadeOut();
        },3000);
    }
</script>
</html>
