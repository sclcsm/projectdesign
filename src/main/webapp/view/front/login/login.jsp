<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>


<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="meta description">
    <title>登录页面</title>
    <!--=== Favicon ===-->
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/assets/img/favicon.ico" type="image/x-icon"/>
    <!--== Google Fonts ==-->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,400i,500,600,700" rel="stylesheet">
    <!--=== Bootstrap CSS ===-->
    <link href="${pageContext.request.contextPath}/assets/css/vendor/bootstrap.min.css" rel="stylesheet">
    <!--=== Font-Awesome CSS ===-->
    <link href="${pageContext.request.contextPath}/assets/css/vendor/font-awesome.css" rel="stylesheet">
    <!--=== Plugins CSS ===-->
    <link href="${pageContext.request.contextPath}/assets/css/plugins.css" rel="stylesheet">
    <!--=== Helper CSS ===-->
    <link href="${pageContext.request.contextPath}/assets/css/helper.min.css" rel="stylesheet">
    <!--=== Main Style CSS ===-->
    <link href="${pageContext.request.contextPath}/assets/css/style.css" rel="stylesheet">
    <!-- Modernizer JS -->
    <script src="${pageContext.request.contextPath}/assets/js/vendor/modernizr-2.8.3.min.js"></script>
</head>
<body>
<%@include file="../head.jsp"%>




    <%--登录界面--%>
<div id="page-content-wrapper">
    <div class="container">
        <div class="member-area-from-wrap">
            <div class="row">
                <!-- Login Content Start -->
                <div class="col-lg-5" style="margin: auto;">
                    <div class="login-reg-form-wrap  pr-lg-50" style="text-align: center;">
                        <h2>Already a Member?</h2>

                        <form  method="post">
                            <div class="single-input-item">
                                <%----%>
                                <input required="" type="text" placeholder=" userName" name="userName" id="userName" >
                            </div>

                            <div class="single-input-item">
                                <input required="" type="password" placeholder="Enter your Password" name="password" id="password" required />
                            </div>

                            <div class="single-input-item">
                                <div class="login-reg-form-meta d-flex align-items-center justify-content-between">
                                    <div class="remember-meta">
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input" id="rememberMe"  name="rememberMe" type="checkbox">
                                            <label class="custom-control-label" for="rememberMe" >Remember Me</label>
                                        </div>
                                    </div>


                                </div>
                            </div>

                            <div class="single-input-item">
                                <button class="btn" type="button" onclick="login()">登录</button>
                                <button class="btn" type="button" onclick="window.location.href='${pageContext.request.contextPath}/login/register'">注册账号</button>

                            </div>
                        </form>
                    </div>
                </div>
                <!-- Login Content End -->

                <!-- Register Content Start -->

                <!-- Register Content End -->
            </div>
        </div>
    </div>
</div>




<%@include file="../foot.jsp"%>
</body>
<!--=======================Javascript============================-->
<!--=== Jquery Min Js ===-->
<script src="${pageContext.request.contextPath}/assets/js/vendor/jquery-3.3.1.min.js"></script>
<!--=== Jquery Migrate Min Js ===-->
<script src="${pageContext.request.contextPath}/assets/js/vendor/jquery-migrate-1.4.1.min.js"></script>
<!--=== Popper Min Js ===-->
<script src="${pageContext.request.contextPath}/assets/js/vendor/popper.min.js"></script>
<!--=== Bootstrap Min Js ===-->
<script src="${pageContext.request.contextPath}/assets/js/vendor/bootstrap.min.js"></script>
<!--=== Ajax Mail Js ===-->
<script src="${pageContext.request.contextPath}/assets/js/ajax-mail.js"></script>
<!--=== Plugins Min Js ===-->
<script src="${pageContext.request.contextPath}/assets/js/plugins.js"></script>
<!--=== Active Js ===-->
<script src="${pageContext.request.contextPath}/assets/js/active.js"></script>

    <script type="text/javascript" charset="UTF-8">
        function login() {
// 	编写函数,在按键升起时触发,监测cookie中是否存在该用户名的key,如果有,则把value赋值给密码框
                    var userName = $("#userName").val();
                    var password = $("#password").val();
                    var rememberMe = $("input[name='rememberMe']").is(":checked");//是够勾选记住密码
                    var obj = {"userName": userName, "password": password, "rememberMe":rememberMe} ;
                    $.ajax({
                        url: "${pageContext.request.contextPath}/login/doLogin",
                        type: "POST",
                        async: true,
                        data: {data:JSON.stringify(obj)},
                        dataType: "json",
                        success:function (data) {
                            if (data.code=="0") {
                                alert("用户名或者密码错误");
                            }
                            else {
                                window.location.href="${pageContext.request.contextPath}/product/home";
                            }
                        },
                       error:function () {
                           alert("网络异常");
                       }
                }
            )

        };



</script>

</html>
