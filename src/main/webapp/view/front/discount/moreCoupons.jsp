<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <%--<meta name="viewport" content="width=device-width, initial-scale=1">--%>
    <meta name="description" content="meta description">
    <title>查询优惠券</title>
    <!--=== Favicon ===-->
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/assets/img/favicon.ico" type="image/x-icon"/>
    <!--== Google Fonts ==-->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,400i,500,600,700" rel="stylesheet">
    <!--=== Bootstrap CSS ===-->
    <link href="${pageContext.request.contextPath}/assets/css/vendor/bootstrap.min.css" rel="stylesheet">
    <!--=== Font-Awesome CSS ===-->
    <link href="${pageContext.request.contextPath}/assets/css/vendor/font-awesome.css" rel="stylesheet">
    <!--=== Plugins CSS ===-->
    <link href="${pageContext.request.contextPath}/assets/css/plugins.css" rel="stylesheet">
    <!--=== Helper CSS ===-->
    <link href="${pageContext.request.contextPath}/assets/css/helper.min.css" rel="stylesheet">
    <!--=== Main Style CSS ===-->
    <link href="${pageContext.request.contextPath}/assets/css/style.css" rel="stylesheet">
    <!-- Modernizer JS -->
    <script src="${pageContext.request.contextPath}/assets/js/vendor/modernizr-2.8.3.min.js"></script>



    <meta content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" name="viewport"/>
    <meta content="yes" name="apple-mobile-web-app-capable"/>
    <meta content="black" name="apple-mobile-web-app-status-bar-style"/>
    <meta content="telephone=no" name="format-detection"/>
    <link href="${pageContext.request.contextPath}/assets/css/discount/style.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="${pageContext.request.contextPath}/assets/js/discount/jquery.min.js"></script>

</head>
<body>
<%@include file="/view/front/head.jsp"%>



<div>
    <section class="aui-flexView">
        <header class="aui-navBar aui-navBar-fixed">
            <a href="javascript:;" class="aui-navBar-item">
                <a class="icon icon-return" href="/product/detail/${productId}"></a>
            </a>
            <div class="aui-center">
                <span class="aui-center-title">优惠券</span>
                <div class="alert alert-success" role="alert" id="successMsg"
                     style="display: none;position: absolute;height: 65px;width: 600px;text-align: center;padding: 18px;font-size: 30px;"></div>
            </div>



            <a href="javascript:;" class="aui-navBar-item">
                <i class="icon icon-more"></i>
            </a>
        </header>
        <section class="aui-scrollView">
            <div class="aui-tab" data-ydui-tab>
                <div class="tab-panel">
                    <div class="tab-panel-item tab-active">
                        <div class="tab-item">
                            <c:forEach var="coupon" items="${list1}">
                                <c:choose>
                                    <c:when test="${user!=null && coupon.userId == user.userId}">
                                        <div class="tab-item-list tab-item-list-default">
                                            <div class="aui-flex">
                                                <div class="aui-left-change">
                                                    <h2>
                                                        <em>￥</em>
                                                            ${coupon.preferentialSum}

                                                    </h2>
                                                    <p>
                                                        <c:choose>
                                                            <c:when test="${coupon.scope!=0}">
                                                                满${coupon.minimumConsumptionSum}元可用
                                                            </c:when>
                                                            <c:otherwise>
                                                                特殊商品除外
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </p>
                                                </div>
                                                <div class="aui-flex-box aui-flex-box-two">
                                                        <span class="aui-icon-used">
                                                            <img src="${pageContext.request.contextPath}/assets/img/discount/icon-used.png" alt="">
                                                        </span>
                                                    <h3>
                                                        <em>券票</em>
                                                            ${coupon.target}

                                                    </h3>
                                                    <div class="aui-flex aui-flex-bor-bom">
                                                        <div class="aui-flex-box aui-flex-box-flow">${coupon.startTime}-${coupon.deadline}</div>
                                                    </div>
                                                    <div class="aui-flex aui-flex-show">
                                                        <div class="aui-flex-box">详细信息</div>
                                                        <div class="aui-show-btn">
                                                            <i class="icon fa fa-angle1-right" data-placement="top" data-toggle="popover" data-content="${coupon.detail}" ></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="aui-show-box">
                                                <p>默认隐藏内容</p>
                                            </div>
                                        </div>

                                    </c:when>
                                    <c:otherwise>
                                        <c:choose>
                                            <c:when test="${coupon.scope!=0}">
                                                <div class="tab-item-list">
                                                    <div class="aui-flex">
                                                        <div class="aui-left-change">
                                                            <h2 >
                                                                <em>￥</em>
                                                                    ${coupon.preferentialSum}

                                                            </h2>
                                                            <p>满${coupon.minimumConsumptionSum}元可用</p>
                                                            <div class="aui-icon-circle"></div>
                                                        </div>
                                                        <div class="aui-flex-box aui-flex-box-two">
                                                            <h3>
                                                                <em>券票</em>
                                                                    ${coupon.target}

                                                            </h3>
                                                            <div class="aui-flex aui-flex-bor-bom">
                                                                <div class="aui-flex-box aui-flex-box-flow">${coupon.startTime}-${coupon.deadline}</div>
                                                                <a onclick="getCoupon(${coupon.couponId})"  class="aui-btn-use" >立即领取</a>
                                                            </div>
                                                            <div class="aui-flex aui-flex-show">
                                                                <div class="aui-flex-box">详细信息</div>
                                                                <div class="aui-show-btn">
                                                                    <i class="icon fa fa-angle1-right" data-placement="top" data-toggle="popover" data-content="${coupon.detail}"></i>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="aui-show-box">
                                                        <p>默认隐藏内容</p>
                                                    </div>
                                                </div>
                                            </c:when>
                                            <c:otherwise>
                                                <div class="tab-item-list tab-item-list-green">
                                                    <div class="aui-flex">
                                                        <div class="aui-left-change">
                                                            <h2>
                                                                <em>￥</em>
                                                                    ${coupon.preferentialSum}

                                                            </h2>
                                                            <p>特殊商品除外</p>
                                                            <div class="aui-icon-circle"></div>
                                                        </div>
                                                        <div class="aui-flex-box aui-flex-box-two">
                                                            <h3>
                                                                <em>运费券</em>
                                                                    ${coupon.target}

                                                            </h3>
                                                            <div class="aui-flex aui-flex-bor-bom">
                                                                <div class="aui-flex-box aui-flex-box-flow">${coupon.startTime}-${coupon.deadline}</div>
                                                                <a onclick="getCoupon(${coupon.couponId})"  class="aui-btn-use" >立即领取</a>
                                                            </div>
                                                            <div class="aui-flex">
                                                                <div class="aui-flex-box">详细信息</div>
                                                                <div class="aui-show-btn">
                                                                    <i class="icon fa fa-angle1-right" data-placement="top" data-toggle="popover" data-content="${coupon.detail}"></i>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="aui-show-box">
                                                        <p>默认隐藏内容</p>
                                                    </div>
                                                </div>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:otherwise>
                                </c:choose>

                            </c:forEach>
                        </div>
                    </div>

                </div>
            </div>
        </section>
    </section>
</div>

<script type="text/javascript">

    $(function (){
        $("[data-toggle='popover']").popover();
    });

    function showSuccess(msg) {
        $("#successMsg").html(msg);
        $("#successMsg").fadeIn();
        setTimeout(function () {
            $("#successMsg").fadeOut();
        },800);
    }

    function getCoupon(couponId) {
        //console.log(${productId});
        if (${user==null}) {
            //console.log("ojbk!");
            showSuccess("客官别急，请登录取后再领取哦！")
        }else {

            window.location.href = "/coupon/getCoupon/"+couponId+"/${productId}?page=showCoupons";
        }
    }


</script>


<%@include file="/view/front/foot.jsp"%>
</body>
<!--=======================Javascript============================-->
<!--=== Jquery Min Js ===-->
<script src="${pageContext.request.contextPath}/assets/js/vendor/jquery-3.3.1.min.js"></script>
<!--=== Jquery Migrate Min Js ===-->
<script src="${pageContext.request.contextPath}/assets/js/vendor/jquery-migrate-1.4.1.min.js"></script>
<!--=== Popper Min Js ===-->
<script src="${pageContext.request.contextPath}/assets/js/vendor/popper.min.js"></script>
<!--=== Bootstrap Min Js ===-->
<script src="${pageContext.request.contextPath}/assets/js/vendor/bootstrap.min.js"></script>
<!--=== Ajax Mail Js ===-->
<script src="${pageContext.request.contextPath}/assets/js/ajax-mail.js"></script>
<!--=== Plugins Min Js ===-->
<script src="${pageContext.request.contextPath}/assets/js/plugins.js"></script>
<!--=== Active Js ===-->
<script src="${pageContext.request.contextPath}/assets/js/active.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/assets/js/discount/tab.js"></script>
</html>
