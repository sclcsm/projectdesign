<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>

<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="meta description">
    <title>订单详情</title>
    <!--=== Favicon ===-->
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/assets/img/favicon.ico" type="image/x-icon"/>
    <!--== Google Fonts ==-->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,400i,500,600,700" rel="stylesheet">
    <!--=== Bootstrap CSS ===-->
    <link href="${pageContext.request.contextPath}/assets/css/vendor/bootstrap.min.css" rel="stylesheet">
    <!--=== Font-Awesome CSS ===-->
    <link href="${pageContext.request.contextPath}/assets/css/vendor/font-awesome.css" rel="stylesheet">
    <!--=== Plugins CSS ===-->
    <link href="${pageContext.request.contextPath}/assets/css/plugins.css" rel="stylesheet">
    <!--=== Helper CSS ===-->
    <link href="${pageContext.request.contextPath}/assets/css/helper.min.css" rel="stylesheet">
    <!--=== Main Style CSS ===-->
    <link href="${pageContext.request.contextPath}/assets/css/style.css" rel="stylesheet">
    <!-- Modernizer JS -->
    <script src="${pageContext.request.contextPath}/assets/js/vendor/modernizr-2.8.3.min.js"></script>
    <style>
        .divcss1{font-family: 方正粗黑宋简体;font-size: 20px}
        .divcss2{font-family: 仿宋}
        canvas {
            cursor: crosshair;
            display: block;
            position:absolute;
        }
    </style>
</head>
<body>
<%@include file="../head.jsp"%>



<!--== Page Content Wrapper Start ==-->
<div id="page-content-wrapper">
    <div class="container">
        <!-- Cart Page Content Start -->
        <div class="row">
            <div class="col-lg-12">
                <!--联系人框-->
                <div class="cart-table table-responsive" style="margin-bottom: 30px;border: 2px solid #9add42;word-break: break-all;position: relative">
                    <canvas id="canvas">Canvas is not supported in your browser.</canvas>
                    <div style="float:left;width: 226px;">
                        <div style="background-color: #9add42;height: 53px;line-height: 53px;color: white;font-size: 1.5rem;padding-left: 10px;text-transform: uppercase;font-weight:700">订单信息</div>
                        <div style="padding: 10px 20px;">
                            <ul>
                                <li style="margin-bottom: 10px">
                                    <div style="display: inline-block;font-weight: 900"><span>收货地址：</span></div>
                                    <div><span>${recAddress.country},${recAddress.province},${recAddress.city},${recAddress.street},${recAddress.detail},${recAddress.recName},${recAddress.recPhone}</span></div>
                                </li>
                                <li style="margin-bottom: 10px">
                                    <div style="display: inline-block;font-weight: 900"><span>订单备注：</span></div>
                                    <div><span>老板优惠一点</span></div>
                                </li>
                                <li style="margin-bottom: 10px">
                                    <div style="display: inline-block;font-weight: 900"><span>订单编号：</span></div>
                                    <div><span>${orderList[0].orderId}</span></div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div style="float: left;border-left: 2px solid #9add42;padding-left: 250px;">
                        <div class="divcss1" style="margin-top:  50px;margin-left: -150px;margin-bottom: 20px;"><strong>订单状态：${orderList[0].orderStatus}</strong></div>
                        <div class="divcss2" style="left: 300px;top: 120px;margin-bottom: 20px;margin-left: -100px;">
                            物流：已到达四川轻化工大学 <br>
                            已到达宜宾分配站<br>
                            已到达成都分配站<br>
                            已从杭州出发，发往成都分配站<br>
                            已从义务出发，发往 杭州<br>
                        </div>

                        <c:if test="${orderList[0].orderStatus == '待支付'}">
                            <div style="left: 300px;top: 250px;margin-bottom: 30px;margin-left: -150px;">您可以   <button type="button" class="btn btn-default" onclick="payTheOrder(${orderList[0].orderId})">支付订单</button></div>
                        </c:if>
                        <c:if test="${orderList[0].orderStatus == '待发货'}">
                            <div style="left: 300px;top: 250px;margin-bottom: 30px;margin-left: -150px;">您可以   <button type="button" class="btn btn-default">提醒发货</button></div>
                        </c:if>
                        <c:if test="${orderList[0].orderStatus == '待确认'}">
                            <div style="left: 300px;top: 250px;margin-bottom: 30px;margin-left: -150px;">您可以   <button type="button" class="btn btn-default" onclick="confirm('${orderList[0].orderId}')">确认收货</button></div>
                        </c:if>
                        <c:if test="${orderList[0].orderStatus == '未评价'}">
                            <div style="left: 300px;top: 250px;margin-bottom: 30px;margin-left: -150px;">您可以   <button type="button" class="btn btn-default" onclick="window.location.href='${pageContext.request.contextPath}/order/mark?orderId=${orderList[0].orderId}&id=${orderList[0].productId}'">评价</button></div>
                        </c:if>
                        <c:if test="${orderList[0].orderStatus == '已完成'}">
                            <div style="left: 300px;top: 250px;margin-bottom: 30px;margin-left: -150px;">您可以   <button type="button" class="btn btn-default">给程序猿点个赞</button></div>
                            <script src="${pageContext.request.contextPath}/assets/js/yanhua.js"></script>
                        </c:if>

                    </div>

                </div>



                <!-- Cart Table Area -->
                <div class="cart-table table-responsive">
                    <table class="table table-bordered" style="white-space: normal">
                        <thead>
                        <tr>
                            <th class="pro-title">商品</th>
                            <th class="pro-title">属性</th>
                            <th class="pro-price">单价</th>
                            <th class="pro-quantity">数量</th>
                            <th class="pro-subtotal">优惠</th>
                            <th class="pro-remove">状态</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach var="p" items="${orderList}">
                            <tr>
                                <td class="pro-title"><a href="#">${p.productName}</a></td>
                                <td class="pro-attr"><span>${p.productAttr}</span></td>
                                <td class="pro-price"><span>${p.productOriPrice}</span></td>
                                <td class="pro-subtotal"><span>${p.productCount}</span></td>
                                <td class="pro-remove"><span>${p.discountId}</span></td>
                                <td class="pro-remove"><span>${p.orderStatus}</span></td>
                            </tr>
                        </c:forEach>

                        </tbody>
                    </table>
                </div>

            </div>
        </div>

        <div class="row">
            <div class="col-lg-6 ml-auto">
                <!-- Cart Calculation Area -->
                <div class="cart-calculator-wrapper">
                    <h3>Cart Totals</h3>
                    <div class="cart-calculate-items">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <tr>
                                    <td>商品总价</td>
                                    <td>￥${ptotal}</td>
                                </tr>
                                <tr>
                                    <td>运费</td>
                                    <td>￥20</td>
                                </tr>
                                <tr>
                                    <td>总价</td>
                                    <td class="total-amount">￥${ptotal+20}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Cart Page Content End -->
    </div>
</div>
<!--== Page Content Wrapper End ==-->

<!--模态框-->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">请勿关闭此页面</h4>
            </div>
            <div class="modal-body">
                <p id="payres">请等待支付结果...</p>
            </div>
            <div class="modal-footer">
                <a href="${pageContext.request.contextPath}/order/myorder">
                    <button type="button" class="btn btn-primary" id="ifsuccess" disabled="disabled" style="background-color: gray;color: black">已支付成功但无结果返回？</button>
                </a>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<%@include file="../foot.jsp"%>
</body>
<!--=======================Javascript============================-->
<!--=== Jquery Min Js ===-->
<script src="${pageContext.request.contextPath}/assets/js/vendor/jquery-3.3.1.min.js"></script>
<!--=== Jquery Migrate Min Js ===-->
<script src="${pageContext.request.contextPath}/assets/js/vendor/jquery-migrate-1.4.1.min.js"></script>
<!--=== Popper Min Js ===-->
<script src="${pageContext.request.contextPath}/assets/js/vendor/popper.min.js"></script>
<!--=== Bootstrap Min Js ===-->
<script src="${pageContext.request.contextPath}/assets/js/vendor/bootstrap.min.js"></script>
<!--=== Ajax Mail Js ===-->
<script src="${pageContext.request.contextPath}/assets/js/ajax-mail.js"></script>
<!--=== Plugins Min Js ===-->
<script src="${pageContext.request.contextPath}/assets/js/plugins.js"></script>
<!--=== Active Js ===-->
<script src="${pageContext.request.contextPath}/assets/js/active.js"></script>

<script>
    var handle = 0;
    var times = 0;
    var com = 1;
    function payTheOrder(oid) {
        $("#pay").html("支付中...");
        $("#myModal").modal({backdrop: 'static', keyboard: false});  //手动开启
        var url = "${pageContext.request.contextPath}/order/pay?oid="+oid+"&com="+com;
        window.open(url);
        times = 0;
        handle = setInterval(function () {
            inqueryRes(oid);
        },3000);
    }

    //要查看第三方支付调用的情况 每三秒请求一次
    function inqueryRes(oid) {
        times++;
        if (times == 3){
            $("#ifsuccess").attr("disabled",false);
            $("#ifsuccess").removeAttr("style");
        }

        $.ajax({
            //请求方式
            type : "POST",
            //请求地址
            url : "${pageContext.request.contextPath}/order/payinquery",
            //数据，json字符串
            data : {oid:oid,com:com},
            dataType:"json",
            //请求成功
            success : function(result) {
                if (result.code == 0){
                    clearInterval(handle);
                    $("#payres").html("支付成功！请等待跳转页面");
                    sleep(3000);
                    window.location.reload();
                }
            },
            //请求失败，包含具体的错误信息
            error : function(e){
                console.log(e.status);
                console.log(e.responseText);
            }
        });

        function sleep(delay) {
            var start = (new Date()).getTime();
            while ((new Date()).getTime() - start < delay) {
                continue;
            }
        }
    }


    function confirm(oid) {
        $.ajax({
            //请求方式
            type : "POST",
            //请求地址
            url : "${pageContext.request.contextPath}/order/confirm",
            //数据，json字符串
            data : {oid:oid},
            dataType:"json",
            //请求成功
            success : function(result) {
                console.log("-------------");
                window.location.reload();
            },
            //请求失败，包含具体的错误信息
            error : function(e){
                console.log(e.status);
                console.log(e.responseText);
            }
        });
    }

</script>
</html>
