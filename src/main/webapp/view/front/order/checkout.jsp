<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="meta description">
    <title>确认订单</title>
    <!--=== Favicon ===-->
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/assets/img/favicon.ico" type="image/x-icon"/>
    <!--== Google Fonts ==-->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,400i,500,600,700" rel="stylesheet">
    <!--=== Bootstrap CSS ===-->
    <link href="${pageContext.request.contextPath}/assets/css/vendor/bootstrap.min.css" rel="stylesheet">
    <!--=== Font-Awesome CSS ===-->
    <link href="${pageContext.request.contextPath}/assets/css/vendor/font-awesome.css" rel="stylesheet">
    <!--=== Plugins CSS ===-->
    <link href="${pageContext.request.contextPath}/assets/css/plugins.css" rel="stylesheet">
    <!--=== Helper CSS ===-->
    <link href="${pageContext.request.contextPath}/assets/css/helper.min.css" rel="stylesheet">
    <!--=== Main Style CSS ===-->
    <link href="${pageContext.request.contextPath}/assets/css/style.css" rel="stylesheet">
    <!-- Modernizer JS -->
    <script src="${pageContext.request.contextPath}/assets/js/vendor/modernizr-2.8.3.min.js"></script>
</head>
<body>
<%@include file="../head.jsp"%>



<!--== Page Content Wrapper Start ==-->
<div id="page-content-wrapper">
    <div class="container">
        <!--== Checkout Page Content Area ==-->
        <div class="row">
            <div class="col-12">

            </div>
        </div>

        <div class="row">
            <!-- Checkout Billing Details -->
            <div class="col-lg-6">
                <div class="checkout-billing-details-wrap">
                    <h2>收货地址</h2>
                    <div class="billing-form-wrap">
                        <form action="#">
                            <div class="single-input-item">
                                <label for="state" class="required">省份</label>
                                <input type="text" id="state"  placeholder="省份" class="myRequired"/>
                            </div>

                            <div class="single-input-item">
                                <label for="town" class="required">城市</label>
                                <input type="text" id="town"  placeholder="城市" class="myRequired"/>
                            </div>



                            <div class="single-input-item">
                                <label for="street-address" class="required">街道</label>
                                <input type="text" id="street-address" placeholder="街道地址" class="myRequired"/>
                            </div>

                            <div class="single-input-item">
                                <label for="detail" >详细地址</label>
                                <input type="text" id="detail" placeholder="详细地址"/>
                            </div>


                            <div class="single-input-item">
                                <label for="f_name" class="required">联系人</label>
                                <input type="text" id="f_name" placeholder="联系人" class="myRequired"/>
                            </div>

                            <div class="single-input-item">
                                <label for="phone" class="required">联系方式</label>
                                <input type="text" id="phone"  placeholder="联系方式" class="myRequired"/>
                            </div>

                            <div class="single-input-item">
                                <label for="ordernote">订单备注</label>
                                <textarea name="ordernote" id="ordernote" cols="30" rows="3" placeholder="订单备注"></textarea>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <!-- Order Summary Details -->
            <div class="col-lg-6 mt-5 mt-lg-0">
                <div class="order-summary-details">
                    <h2>你的订单列表</h2>
                    <div class="order-summary-content">
                        <!-- Order Summary Table -->
                        <div class="order-summary-table table-responsive text-center">
                            <table class="table table-bordered" style="white-space: normal;">
                                <thead>
                                <tr>
                                    <th>商品列表</th>
                                    <th>总价</th>
                                </tr>
                                </thead>
                                <tbody>
                                <c:forEach var="p" items="${requestScope.realcartlist}">
                                <tr>
                                    <td><a href="http://www.baidu.com?wx=${p.product_id}">${p.product_name}<strong> × ${p.count}</strong></a></td>
                                    <td>￥${p.sum}</td>
                                </tr>
                                </c:forEach>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <td>商品总价</td>
                                    <td><strong>￥${requestScope.total}</strong></td>
                                </tr>

                                <tr>
                                    <td>运费</td>
                                    <td>
                                        <strong>￥20</strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td>应付款</td>
                                    <td><strong>￥${requestScope.total+20}</strong></td>
                                </tr>
                                </tfoot>
                            </table>
                        </div>

                        <!-- Order Payment Method -->
                        <div class="order-payment-method">
                            <div class="single-payment-method show">
                                <div class="payment-method-name">
                                    <div class="custom-control custom-radio">
                                        <input type="radio" id="alipayment" name="paymentmethod" data-id="1" value="alipay" class="custom-control-input"  checked/>
                                        <label class="custom-control-label" for="alipayment">支付宝支付<img src="https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1562596116818&di=1e6b99e56c03e6d8ba5d1994e50388e4&imgtype=0&src=http%3A%2F%2Fupload.gfan.com%2F2017%2F0110%2F20170110054600748.jpg" class="img-fluid paypal-card" alt="Paypal" /></label>
                                    </div>
                                </div>
                                <div class="payment-method-details" data-method="alipay">
                                    <p>通过支付宝支付你的订单</p>
                                </div>
                            </div>

                            <div class="single-payment-method">
                                <div class="payment-method-name">
                                    <div class="custom-control custom-radio">
                                        <input type="radio" id="weixinpayment" name="paymentmethod" data-id="2" value="weixinpay" class="custom-control-input"/>
                                        <label class="custom-control-label" for="weixinpayment">微信支付<img src="https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1562596261798&di=22e04207796c5955055f9bcea8d89290&imgtype=0&src=http%3A%2F%2Fhbimg.b0.upaiyun.com%2Fbd55f584925b0613fc57e93a5fab6a9beef11b8dc268-bHtvZL_fw658" class="img-fluid paypal-card" alt="Paypal" /></label>
                                    </div>
                                </div>
                                <div class="payment-method-details" data-method="weixinpay">
                                    <p>通过微信支付你的订单</p>
                                </div>
                            </div>

                            <div class="summary-footer-area">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="terms" required="">
                                    <label class="custom-control-label" for="terms">我已经阅读并同意<a href="index.html">网站协议</a></label>
                                </div>

                                <a href="javascript:void(0)" class="btn" id="pay">支付</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--== Checkout Page Content End ==-->
    </div>
</div>
<!--== Page Content Wrapper End ==-->

<!--模态框-->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">请勿关闭此页面</h4>
            </div>
            <div class="modal-body">
                <p id="payres">请等待支付结果...</p>
            </div>
            <div class="modal-footer">
                <a href="${pageContext.request.contextPath}/order/myorder">
                    <button type="button" class="btn btn-primary" id="ifsuccess" disabled="disabled" style="background-color: gray;color: black">已支付成功但无结果返回？</button>
                </a>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<%@include file="../foot.jsp"%>
</body>
<!--=======================Javascript============================-->
<!--=== Jquery Min Js ===-->
<script src="${pageContext.request.contextPath}/assets/js/vendor/jquery-3.3.1.min.js"></script>
<!--=== Jquery Migrate Min Js ===-->
<script src="${pageContext.request.contextPath}/assets/js/vendor/jquery-migrate-1.4.1.min.js"></script>
<!--=== Popper Min Js ===-->
<script src="${pageContext.request.contextPath}/assets/js/vendor/popper.min.js"></script>
<!--=== Bootstrap Min Js ===-->
<script src="${pageContext.request.contextPath}/assets/js/vendor/bootstrap.min.js"></script>
<!--=== Ajax Mail Js ===-->
<script src="${pageContext.request.contextPath}/assets/js/ajax-mail.js"></script>
<!--=== Plugins Min Js ===-->
<script src="${pageContext.request.contextPath}/assets/js/plugins.js"></script>
<!--=== Active Js ===-->
<script src="${pageContext.request.contextPath}/assets/js/active.js"></script>

<script>

    var handle = 0;
    var times = 0;

    $(document).ready(function () {
        $("#pay").bind("click",function () {
            if (!$("#terms").is(":checked")) {
                alert("请先同意协议");
                return false;
            }

            if (!hasEmpty()){
                addOrder();
            }


        })
    });

    function hasEmpty() {
        var flag = false;
        $(".myRequired").each(function () {
            if($(this).val() == ""){
                alert("必填项不能为空！");
                flag = true;
                return false;
            }
        });
        return flag;
    }
    var com = 0;
    function addOrder() {
        $("#pay").html("支付中...");
        $("#myModal").modal({backdrop: 'static', keyboard: false});  //手动开启


        $(".custom-control-input").each(function () {
            if ($(this).is(":checked")) {
                com = $(this).attr("data-id");
                return false;
            }
        });

        var province = $("#state").val();
        var city = $("#town").val();
        var street = $("#street-address").val();
        var detail = $("#detail").val();
        var rec_name = $("#f_name").val();
        var rec_phone = $("#phone").val();
        var ordernote = $("#ordernote").val();


        var obj = {"province":province,"city":city,"street":street,"detail":detail,"rec_name":rec_name,"rec_phone":rec_phone,"ordernote":ordernote,"com":com};

        $.ajax({
            //请求方式
            type : "POST",
            //请求地址
            url : "${pageContext.request.contextPath}/order/add",
            //数据，json字符串
            data : {content:JSON.stringify(obj)},
            dataType:"json",
            //请求成功
            success : function(result) {
                if (result.code == 0){
                    //每三秒查询一次
                    //初始化查询次数
                    times = 0;
                    handle = setInterval(function () {
                        inqueryRes(result.oid);
                    },3000);
                    window.open(result.msg);
                } else {
                    console.log(result.msg);
                }
            },
            //请求失败，包含具体的错误信息
            error : function(e){
                console.log(e.status);
                console.log(e.responseText);
            }
        });
    }

    //要查看第三方支付调用的情况 每三秒请求一次
    function inqueryRes(oid) {
        times++;
        if (times == 3){
            $("#ifsuccess").attr("disabled",false);
            $("#ifsuccess").removeAttr("style");
        }
        $.ajax({
            //请求方式
            type : "POST",
            //请求地址
            url : "${pageContext.request.contextPath}/order/payinquery",
            //数据，json字符串
            data : {oid:oid,com:com},
            dataType:"json",
            //请求成功
            success : function(result) {
                if (result.code == 0){
                    clearInterval(handle);
                    $("#payres").html("支付成功！请等待跳转页面");
                    sleep(3000);
                    window.location.href = "${pageContext.request.contextPath}/order/myorder";
                }
            },
            //请求失败，包含具体的错误信息
            error : function(e){
                console.log(e.status);
                console.log(e.responseText);
            }
        });

        function sleep(delay) {
            var start = (new Date()).getTime();
            while ((new Date()).getTime() - start < delay) {
                continue;
            }
        }
    }
</script>
</html>
