<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="meta description">
    <title>购物车</title>
    <!--=== Favicon ===-->
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/assets/img/favicon.ico" type="image/x-icon"/>
    <!--== Google Fonts ==-->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,400i,500,600,700" rel="stylesheet">
    <!--=== Bootstrap CSS ===-->
    <link href="${pageContext.request.contextPath}/assets/css/vendor/bootstrap.min.css" rel="stylesheet">
    <!--=== Font-Awesome CSS ===-->
    <link href="${pageContext.request.contextPath}/assets/css/vendor/font-awesome.css" rel="stylesheet">
    <!--=== Plugins CSS ===-->
    <link href="${pageContext.request.contextPath}/assets/css/plugins.css" rel="stylesheet">
    <!--=== Helper CSS ===-->
    <link href="${pageContext.request.contextPath}/assets/css/helper.min.css" rel="stylesheet">
    <!--=== Main Style CSS ===-->
    <link href="${pageContext.request.contextPath}/assets/css/style.css" rel="stylesheet">
    <!-- Modernizer JS -->
    <script src="${pageContext.request.contextPath}/assets/js/vendor/modernizr-2.8.3.min.js"></script>
</head>
<body>
<%@include file="../head.jsp"%>

<!--== Page Content Wrapper Start ==-->
<div id="page-content-wrapper">
    <div class="container">
        <!-- Cart Page Content Start -->
        <div class="row">
            <div class="col-lg-12">
                <!-- Cart Table Area -->
                <div class="cart-table table-responsive">
                    <table class="table table-bordered" style="white-space: normal">
                        <thead>
                        <tr>
                            <th class="pro-thumbnail">缩略图</th>
                            <th class="pro-title">产品名称</th>
                            <th class="pro-price">单价</th>
                            <th class="pro-quantity">数量</th>
                            <th class="pro-subtotal">总价</th>
                            <th class="pro-remove">删除</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach var="p" items="${sessionScope.cartlist}">
                            <tr class="itor">
                                <td class="pro-thumbnail"><a href="http://www.baidu.com?wx=${p.product_id}"><img class="img-fluid" src="${p.thumbnail}" alt="Product"/></a></td>
                                <td class="pro-title"><a href="http://www.baidu.com?wx=${p.product_id}">${p.product_name}</a></td>
                                <td class="pro-price"><span>￥${p.price}</span></td>
                                <td class="pro-quantity">
                                    <div class="pro-qty"><input type="text" value="${p.count}" data-id="${p.product_id}"></div>
                                </td>
                                <td class="pro-subtotal"><span>￥${p.sum}</span></td>
                                <td class="pro-remove"><a  data-id="${p.product_id}"><i class="fa fa-trash-o"></i></a></td>
                            </tr>
                        </c:forEach>

                        <%--<tr>--%>
                            <%--<td class="pro-thumbnail"><a href="#"><img class="img-fluid" src="${pageContext.request.contextPath}/assets/img/product-1.jpg"--%>
                                                                       <%--alt="Product"/></a></td>--%>
                            <%--<td class="pro-title"><a href="#">Zeon Zen 4 Pro</a></td>--%>
                            <%--<td class="pro-price"><span>$295.00</span></td>--%>
                            <%--<td class="pro-quantity">--%>
                                <%--<div class="pro-qty"><input type="text" value="1"></div>--%>
                            <%--</td>--%>
                            <%--<td class="pro-subtotal"><span>$295.00</span></td>--%>
                            <%--<td class="pro-remove"><a href="#"><i class="fa fa-trash-o"></i></a></td>--%>
                        <%--</tr>--%>
                        <%--<tr>--%>
                            <%--<td class="pro-thumbnail"><a href="#"><img class="img-fluid" src="${pageContext.request.contextPath}/assets/img/product-2.jpg"--%>
                                                                       <%--alt="Product"/></a></td>--%>
                            <%--<td class="pro-title"><a href="#">Aquet Drone D 420</a></td>--%>
                            <%--<td class="pro-price"><span>$275.00</span></td>--%>
                            <%--<td class="pro-quantity">--%>
                                <%--<div class="pro-qty"><input type="text" value="2"></div>--%>
                            <%--</td>--%>
                            <%--<td class="pro-subtotal"><span>$550.00</span></td>--%>
                            <%--<td class="pro-remove"><a href="#"><i class="fa fa-trash-o"></i></a></td>--%>
                        <%--</tr>--%>
                        <%--<tr>--%>
                            <%--<td class="pro-thumbnail"><a href="#"><img class="img-fluid" src="${pageContext.request.contextPath}/assets/img/product-3.jpg"--%>
                                                                       <%--alt="Product"/></a></td>--%>
                            <%--<td class="pro-title"><a href="#">Game Station X 22</a></td>--%>
                            <%--<td class="pro-price"><span>$295.00</span></td>--%>
                            <%--<td class="pro-quantity">--%>
                                <%--<div class="pro-qty"><input type="text" value="1"></div>--%>
                            <%--</td>--%>
                            <%--<td class="pro-subtotal"><span>$295.00</span></td>--%>
                            <%--<td class="pro-remove"><a href="#"><i class="fa fa-trash-o"></i></a></td>--%>
                        <%--</tr>--%>
                        <%--<tr>--%>
                            <%--<td class="pro-thumbnail"><a href="#"><img class="img-fluid" src="${pageContext.request.contextPath}/assets/img/product-4.jpg"--%>
                                                                       <%--alt="Product"/></a></td>--%>
                            <%--<td class="pro-title"><a href="#">Roxxe Headphone Z 75 </a></td>--%>
                            <%--<td class="pro-price"><span>$110.00</span></td>--%>
                            <%--<td class="pro-quantity">--%>
                                <%--<div class="pro-qty"><input type="text" value="1"></div>--%>
                            <%--</td>--%>
                            <%--<td class="pro-subtotal"><span>$110.00</span></td>--%>
                            <%--<td class="pro-remove"><a href="#"><i class="fa fa-trash-o"></i></a></td>--%>
                        <%--</tr>--%>
                        </tbody>
                    </table>
                </div>

                <!-- Cart Update Option -->
                <div class="cart-update-option d-block" style="text-align: right">
                    <div class="cart-update">
                        <a  href="javascript:void(0)" class="btn">更新购物车</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6 ml-auto">
                <!-- Cart Calculation Area -->
                <div class="cart-calculator-wrapper">
                    <h3>Cart Totals</h3>
                    <div class="cart-calculate-items">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <tr>
                                    <td>商品总价</td>
                                    <td>￥${total}</td>
                                </tr>
                                <tr>
                                    <td>运费</td>
                                    <td>￥20</td>
                                </tr>
                                <tr>
                                    <td>总价</td>
                                    <td class="total-amount">￥${total+20}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <a href="${pageContext.request.contextPath}/order/pre" class="btn">结算</a>
                </div>
            </div>
        </div>
        <!-- Cart Page Content End -->
    </div>
</div>
<!--== Page Content Wrapper End ==-->



<%@include file="../foot.jsp"%>
</body>
<!--=======================Javascript============================-->
<!--=== Jquery Min Js ===-->
<script src="${pageContext.request.contextPath}/assets/js/vendor/jquery-3.3.1.min.js"></script>
<!--=== Jquery Migrate Min Js ===-->
<script src="${pageContext.request.contextPath}/assets/js/vendor/jquery-migrate-1.4.1.min.js"></script>
<!--=== Popper Min Js ===-->
<script src="${pageContext.request.contextPath}/assets/js/vendor/popper.min.js"></script>
<!--=== Bootstrap Min Js ===-->
<script src="${pageContext.request.contextPath}/assets/js/vendor/bootstrap.min.js"></script>
<!--=== Ajax Mail Js ===-->
<script src="${pageContext.request.contextPath}/assets/js/ajax-mail.js"></script>
<!--=== Plugins Min Js ===-->
<script src="${pageContext.request.contextPath}/assets/js/plugins.js"></script>
<!--=== Active Js ===-->
<script src="${pageContext.request.contextPath}/assets/js/active.js"></script>
</html>

<script>
    $(document).ready(function () {
        $(".pro-remove").children("a").click(function () {
            //执行删除操作
            deleteById($(this).attr("data-id"));
        });

        $(".cart-update").children("a").click(function () {
            //执行更新操作
            updateCart();
        });
    });

    function deleteById(id) {
            $.ajax({
                //请求方式
                type : "POST",
                //请求地址
                url : "${pageContext.request.contextPath}/cart/delete",
                //数据，json字符串
                data : {id:id},
                dataType:"json",
                //请求成功
                success : function(result) {
                    if (result.code == 0){
                        window.location.reload();
                    } else {
                        console.log(result.msg);
                    }
                },
                //请求失败，包含具体的错误信息
                error : function(e){
                    console.log(e.status);
                    console.log(e.responseText);
                }
            });
    }


    function updateCart() {
        var json = '{';
        $(".itor").each(function () {
            var count = $(this).find("input").attr("value");
            var pro_id = $(this).find("input").attr("data-id");
            json+='"'+pro_id+'":"'+count+'",';
        });

        json = json.substr(0,json.length-1);
        json+='}';

        $.ajax({
            //请求方式
            type : "POST",
            //请求地址
            url : "${pageContext.request.contextPath}/cart/update",
            //数据，json字符串
            data : {data:json},
            dataType:"json",
            //请求成功
            success : function(result) {
                if (result.code == 0){
                    window.location.reload();
                } else {
                    console.log(result.msg);
                }
            },
            //请求失败，包含具体的错误信息
            error : function(e){
                console.log(e.status);
                console.log(e.responseText);
            }
        });
    }
</script>
