<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%

    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<head>
    <base href="<%=basePath%>">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="meta description">
    <title>首页</title>
    <!--=== Favicon ===-->
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/assets/img/favicon.ico" type="image/x-icon"/>
    <!--== Google Fonts ==-->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,400i,500,600,700" rel="stylesheet">
    <!--=== Bootstrap CSS ===-->
    <link href="${pageContext.request.contextPath}/assets/css/vendor/bootstrap.min.css" rel="stylesheet">
    <!--=== Font-Awesome CSS ===-->
    <link href="${pageContext.request.contextPath}/assets/css/vendor/font-awesome.css" rel="stylesheet">
    <!--=== Plugins CSS ===-->
    <link href="${pageContext.request.contextPath}/assets/css/plugins.css" rel="stylesheet">
    <!--=== Helper CSS ===-->
    <link href="${pageContext.request.contextPath}/assets/css/helper.min.css" rel="stylesheet">
    <!--=== Main Style CSS ===-->
    <link href="${pageContext.request.contextPath}/assets/css/style.css" rel="stylesheet">
    <!-- Modernizer JS -->
    <script src="${pageContext.request.contextPath}/assets/js/vendor/modernizr-2.8.3.min.js"></script>
    <style>
        /*提示框*/
        .tipsbox{
            position: fixed;
            top: 40%;
            left: 50%;
            z-index: 10000;
            min-width: 200px;
            min-height: 40px;
            max-width: 500px;
            border-radius: 5px;
            text-align: center;
            padding: 10px;
            color: white;
            background-color: red;
            opacity: 0.6;
            display: none;
        }

        .btn-cancelCollection{
            background-color: #f5740a;
            color: #fff;
        }
    </style>
</head>
<body>
<%@include file="/view/front/head.jsp"%>



<div>


    <!--== Start Slider Area ==-->
    <div class="slider-area-wrap">
        <div class="home-slider-carousel owl-carousel">
            <div class="single-slide-item">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="slider-text">
                                <h2>Canvas Gear</h2>
                                <h3>With Dual Front Camera</h3>
                                <h4>Can Smooth Beautiful Images</h4>
                                <a href="product/detail/123312132" target="_blank" class="btn">Shop Now</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="single-slide-item slide-item_2">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="slider-text">
                                <h2>Ajaira Mobile</h2>
                                <h3>With Dollbee Speaker</h3>
                                <h4>Can Smooth Sound</h4>
                                <a href="product/detail/123312177" target="_blank" class="btn">Shop Now</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="single-slide-item slide-item_3">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="slider-text">
                                <h2>HasMobile</h2>
                                <h3>With Dual Front Camera</h3>
                                <h4>Can Smooth Beautiful Images</h4>
                                <a href="product/detail/123312184" target="_blank" class="btn">Shop Now</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--== End Slider Area ==-->

    <%-- 第一个商品位 --%>
    <!--== Start Products  Area ==-->
    <div id="product-area-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="product-content-wrap">
                        <!-- Product Tab Menu Start -->
                        <nav class="product-teb-menu">
                            <ul class="nav justify-content-center" role="tablist">
                                <li><a class="active" href="#new-products" id="new-product-tab" data-toggle="tab">爆款</a></li>
                                <li><a href="#sale-products" id="sale-product-tab" data-toggle="tab">全网最低价</a></li>
                                <li><a href="#feature-products" id="feature-product-tab" data-toggle="tab">最新上架</a></li>
                            </ul>
                        </nav>
                        <!-- Product Tab Menu End -->

                        <!-- Product Tab Content Start -->
                        <div class="tab-content" id="productContent">
                            <%--爆款--%>
                            <div class="tab-pane fade show active" id="new-products" role="tabpanel">
                                <div class="products-wrapper">
                                    <div class="product-carousel owl-carousel">
                                        <c:choose>
                                            <c:when test="${hotProduct!=null}">
                                                <c:forEach items="${hotProduct}" var="product">
                                                    <div class="single-product-item">
                                                        <figure class="product-thumb">
                                                            <c:choose>
                                                                <c:when test="${(product.images)[0].url!=null}">
                                                                    <a href="product/detail/${product.productId}" target="_blank">
                                                                        <img src="${(product.images)[0].url}" alt="${product.name}"></a>
                                                                </c:when>
                                                                <c:otherwise>
                                                                    <a href="product/detail/${product.productId}">
                                                                        <img src="assets/img/product-1.jpg" alt="Product"></a>
                                                                </c:otherwise>
                                                            </c:choose>

                                                            <a href="product/detail/${product.productId}" target="_blank" class="btn btn-round btn-cart" title="查看详情"><i class="fa fa-eye"></i></a>
                                                        </figure>

                                                        <div class="product-details">
                                                            <h2 class="product-title"><a href="product/detail/${product.productId}"  target="_blank">${product.name}</a></h2>
                                                            <div class="rating">
                                                                <c:if test="${product.xing!=null}">
                                                                    <c:forEach var="i" begin="1" end="${product.xing/2}" step="1">
                                                                        <i class="fa fa-star"></i>
                                                                    </c:forEach>
                                                                    <c:if test="${product.xing%2 == 1}">
                                                                        <i class="fa fa-star-half"></i>
                                                                    </c:if>
                                                                    <c:forEach var="j" begin="${product.xing/2+product.xing%2}" end="4" step="1">
                                                                        <i class="fa fa-star-o"></i>
                                                                    </c:forEach>
                                                                </c:if>
                                                            </div>
                                                            <span class="product-price">￥${product.price}</span>

                                                            <div class="product-meta">
                                                                <a href="javascript:addCart(${product.productId})" class="btn btn-round btn-cart" title="加入购物车"><i
                                                                        class="fa fa-shopping-cart"></i></a>

                                                                <c:choose>
                                                                    <c:when test="${product.isCollection==1}">
                                                                        <a href="javascript:void(0);" productId="${product.productId}" class="btn btn-round btn-cart btn-cancelCollection"
                                                                           title="取消收藏"><i class="fa fa-heart"></i></a>
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                        <a href="javascript:void(0);" productId="${product.productId}" class="btn btn-round btn-cart btn-collection"
                                                                           title="加入收藏"><i class="fa fa-heart"></i></a>
                                                                    </c:otherwise>
                                                                </c:choose>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </c:forEach>
                                            </c:when>
                                            <c:otherwise>
                                                <!-- Single Product Start -->
                                                <div class="single-product-item">
                                                    <figure class="product-thumb">
                                                        <a href="product/home"><img src="assets/img/product-1.jpg"
                                                                                           alt="Product"></a>
                                                        <a href="product/home" class="btn btn-round btn-cart" title="Quick View"
                                                           data-toggle="modal" data-target="#quickView"><i
                                                                class="fa fa-eye"></i></a>
                                                    </figure>
                                                    <div class="product-details">
                                                        <h2 class="product-title"><a href="product/home">Rival Field
                                                            Messenger</a></h2>
                                                        <div class="rating">
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                        </div>
                                                        <span class="product-price">$40.99</span>

                                                        <div class="product-meta">
                                                            <a href="product/home" class="btn btn-round btn-cart" title="Add to Cart"><i
                                                                    class="fa fa-shopping-cart"></i></a>
                                                            <a href="wishlist.html" class="btn btn-round btn-cart"
                                                               title="Add to Wishlist"><i class="fa fa-heart"></i></a>
                                                            <a href="compare.html" class="btn btn-round btn-cart"
                                                               title="Add to Compare"><i class="fa fa-exchange"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- Single Product End -->

                                                <!-- Single Product Start -->
                                                <div class="single-product-item">
                                                    <figure class="product-thumb">
                                                        <a href="product/home"><img src="assets/img/product-2.jpg"
                                                                                           alt="Product"></a>
                                                        <a href="product/home" class="btn btn-round btn-cart" title="Quick View"
                                                           data-toggle="modal" data-target="#quickView"><i
                                                                class="fa fa-eye"></i></a>
                                                    </figure>
                                                    <div class="product-details">
                                                        <h2 class="product-title"><a href="product/home">Compete Track
                                                            Tote</a></h2>
                                                        <div class="rating">
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                        </div>
                                                        <span class="product-price">$40.99</span>

                                                        <div class="product-meta">
                                                            <a href="product/home" class="btn btn-round btn-cart" title="Add to Cart"><i
                                                                    class="fa fa-shopping-cart"></i></a>
                                                            <a href="wishlist.html" class="btn btn-round btn-cart"
                                                               title="Add to Wishlist"><i class="fa fa-heart"></i></a>
                                                            <a href="compare.html" class="btn btn-round btn-cart"
                                                               title="Add to Compare"><i class="fa fa-exchange"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- Single Product End -->

                                                <!-- Single Product Start -->
                                                <div class="single-product-item">
                                                    <figure class="product-thumb">
                                                        <a href="product/home"><img src="assets/img/product-3.jpg"
                                                                                           alt="Product"></a>
                                                        <a href="product/home" class="btn btn-round btn-cart" title="Quick View"
                                                           data-toggle="modal" data-target="#quickView"><i
                                                                class="fa fa-eye"></i></a>
                                                    </figure>
                                                    <div class="product-details">
                                                        <h2 class="product-title"><a href="product/home">Voyage Yoga Bag</a>
                                                        </h2>
                                                        <div class="rating">
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                        </div>
                                                        <span class="product-price">$40.99</span>

                                                        <div class="product-meta">
                                                            <a href="product/home" class="btn btn-round btn-cart" title="Add to Cart"><i
                                                                    class="fa fa-shopping-cart"></i></a>
                                                            <a href="wishlist.html" class="btn btn-round btn-cart"
                                                               title="Add to Wishlist"><i class="fa fa-heart"></i></a>
                                                            <a href="compare.html" class="btn btn-round btn-cart"
                                                               title="Add to Compare"><i class="fa fa-exchange"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- Single Product End -->

                                                <!-- Single Product Start -->
                                                <div class="single-product-item">
                                                    <figure class="product-thumb">
                                                        <a href="product/home"><img src="assets/img/product-4.jpg"
                                                                                           alt="Product"></a>
                                                        <a href="product/home" class="btn btn-round btn-cart" title="Quick View"
                                                           data-toggle="modal" data-target="#quickView"><i
                                                                class="fa fa-eye"></i></a>
                                                    </figure>
                                                    <div class="product-details">
                                                        <h2 class="product-title"><a href="product/home">Chaz Kangeroo
                                                            Hoodie</a></h2>
                                                        <div class="rating">
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                        </div>
                                                        <span class="product-price">$40.99</span>

                                                        <div class="product-meta">
                                                            <a href="product/home" class="btn btn-round btn-cart" title="Add to Cart"><i
                                                                    class="fa fa-shopping-cart"></i></a>
                                                            <a href="wishlist.html" class="btn btn-round btn-cart"
                                                               title="Add to Wishlist"><i class="fa fa-heart"></i></a>
                                                            <a href="compare.html" class="btn btn-round btn-cart"
                                                               title="Add to Compare"><i class="fa fa-exchange"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- Single Product End -->

                                                <!-- Single Product Start -->
                                                <div class="single-product-item">
                                                    <figure class="product-thumb">
                                                        <a href="product/home"><img src="assets/img/product-5.jpg"
                                                                                           alt="Product"></a>
                                                        <a href="product/home" class="btn btn-round btn-cart" title="Quick View"
                                                           data-toggle="modal" data-target="#quickView"><i
                                                                class="fa fa-eye"></i></a>
                                                    </figure>
                                                    <div class="product-details">
                                                        <h2 class="product-title"><a href="product/home">Endeavor Daytrip
                                                            Backpack</a></h2>
                                                        <div class="rating">
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                        </div>
                                                        <span class="product-price">$40.99</span>

                                                        <div class="product-meta">
                                                            <a href="product/home" class="btn btn-round btn-cart" title="Add to Cart"><i
                                                                    class="fa fa-shopping-cart"></i></a>
                                                            <a href="wishlist.html" class="btn btn-round btn-cart"
                                                               title="Add to Wishlist"><i class="fa fa-heart"></i></a>
                                                            <a href="compare.html" class="btn btn-round btn-cart"
                                                               title="Add to Compare"><i class="fa fa-exchange"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- Single Product End -->
                                            </c:otherwise>
                                        </c:choose>

                                    </div>
                                </div>
                            </div>

                                <%--最低价--%>
                            <div class="tab-pane fade" id="sale-products" role="tabpanel">
                                <div class="products-wrapper">
                                    <div class="product-carousel owl-carousel">
                                        <c:choose>
                                            <c:when test="${lowPrice!=null}">
                                                <c:forEach items="${lowPrice}" var="product">
                                                    <div class="single-product-item">
                                                        <figure class="product-thumb">
                                                            <c:choose>
                                                                <c:when test="${(product.images)[0].url!=null}">
                                                                    <a href="product/detail/${product.productId}" target="_blank">
                                                                        <img src="${(product.images)[0].url}" alt="${product.name}"></a>
                                                                </c:when>
                                                                <c:otherwise>
                                                                    <a href="product/detail/${product.productId}">
                                                                        <img src="assets/img/product-1.jpg" alt="Product"></a>
                                                                </c:otherwise>
                                                            </c:choose>

                                                            <a href="product/detail/${product.productId}" target="_blank" class="btn btn-round btn-cart" title="查看详情"><i class="fa fa-eye"></i></a>
                                                        </figure>

                                                        <div class="product-details">
                                                            <h2 class="product-title"><a href="product/detail/${product.productId}"  target="_blank">${product.name}</a></h2>
                                                            <div class="rating">
                                                                <c:if test="${product.xing!=null}">
                                                                    <c:forEach var="i" begin="1" end="${product.xing/2}" step="1">
                                                                        <i class="fa fa-star"></i>
                                                                    </c:forEach>
                                                                    <c:if test="${product.xing%2 == 1}">
                                                                        <i class="fa fa-star-half"></i>
                                                                    </c:if>
                                                                    <c:forEach var="j" begin="${product.xing/2+product.xing%2}" end="4" step="1">
                                                                        <i class="fa fa-star-o"></i>
                                                                    </c:forEach>
                                                                </c:if>
                                                            </div>
                                                            <span class="product-price">￥${product.price}</span>

                                                            <div class="product-meta">
                                                                <a href="javascript:addCart(${product.productId})" class="btn btn-round btn-cart" title="加入购物车"><i
                                                                        class="fa fa-shopping-cart"></i></a>

                                                                <c:choose>
                                                                    <c:when test="${product.isCollection==1}">
                                                                        <a href="javascript:void(0);" productId="${product.productId}" class="btn btn-round btn-cart btn-cancelCollection"
                                                                           title="取消收藏"><i class="fa fa-heart"></i></a>
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                        <a href="javascript:void(0);" productId="${product.productId}" class="btn btn-round btn-cart btn-collection"
                                                                           title="加入收藏"><i class="fa fa-heart"></i></a>
                                                                    </c:otherwise>
                                                                </c:choose>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </c:forEach>
                                            </c:when>
                                            <c:otherwise>
                                                <!-- Single Product Start -->
                                                <div class="single-product-item">
                                                    <figure class="product-thumb">
                                                        <a href="product/home"><img src="assets/img/product-1.jpg"
                                                                                           alt="Product"></a>
                                                        <a href="product/home" class="btn btn-round btn-cart" title="Quick View"
                                                           data-toggle="modal" data-target="#quickView"><i
                                                                class="fa fa-eye"></i></a>
                                                    </figure>
                                                    <div class="product-details">
                                                        <h2 class="product-title"><a href="product/home">Rival Field
                                                            Messenger</a></h2>
                                                        <div class="rating">
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                        </div>
                                                        <span class="product-price">$40.99</span>

                                                        <div class="product-meta">
                                                            <a href="product/home" class="btn btn-round btn-cart" title="Add to Cart"><i
                                                                    class="fa fa-shopping-cart"></i></a>
                                                            <a href="wishlist.html" class="btn btn-round btn-cart"
                                                               title="Add to Wishlist"><i class="fa fa-heart"></i></a>
                                                            <a href="compare.html" class="btn btn-round btn-cart"
                                                               title="Add to Compare"><i class="fa fa-exchange"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- Single Product End -->

                                                <!-- Single Product Start -->
                                                <div class="single-product-item">
                                                    <figure class="product-thumb">
                                                        <a href="product/home"><img src="assets/img/product-2.jpg"
                                                                                           alt="Product"></a>
                                                        <a href="product/home" class="btn btn-round btn-cart" title="Quick View"
                                                           data-toggle="modal" data-target="#quickView"><i
                                                                class="fa fa-eye"></i></a>
                                                    </figure>
                                                    <div class="product-details">
                                                        <h2 class="product-title"><a href="product/home">Compete Track
                                                            Tote</a></h2>
                                                        <div class="rating">
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                        </div>
                                                        <span class="product-price">$40.99</span>

                                                        <div class="product-meta">
                                                            <a href="product/home" class="btn btn-round btn-cart" title="Add to Cart"><i
                                                                    class="fa fa-shopping-cart"></i></a>
                                                            <a href="wishlist.html" class="btn btn-round btn-cart"
                                                               title="Add to Wishlist"><i class="fa fa-heart"></i></a>
                                                            <a href="compare.html" class="btn btn-round btn-cart"
                                                               title="Add to Compare"><i class="fa fa-exchange"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- Single Product End -->

                                                <!-- Single Product Start -->
                                                <div class="single-product-item">
                                                    <figure class="product-thumb">
                                                        <a href="product/home"><img src="assets/img/product-3.jpg"
                                                                                           alt="Product"></a>
                                                        <a href="product/home" class="btn btn-round btn-cart" title="Quick View"
                                                           data-toggle="modal" data-target="#quickView"><i
                                                                class="fa fa-eye"></i></a>
                                                    </figure>
                                                    <div class="product-details">
                                                        <h2 class="product-title"><a href="product/home">Voyage Yoga Bag</a>
                                                        </h2>
                                                        <div class="rating">
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                        </div>
                                                        <span class="product-price">$40.99</span>

                                                        <div class="product-meta">
                                                            <a href="product/home" class="btn btn-round btn-cart" title="Add to Cart"><i
                                                                    class="fa fa-shopping-cart"></i></a>
                                                            <a href="wishlist.html" class="btn btn-round btn-cart"
                                                               title="Add to Wishlist"><i class="fa fa-heart"></i></a>
                                                            <a href="compare.html" class="btn btn-round btn-cart"
                                                               title="Add to Compare"><i class="fa fa-exchange"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- Single Product End -->

                                                <!-- Single Product Start -->
                                                <div class="single-product-item">
                                                    <figure class="product-thumb">
                                                        <a href="product/home"><img src="assets/img/product-4.jpg"
                                                                                           alt="Product"></a>
                                                        <a href="product/home" class="btn btn-round btn-cart" title="Quick View"
                                                           data-toggle="modal" data-target="#quickView"><i
                                                                class="fa fa-eye"></i></a>
                                                    </figure>
                                                    <div class="product-details">
                                                        <h2 class="product-title"><a href="product/home">Chaz Kangeroo
                                                            Hoodie</a></h2>
                                                        <div class="rating">
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                        </div>
                                                        <span class="product-price">$40.99</span>

                                                        <div class="product-meta">
                                                            <a href="product/home" class="btn btn-round btn-cart" title="Add to Cart"><i
                                                                    class="fa fa-shopping-cart"></i></a>
                                                            <a href="wishlist.html" class="btn btn-round btn-cart"
                                                               title="Add to Wishlist"><i class="fa fa-heart"></i></a>
                                                            <a href="compare.html" class="btn btn-round btn-cart"
                                                               title="Add to Compare"><i class="fa fa-exchange"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- Single Product End -->

                                                <!-- Single Product Start -->
                                                <div class="single-product-item">
                                                    <figure class="product-thumb">
                                                        <a href="product/home"><img src="assets/img/product-5.jpg"
                                                                                           alt="Product"></a>
                                                        <a href="product/home" class="btn btn-round btn-cart" title="Quick View"
                                                           data-toggle="modal" data-target="#quickView"><i
                                                                class="fa fa-eye"></i></a>
                                                    </figure>
                                                    <div class="product-details">
                                                        <h2 class="product-title"><a href="product/home">Endeavor Daytrip
                                                            Backpack</a></h2>
                                                        <div class="rating">
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                        </div>
                                                        <span class="product-price">$40.99</span>

                                                        <div class="product-meta">
                                                            <a href="product/home" class="btn btn-round btn-cart" title="Add to Cart"><i
                                                                    class="fa fa-shopping-cart"></i></a>
                                                            <a href="wishlist.html" class="btn btn-round btn-cart"
                                                               title="Add to Wishlist"><i class="fa fa-heart"></i></a>
                                                            <a href="compare.html" class="btn btn-round btn-cart"
                                                               title="Add to Compare"><i class="fa fa-exchange"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- Single Product End -->
                                            </c:otherwise>
                                        </c:choose>
                                    </div>
                                </div>
                            </div>

                                <%--最新上架--%>
                            <div class="tab-pane fade" id="feature-products" role="tabpanel">
                                <div class="products-wrapper">
                                    <div class="product-carousel owl-carousel">
                                        <c:choose>
                                            <c:when test="${newProduct!=null}">
                                                <c:forEach items="${newProduct}" var="product">
                                                    <div class="single-product-item">
                                                        <figure class="product-thumb">
                                                            <c:choose>
                                                                <c:when test="${(product.images)[0].url!=null}">
                                                                    <a href="product/detail/${product.productId}" target="_blank">
                                                                        <img src="${(product.images)[0].url}" alt="${product.name}"></a>
                                                                </c:when>
                                                                <c:otherwise>
                                                                    <a href="product/detail/${product.productId}">
                                                                        <img src="assets/img/product-1.jpg" alt="Product"></a>
                                                                </c:otherwise>
                                                            </c:choose>

                                                            <a href="product/detail/${product.productId}" target="_blank" class="btn btn-round btn-cart" title="查看详情"><i class="fa fa-eye"></i></a>
                                                        </figure>

                                                        <div class="product-details">
                                                            <h2 class="product-title"><a href="product/detail/${product.productId}"  target="_blank">${product.name}</a></h2>
                                                            <div class="rating">
                                                                <c:if test="${product.xing!=null}">
                                                                    <c:forEach var="i" begin="1" end="${product.xing/2}" step="1">
                                                                        <i class="fa fa-star"></i>
                                                                    </c:forEach>
                                                                    <c:if test="${product.xing%2 == 1}">
                                                                        <i class="fa fa-star-half"></i>
                                                                    </c:if>
                                                                    <c:forEach var="j" begin="${product.xing/2+product.xing%2}" end="4" step="1">
                                                                        <i class="fa fa-star-o"></i>
                                                                    </c:forEach>
                                                                </c:if>
                                                            </div>
                                                            <span class="product-price">￥${product.price}</span>

                                                            <div class="product-meta">
                                                                <a href="javascript:addCart(${product.productId})" class="btn btn-round btn-cart" title="加入购物车"><i
                                                                        class="fa fa-shopping-cart"></i></a>

                                                                <c:choose>
                                                                    <c:when test="${product.isCollection==1}">
                                                                        <a href="javascript:void(0);" productId="${product.productId}" class="btn btn-round btn-cart btn-cancelCollection"
                                                                           title="取消收藏"><i class="fa fa-heart"></i></a>
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                        <a href="javascript:void(0);" productId="${product.productId}" class="btn btn-round btn-cart btn-collection"
                                                                           title="加入收藏"><i class="fa fa-heart"></i></a>
                                                                    </c:otherwise>
                                                                </c:choose>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </c:forEach>
                                            </c:when>
                                            <c:otherwise>
                                                <!-- Single Product Start -->
                                                <div class="single-product-item">
                                                    <figure class="product-thumb">
                                                        <a href="product/home"><img src="assets/img/product-1.jpg"
                                                                                           alt="Product"></a>
                                                        <a href="product/home" class="btn btn-round btn-cart" title="Quick View"
                                                           data-toggle="modal" data-target="#quickView"><i
                                                                class="fa fa-eye"></i></a>
                                                    </figure>
                                                    <div class="product-details">
                                                        <h2 class="product-title"><a href="product/home">Rival Field
                                                            Messenger</a></h2>
                                                        <div class="rating">
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                        </div>
                                                        <span class="product-price">$40.99</span>

                                                        <div class="product-meta">
                                                            <a href="product/home" class="btn btn-round btn-cart" title="Add to Cart"><i
                                                                    class="fa fa-shopping-cart"></i></a>
                                                            <a href="wishlist.html" class="btn btn-round btn-cart"
                                                               title="Add to Wishlist"><i class="fa fa-heart"></i></a>
                                                            <a href="compare.html" class="btn btn-round btn-cart"
                                                               title="Add to Compare"><i class="fa fa-exchange"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- Single Product End -->

                                                <!-- Single Product Start -->
                                                <div class="single-product-item">
                                                    <figure class="product-thumb">
                                                        <a href="product/home"><img src="assets/img/product-2.jpg"
                                                                                           alt="Product"></a>
                                                        <a href="product/home" class="btn btn-round btn-cart" title="Quick View"
                                                           data-toggle="modal" data-target="#quickView"><i
                                                                class="fa fa-eye"></i></a>
                                                    </figure>
                                                    <div class="product-details">
                                                        <h2 class="product-title"><a href="product/home">Compete Track
                                                            Tote</a></h2>
                                                        <div class="rating">
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                        </div>
                                                        <span class="product-price">$40.99</span>

                                                        <div class="product-meta">
                                                            <a href="product/home" class="btn btn-round btn-cart" title="Add to Cart"><i
                                                                    class="fa fa-shopping-cart"></i></a>
                                                            <a href="wishlist.html" class="btn btn-round btn-cart"
                                                               title="Add to Wishlist"><i class="fa fa-heart"></i></a>
                                                            <a href="compare.html" class="btn btn-round btn-cart"
                                                               title="Add to Compare"><i class="fa fa-exchange"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- Single Product End -->

                                                <!-- Single Product Start -->
                                                <div class="single-product-item">
                                                    <figure class="product-thumb">
                                                        <a href="product/home"><img src="assets/img/product-3.jpg"
                                                                                           alt="Product"></a>
                                                        <a href="product/home" class="btn btn-round btn-cart" title="Quick View"
                                                           data-toggle="modal" data-target="#quickView"><i
                                                                class="fa fa-eye"></i></a>
                                                    </figure>
                                                    <div class="product-details">
                                                        <h2 class="product-title"><a href="product/home">Voyage Yoga Bag</a>
                                                        </h2>
                                                        <div class="rating">
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                        </div>
                                                        <span class="product-price">$40.99</span>

                                                        <div class="product-meta">
                                                            <a href="product/home" class="btn btn-round btn-cart" title="Add to Cart"><i
                                                                    class="fa fa-shopping-cart"></i></a>
                                                            <a href="wishlist.html" class="btn btn-round btn-cart"
                                                               title="Add to Wishlist"><i class="fa fa-heart"></i></a>
                                                            <a href="compare.html" class="btn btn-round btn-cart"
                                                               title="Add to Compare"><i class="fa fa-exchange"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- Single Product End -->

                                                <!-- Single Product Start -->
                                                <div class="single-product-item">
                                                    <figure class="product-thumb">
                                                        <a href="product/home"><img src="assets/img/product-4.jpg"
                                                                                           alt="Product"></a>
                                                        <a href="product/home" class="btn btn-round btn-cart" title="Quick View"
                                                           data-toggle="modal" data-target="#quickView"><i
                                                                class="fa fa-eye"></i></a>
                                                    </figure>
                                                    <div class="product-details">
                                                        <h2 class="product-title"><a href="product/home">Chaz Kangeroo
                                                            Hoodie</a></h2>
                                                        <div class="rating">
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                        </div>
                                                        <span class="product-price">$40.99</span>

                                                        <div class="product-meta">
                                                            <a href="product/home" class="btn btn-round btn-cart" title="Add to Cart"><i
                                                                    class="fa fa-shopping-cart"></i></a>
                                                            <a href="wishlist.html" class="btn btn-round btn-cart"
                                                               title="Add to Wishlist"><i class="fa fa-heart"></i></a>
                                                            <a href="compare.html" class="btn btn-round btn-cart"
                                                               title="Add to Compare"><i class="fa fa-exchange"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- Single Product End -->

                                                <!-- Single Product Start -->
                                                <div class="single-product-item">
                                                    <figure class="product-thumb">
                                                        <a href="product/home"><img src="assets/img/product-5.jpg"
                                                                                           alt="Product"></a>
                                                        <a href="product/home" class="btn btn-round btn-cart" title="Quick View"
                                                           data-toggle="modal" data-target="#quickView"><i
                                                                class="fa fa-eye"></i></a>
                                                    </figure>
                                                    <div class="product-details">
                                                        <h2 class="product-title"><a href="product/home">Endeavor Daytrip
                                                            Backpack</a></h2>
                                                        <div class="rating">
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                        </div>
                                                        <span class="product-price">$40.99</span>

                                                        <div class="product-meta">
                                                            <a href="product/home" class="btn btn-round btn-cart" title="Add to Cart"><i
                                                                    class="fa fa-shopping-cart"></i></a>
                                                            <a href="wishlist.html" class="btn btn-round btn-cart"
                                                               title="Add to Wishlist"><i class="fa fa-heart"></i></a>
                                                            <a href="compare.html" class="btn btn-round btn-cart"
                                                               title="Add to Compare"><i class="fa fa-exchange"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- Single Product End -->
                                            </c:otherwise>
                                        </c:choose>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Product Tab Content End -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--== End Products  Area ==-->

    <%--广告--%>
    <!-- Start Sale Banner Area -->
    <div class="banner-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <a href="product/list">
                        <img src="assets/img/banner-home-one.jpg" alt="Banner"/>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <!-- End Sale Banner Area -->

    <%-- 分类商品 --%>

    <!-- Start Product By Category -->
    <section id="productsby-category">
        <div class="container">
            <div class="productby-cate-content">
                <div class="row">

                    <%--华为--%>
                    <div class="col-lg-6">
                        <div class="category-product-wrap">
                            <h4 class="cate-pro-title"><a href="product/list">华为</a></h4>
                            <figure class="cat-banner">
                                <a href="product/list?categoryId=501020"><img src="https://i.loli.net/2019/07/10/5d257cda9c7c734054.png" alt="华为"></a>
                            </figure>

                            <div class="products-wrapper">
                                <div class="cat-pro-carousel owl-carousel">
                                    <c:choose>
                                        <c:when test="${huawei!=null}">
                                            <c:forEach items="${huawei}" var="product">
                                                <div class="single-product-item">
                                                    <figure class="product-thumb">
                                                        <c:choose>
                                                            <c:when test="${(product.images)[0].url!=null}">
                                                                <a href="product/detail/${product.productId}" target="_blank">
                                                                    <img src="${(product.images)[0].url}" alt="Product"></a>
                                                            </c:when>
                                                            <c:otherwise>
                                                                <a href="product/detail/${product.productId}">
                                                                    <img src="assets/img/product-1.jpg" alt="Product"></a>
                                                            </c:otherwise>
                                                        </c:choose>
                                                        <a href="product/detail/${product.productId}" target="_blank" class="btn btn-round btn-cart" title="查看详情"><i
                                                                class="fa fa-eye"></i></a>
                                                    </figure>
                                                    <div class="product-details">
                                                        <h2 class="product-title"><a href="product/detail/${product.productId}" target="_blank">${product.name}</a></h2>
                                                        <div class="rating">
                                                            <c:if test="${product.xing!=null}">
                                                                <c:forEach var="i" begin="1" end="${product.xing/2}" step="1">
                                                                    <i class="fa fa-star"></i>
                                                                </c:forEach>
                                                                <c:if test="${product.xing%2 == 1}">
                                                                    <i class="fa fa-star-half"></i>
                                                                </c:if>
                                                                <c:forEach var="j" begin="${product.xing/2+product.xing%2}" end="4" step="1">
                                                                    <i class="fa fa-star-o"></i>
                                                                </c:forEach>
                                                            </c:if>
                                                        </div>
                                                        <span class="product-price">￥${product.price}</span>

                                                        <div class="product-meta">
                                                            <a href="javascript:addCart(${product.productId})" class="btn btn-round btn-cart" title="加入购物车"><i
                                                                    class="fa fa-shopping-cart"></i></a>

                                                            <c:choose>
                                                                <c:when test="${product.isCollection==1}">
                                                                    <a href="javascript:void(0);" productId="${product.productId}" class="btn btn-round btn-cart btn-cancelCollection"
                                                                       title="取消收藏"><i class="fa fa-heart"></i></a>
                                                                </c:when>
                                                                <c:otherwise>
                                                                    <a href="javascript:void(0);" productId="${product.productId}" class="btn btn-round btn-cart btn-collection"
                                                                       title="加入收藏"><i class="fa fa-heart"></i></a>
                                                                </c:otherwise>
                                                            </c:choose>

                                                        </div>
                                                    </div>
                                                </div>
                                            </c:forEach>
                                        </c:when>
                                        <c:otherwise>

                                            <!-- Single Product Start -->
                                            <div class="single-product-item">
                                                <figure class="product-thumb">
                                                    <a href="product/home"><img src="assets/img/product-1.jpg"
                                                                                       alt="Product"></a>
                                                    <a href="product/home" class="btn btn-round btn-cart" title="Quick View"
                                                       data-toggle="modal" data-target="#quickView"><i class="fa fa-eye"></i></a>
                                                </figure>
                                                <div class="product-details">
                                                    <h2 class="product-title"><a href="product/home">Rival Field
                                                        Messenger</a></h2>
                                                    <div class="rating">
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                    </div>
                                                    <span class="product-price">$40.99</span>

                                                    <div class="product-meta">
                                                        <a href="product/home" class="btn btn-round btn-cart" title="Add to Cart"><i
                                                                class="fa fa-shopping-cart"></i></a>
                                                        <a href="wishlist.html" class="btn btn-round btn-cart"
                                                           title="Add to Wishlist"><i class="fa fa-heart"></i></a>
                                                        <a href="compare.html" class="btn btn-round btn-cart"
                                                           title="Add to Compare"><i class="fa fa-exchange"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Single Product End -->

                                            <!-- Single Product Start -->
                                            <div class="single-product-item">
                                                <figure class="product-thumb">
                                                    <a href="product/home"><img src="assets/img/product-2.jpg"
                                                                                       alt="Product"></a>
                                                    <a href="product/home" class="btn btn-round btn-cart" title="Quick View"
                                                       data-toggle="modal" data-target="#quickView"><i class="fa fa-eye"></i></a>
                                                </figure>
                                                <div class="product-details">
                                                    <h2 class="product-title"><a href="product/home">Compete Track
                                                        Tote</a></h2>
                                                    <div class="rating">
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                    </div>
                                                    <span class="product-price">$40.99</span>

                                                    <div class="product-meta">
                                                        <a href="product/home" class="btn btn-round btn-cart" title="Add to Cart"><i
                                                                class="fa fa-shopping-cart"></i></a>
                                                        <a href="wishlist.html" class="btn btn-round btn-cart"
                                                           title="Add to Wishlist"><i class="fa fa-heart"></i></a>
                                                        <a href="compare.html" class="btn btn-round btn-cart"
                                                           title="Add to Compare"><i class="fa fa-exchange"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Single Product End -->

                                            <!-- Single Product Start -->
                                            <div class="single-product-item">
                                                <figure class="product-thumb">
                                                    <a href="product/home"><img src="assets/img/product-3.jpg"
                                                                                       alt="Product"></a>
                                                    <a href="product/home" class="btn btn-round btn-cart" title="Quick View"
                                                       data-toggle="modal" data-target="#quickView"><i class="fa fa-eye"></i></a>
                                                </figure>
                                                <div class="product-details">
                                                    <h2 class="product-title"><a href="product/home">Voyage Yoga Bag</a>
                                                    </h2>
                                                    <div class="rating">
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                    </div>
                                                    <span class="product-price">$40.99</span>

                                                    <div class="product-meta">
                                                        <a href="product/home" class="btn btn-round btn-cart" title="Add to Cart"><i
                                                                class="fa fa-shopping-cart"></i></a>
                                                        <a href="wishlist.html" class="btn btn-round btn-cart"
                                                           title="Add to Wishlist"><i class="fa fa-heart"></i></a>
                                                        <a href="compare.html" class="btn btn-round btn-cart"
                                                           title="Add to Compare"><i class="fa fa-exchange"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Single Product End -->
                                        </c:otherwise>
                                    </c:choose>
                                </div>
                            </div>
                        </div>
                    </div>


                        <%--荣耀--%>
                    <!-- Single Category Product Start -->
                    <div class="col-lg-6">
                        <div class="category-product-wrap">
                            <h4 class="cate-pro-title"><a href="product/list">荣耀</a></h4>
                            <figure class="cat-banner">
                                <a href="product/list?categoryId=501060"><img src="https://i.loli.net/2019/07/10/5d257d5327a7342734.png" alt="荣耀"></a>
                            </figure>

                            <div class="products-wrapper">
                                <div class="cat-pro-carousel owl-carousel">
                                    <c:choose>
                                        <c:when test="${rongyao!=null}">
                                            <c:forEach items="${rongyao}" var="product">
                                                <div class="single-product-item">
                                                    <figure class="product-thumb">
                                                        <c:choose>
                                                            <c:when test="${(product.images)[0].url!=null}">
                                                                <a href="product/detail/${product.productId}" target="_blank">
                                                                    <img src="${(product.images)[0].url}" alt="Product"></a>
                                                            </c:when>
                                                            <c:otherwise>
                                                                <a href="product/detail/${product.productId}">
                                                                    <img src="assets/img/product-1.jpg" alt="Product"></a>
                                                            </c:otherwise>
                                                        </c:choose>
                                                        <a href="product/detail/${product.productId}" target="_blank" class="btn btn-round btn-cart" title="查看详情"><i
                                                                class="fa fa-eye"></i></a>
                                                    </figure>
                                                    <div class="product-details">
                                                        <h2 class="product-title"><a href="product/detail/${product.productId}" target="_blank">${product.name}</a></h2>
                                                        <div class="rating">
                                                            <c:if test="${product.xing!=null}">
                                                                <c:forEach var="i" begin="1" end="${product.xing/2}" step="1">
                                                                    <i class="fa fa-star"></i>
                                                                </c:forEach>
                                                                <c:if test="${product.xing%2 == 1}">
                                                                    <i class="fa fa-star-half"></i>
                                                                </c:if>
                                                                <c:forEach var="j" begin="${product.xing/2+product.xing%2}" end="4" step="1">
                                                                    <i class="fa fa-star-o"></i>
                                                                </c:forEach>
                                                            </c:if>
                                                        </div>
                                                        <span class="product-price">￥${product.price}</span>

                                                        <div class="product-meta">
                                                            <a href="javascript:addCart(${product.productId})" class="btn btn-round btn-cart" title="加入购物车"><i
                                                                    class="fa fa-shopping-cart"></i></a>

                                                            <c:choose>
                                                                <c:when test="${product.isCollection==1}">
                                                                    <a href="javascript:void(0);" productId="${product.productId}" class="btn btn-round btn-cart btn-cancelCollection"
                                                                       title="取消收藏"><i class="fa fa-heart"></i></a>
                                                                </c:when>
                                                                <c:otherwise>
                                                                    <a href="javascript:void(0);" productId="${product.productId}" class="btn btn-round btn-cart btn-collection"
                                                                       title="加入收藏"><i class="fa fa-heart"></i></a>
                                                                </c:otherwise>
                                                            </c:choose>

                                                        </div>
                                                    </div>
                                                </div>
                                            </c:forEach>
                                        </c:when>
                                        <c:otherwise>

                                            <!-- Single Product Start -->
                                            <div class="single-product-item">
                                                <figure class="product-thumb">
                                                    <a href="product/home"><img src="assets/img/product-1.jpg"
                                                                                       alt="Product"></a>
                                                    <a href="product/home" class="btn btn-round btn-cart" title="Quick View"
                                                       data-toggle="modal" data-target="#quickView"><i class="fa fa-eye"></i></a>
                                                </figure>
                                                <div class="product-details">
                                                    <h2 class="product-title"><a href="product/home">Rival Field
                                                        Messenger</a></h2>
                                                    <div class="rating">
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                    </div>
                                                    <span class="product-price">$40.99</span>

                                                    <div class="product-meta">
                                                        <a href="product/home" class="btn btn-round btn-cart" title="Add to Cart"><i
                                                                class="fa fa-shopping-cart"></i></a>
                                                        <a href="wishlist.html" class="btn btn-round btn-cart"
                                                           title="Add to Wishlist"><i class="fa fa-heart"></i></a>
                                                        <a href="compare.html" class="btn btn-round btn-cart"
                                                           title="Add to Compare"><i class="fa fa-exchange"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Single Product End -->

                                            <!-- Single Product Start -->
                                            <div class="single-product-item">
                                                <figure class="product-thumb">
                                                    <a href="product/home"><img src="assets/img/product-2.jpg"
                                                                                       alt="Product"></a>
                                                    <a href="product/home" class="btn btn-round btn-cart" title="Quick View"
                                                       data-toggle="modal" data-target="#quickView"><i class="fa fa-eye"></i></a>
                                                </figure>
                                                <div class="product-details">
                                                    <h2 class="product-title"><a href="product/home">Compete Track
                                                        Tote</a></h2>
                                                    <div class="rating">
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                    </div>
                                                    <span class="product-price">$40.99</span>

                                                    <div class="product-meta">
                                                        <a href="product/home" class="btn btn-round btn-cart" title="Add to Cart"><i
                                                                class="fa fa-shopping-cart"></i></a>
                                                        <a href="wishlist.html" class="btn btn-round btn-cart"
                                                           title="Add to Wishlist"><i class="fa fa-heart"></i></a>
                                                        <a href="compare.html" class="btn btn-round btn-cart"
                                                           title="Add to Compare"><i class="fa fa-exchange"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Single Product End -->

                                            <!-- Single Product Start -->
                                            <div class="single-product-item">
                                                <figure class="product-thumb">
                                                    <a href="product/home"><img src="assets/img/product-3.jpg"
                                                                                       alt="Product"></a>
                                                    <a href="product/home" class="btn btn-round btn-cart" title="Quick View"
                                                       data-toggle="modal" data-target="#quickView"><i class="fa fa-eye"></i></a>
                                                </figure>
                                                <div class="product-details">
                                                    <h2 class="product-title"><a href="product/home">Voyage Yoga Bag</a>
                                                    </h2>
                                                    <div class="rating">
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                    </div>
                                                    <span class="product-price">$40.99</span>

                                                    <div class="product-meta">
                                                        <a href="product/home" class="btn btn-round btn-cart" title="Add to Cart"><i
                                                                class="fa fa-shopping-cart"></i></a>
                                                        <a href="wishlist.html" class="btn btn-round btn-cart"
                                                           title="Add to Wishlist"><i class="fa fa-heart"></i></a>
                                                        <a href="compare.html" class="btn btn-round btn-cart"
                                                           title="Add to Compare"><i class="fa fa-exchange"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Single Product End -->
                                        </c:otherwise>
                                    </c:choose>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Single Category Product End -->
                        
                        <%--苹果--%>
                    <!-- Single Category Product Start -->
                    <div class="col-lg-6">
                        <div class="category-product-wrap">
                            <h4 class="cate-pro-title"><a href="product/list">苹果</a></h4>
                            <figure class="cat-banner">
                                <a href="product/list"><img src="https://i.loli.net/2019/07/10/5d25c79970c7921993.png" alt="苹果"></a>
                            </figure>

                            <div class="products-wrapper">
                                <div class="cat-pro-carousel owl-carousel">
                                    <c:choose>
                                        <c:when test="${apple!=null}">
                                            <c:forEach items="${apple}" var="product">
                                                <div class="single-product-item">
                                                    <figure class="product-thumb">
                                                        <c:choose>
                                                            <c:when test="${(product.images)[0].url!=null}">
                                                                <a href="product/detail/${product.productId}" target="_blank">
                                                                    <img src="${(product.images)[0].url}" alt="Product"></a>
                                                            </c:when>
                                                            <c:otherwise>
                                                                <a href="product/detail/${product.productId}">
                                                                    <img src="assets/img/product-1.jpg" alt="Product"></a>
                                                            </c:otherwise>
                                                        </c:choose>
                                                        <a href="product/detail/${product.productId}" target="_blank" class="btn btn-round btn-cart" title="查看详情"><i
                                                                class="fa fa-eye"></i></a>
                                                    </figure>
                                                    <div class="product-details">
                                                        <h2 class="product-title"><a href="product/detail/${product.productId}" target="_blank">${product.name}</a></h2>
                                                        <div class="rating">
                                                            <c:if test="${product.xing!=null}">
                                                                <c:forEach var="i" begin="1" end="${product.xing/2}" step="1">
                                                                    <i class="fa fa-star"></i>
                                                                </c:forEach>
                                                                <c:if test="${product.xing%2 == 1}">
                                                                    <i class="fa fa-star-half"></i>
                                                                </c:if>
                                                                <c:forEach var="j" begin="${product.xing/2+product.xing%2}" end="4" step="1">
                                                                    <i class="fa fa-star-o"></i>
                                                                </c:forEach>
                                                            </c:if>
                                                        </div>
                                                        <span class="product-price">￥${product.price}</span>

                                                        <div class="product-meta">
                                                            <a href="javascript:addCart(${product.productId})" class="btn btn-round btn-cart" title="加入购物车"><i
                                                                    class="fa fa-shopping-cart"></i></a>

                                                            <c:choose>
                                                                <c:when test="${product.isCollection==1}">
                                                                    <a href="javascript:void(0);" productId="${product.productId}" class="btn btn-round btn-cart btn-cancelCollection"
                                                                       title="取消收藏"><i class="fa fa-heart"></i></a>
                                                                </c:when>
                                                                <c:otherwise>
                                                                    <a href="javascript:void(0);" productId="${product.productId}" class="btn btn-round btn-cart btn-collection"
                                                                       title="加入收藏"><i class="fa fa-heart"></i></a>
                                                                </c:otherwise>
                                                            </c:choose>

                                                        </div>
                                                    </div>
                                                </div>
                                            </c:forEach>
                                        </c:when>
                                        <c:otherwise>

                                            <!-- Single Product Start -->
                                            <div class="single-product-item">
                                                <figure class="product-thumb">
                                                    <a href="product/home"><img src="assets/img/product-1.jpg"
                                                                                       alt="Product"></a>
                                                    <a href="product/home" class="btn btn-round btn-cart" title="Quick View"
                                                       data-toggle="modal" data-target="#quickView"><i class="fa fa-eye"></i></a>
                                                </figure>
                                                <div class="product-details">
                                                    <h2 class="product-title"><a href="product/home">Rival Field
                                                        Messenger</a></h2>
                                                    <div class="rating">
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                    </div>
                                                    <span class="product-price">$40.99</span>

                                                    <div class="product-meta">
                                                        <a href="product/home" class="btn btn-round btn-cart" title="Add to Cart"><i
                                                                class="fa fa-shopping-cart"></i></a>
                                                        <a href="wishlist.html" class="btn btn-round btn-cart"
                                                           title="Add to Wishlist"><i class="fa fa-heart"></i></a>
                                                        <a href="compare.html" class="btn btn-round btn-cart"
                                                           title="Add to Compare"><i class="fa fa-exchange"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Single Product End -->

                                            <!-- Single Product Start -->
                                            <div class="single-product-item">
                                                <figure class="product-thumb">
                                                    <a href="product/home"><img src="assets/img/product-2.jpg"
                                                                                       alt="Product"></a>
                                                    <a href="product/home" class="btn btn-round btn-cart" title="Quick View"
                                                       data-toggle="modal" data-target="#quickView"><i class="fa fa-eye"></i></a>
                                                </figure>
                                                <div class="product-details">
                                                    <h2 class="product-title"><a href="product/home">Compete Track
                                                        Tote</a></h2>
                                                    <div class="rating">
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                    </div>
                                                    <span class="product-price">$40.99</span>

                                                    <div class="product-meta">
                                                        <a href="product/home" class="btn btn-round btn-cart" title="Add to Cart"><i
                                                                class="fa fa-shopping-cart"></i></a>
                                                        <a href="wishlist.html" class="btn btn-round btn-cart"
                                                           title="Add to Wishlist"><i class="fa fa-heart"></i></a>
                                                        <a href="compare.html" class="btn btn-round btn-cart"
                                                           title="Add to Compare"><i class="fa fa-exchange"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Single Product End -->

                                            <!-- Single Product Start -->
                                            <div class="single-product-item">
                                                <figure class="product-thumb">
                                                    <a href="product/home"><img src="assets/img/product-3.jpg"
                                                                                       alt="Product"></a>
                                                    <a href="product/home" class="btn btn-round btn-cart" title="Quick View"
                                                       data-toggle="modal" data-target="#quickView"><i class="fa fa-eye"></i></a>
                                                </figure>
                                                <div class="product-details">
                                                    <h2 class="product-title"><a href="product/home">Voyage Yoga Bag</a>
                                                    </h2>
                                                    <div class="rating">
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                    </div>
                                                    <span class="product-price">$40.99</span>

                                                    <div class="product-meta">
                                                        <a href="product/home" class="btn btn-round btn-cart" title="Add to Cart"><i
                                                                class="fa fa-shopping-cart"></i></a>
                                                        <a href="wishlist.html" class="btn btn-round btn-cart"
                                                           title="Add to Wishlist"><i class="fa fa-heart"></i></a>
                                                        <a href="compare.html" class="btn btn-round btn-cart"
                                                           title="Add to Compare"><i class="fa fa-exchange"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Single Product End -->
                                        </c:otherwise>
                                    </c:choose>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Single Category Product End -->

                        <%--小米--%>
                    <!-- Single Category Product Start -->
                    <div class="col-lg-6">
                        <div class="category-product-wrap">
                            <h4 class="cate-pro-title"><a href="product/list">小米</a></h4>
                            <figure class="cat-banner">
                                <a href="product/list"><img src="https://i.loli.net/2019/07/10/5d2597b54d79a79932.png" alt="小米"></a>
                            </figure>

                            <div class="products-wrapper">
                                <div class="cat-pro-carousel owl-carousel">
                                    <c:choose>
                                        <c:when test="${xiaomi!=null}">
                                            <c:forEach items="${xiaomi}" var="product">
                                                <div class="single-product-item">
                                                    <figure class="product-thumb">
                                                        <c:choose>
                                                            <c:when test="${(product.images)[0].url!=null}">
                                                                <a href="product/detail/${product.productId}" target="_blank">
                                                                    <img src="${(product.images)[0].url}" alt="Product"></a>
                                                            </c:when>
                                                            <c:otherwise>
                                                                <a href="product/detail/${product.productId}">
                                                                    <img src="assets/img/product-1.jpg" alt="Product"></a>
                                                            </c:otherwise>
                                                        </c:choose>
                                                        <a href="product/detail/${product.productId}" target="_blank" class="btn btn-round btn-cart" title="查看详情"><i
                                                                class="fa fa-eye"></i></a>
                                                    </figure>
                                                    <div class="product-details">
                                                        <h2 class="product-title"><a href="product/detail/${product.productId}" target="_blank">${product.name}</a></h2>
                                                        <div class="rating">
                                                            <c:if test="${product.xing!=null}">
                                                                <c:forEach var="i" begin="1" end="${product.xing/2}" step="1">
                                                                    <i class="fa fa-star"></i>
                                                                </c:forEach>
                                                                <c:if test="${product.xing%2 == 1}">
                                                                    <i class="fa fa-star-half"></i>
                                                                </c:if>
                                                                <c:forEach var="j" begin="${product.xing/2+product.xing%2}" end="4" step="1">
                                                                    <i class="fa fa-star-o"></i>
                                                                </c:forEach>
                                                            </c:if>
                                                        </div>
                                                        <span class="product-price">￥${product.price}</span>

                                                        <div class="product-meta">
                                                            <a href="javascript:addCart(${product.productId})" class="btn btn-round btn-cart" title="加入购物车"><i
                                                                    class="fa fa-shopping-cart"></i></a>

                                                            <c:choose>
                                                                <c:when test="${product.isCollection==1}">
                                                                    <a href="javascript:void(0);" productId="${product.productId}" class="btn btn-round btn-cart btn-cancelCollection"
                                                                       title="取消收藏"><i class="fa fa-heart"></i></a>
                                                                </c:when>
                                                                <c:otherwise>
                                                                    <a href="javascript:void(0);" productId="${product.productId}" class="btn btn-round btn-cart btn-collection"
                                                                       title="加入收藏"><i class="fa fa-heart"></i></a>
                                                                </c:otherwise>
                                                            </c:choose>

                                                        </div>
                                                    </div>
                                                </div>
                                            </c:forEach>
                                        </c:when>
                                        <c:otherwise>

                                            <!-- Single Product Start -->
                                            <div class="single-product-item">
                                                <figure class="product-thumb">
                                                    <a href="product/home"><img src="assets/img/product-1.jpg"
                                                                                       alt="Product"></a>
                                                    <a href="product/home" class="btn btn-round btn-cart" title="Quick View"
                                                       data-toggle="modal" data-target="#quickView"><i class="fa fa-eye"></i></a>
                                                </figure>
                                                <div class="product-details">
                                                    <h2 class="product-title"><a href="product/home">Rival Field
                                                        Messenger</a></h2>
                                                    <div class="rating">
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                    </div>
                                                    <span class="product-price">$40.99</span>

                                                    <div class="product-meta">
                                                        <a href="product/home" class="btn btn-round btn-cart" title="Add to Cart"><i
                                                                class="fa fa-shopping-cart"></i></a>
                                                        <a href="wishlist.html" class="btn btn-round btn-cart"
                                                           title="Add to Wishlist"><i class="fa fa-heart"></i></a>
                                                        <a href="compare.html" class="btn btn-round btn-cart"
                                                           title="Add to Compare"><i class="fa fa-exchange"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Single Product End -->

                                            <!-- Single Product Start -->
                                            <div class="single-product-item">
                                                <figure class="product-thumb">
                                                    <a href="product/home"><img src="assets/img/product-2.jpg"
                                                                                       alt="Product"></a>
                                                    <a href="product/home" class="btn btn-round btn-cart" title="Quick View"
                                                       data-toggle="modal" data-target="#quickView"><i class="fa fa-eye"></i></a>
                                                </figure>
                                                <div class="product-details">
                                                    <h2 class="product-title"><a href="product/home">Compete Track
                                                        Tote</a></h2>
                                                    <div class="rating">
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                    </div>
                                                    <span class="product-price">$40.99</span>

                                                    <div class="product-meta">
                                                        <a href="product/home" class="btn btn-round btn-cart" title="Add to Cart"><i
                                                                class="fa fa-shopping-cart"></i></a>
                                                        <a href="wishlist.html" class="btn btn-round btn-cart"
                                                           title="Add to Wishlist"><i class="fa fa-heart"></i></a>
                                                        <a href="compare.html" class="btn btn-round btn-cart"
                                                           title="Add to Compare"><i class="fa fa-exchange"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Single Product End -->

                                            <!-- Single Product Start -->
                                            <div class="single-product-item">
                                                <figure class="product-thumb">
                                                    <a href="product/home"><img src="assets/img/product-3.jpg"
                                                                                       alt="Product"></a>
                                                    <a href="product/home" class="btn btn-round btn-cart" title="Quick View"
                                                       data-toggle="modal" data-target="#quickView"><i class="fa fa-eye"></i></a>
                                                </figure>
                                                <div class="product-details">
                                                    <h2 class="product-title"><a href="product/home">Voyage Yoga Bag</a>
                                                    </h2>
                                                    <div class="rating">
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                    </div>
                                                    <span class="product-price">$40.99</span>

                                                    <div class="product-meta">
                                                        <a href="product/home" class="btn btn-round btn-cart" title="Add to Cart"><i
                                                                class="fa fa-shopping-cart"></i></a>
                                                        <a href="wishlist.html" class="btn btn-round btn-cart"
                                                           title="Add to Wishlist"><i class="fa fa-heart"></i></a>
                                                        <a href="compare.html" class="btn btn-round btn-cart"
                                                           title="Add to Compare"><i class="fa fa-exchange"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Single Product End -->
                                        </c:otherwise>
                                    </c:choose>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Single Category Product End -->

                        <%--OPPO--%>
                    <!-- Single Category Product Start -->
                    <div class="col-lg-6">
                        <div class="category-product-wrap">
                            <h4 class="cate-pro-title"><a href="product/list">OPPO</a></h4>
                            <figure class="cat-banner">
                                <a href="product/list"><img src="https://i.loli.net/2019/07/10/5d259c2e779e435904.png" alt="OPPO"></a>
                            </figure>

                            <div class="products-wrapper">
                                <div class="cat-pro-carousel owl-carousel">
                                    <c:choose>
                                        <c:when test="${oppo!=null}">
                                            <c:forEach items="${oppo}" var="product">
                                                <div class="single-product-item">
                                                    <figure class="product-thumb">
                                                        <c:choose>
                                                            <c:when test="${(product.images)[0].url!=null}">
                                                                <a href="product/detail/${product.productId}" target="_blank">
                                                                    <img src="${(product.images)[0].url}" alt="Product"></a>
                                                            </c:when>
                                                            <c:otherwise>
                                                                <a href="product/detail/${product.productId}">
                                                                    <img src="assets/img/product-1.jpg" alt="Product"></a>
                                                            </c:otherwise>
                                                        </c:choose>
                                                        <a href="product/detail/${product.productId}" target="_blank" class="btn btn-round btn-cart" title="查看详情"><i
                                                                class="fa fa-eye"></i></a>
                                                    </figure>
                                                    <div class="product-details">
                                                        <h2 class="product-title"><a href="product/detail/${product.productId}" target="_blank">${product.name}</a></h2>
                                                        <div class="rating">
                                                            <c:if test="${product.xing!=null}">
                                                                <c:forEach var="i" begin="1" end="${product.xing/2}" step="1">
                                                                    <i class="fa fa-star"></i>
                                                                </c:forEach>
                                                                <c:if test="${product.xing%2 == 1}">
                                                                    <i class="fa fa-star-half"></i>
                                                                </c:if>
                                                                <c:forEach var="j" begin="${product.xing/2+product.xing%2}" end="4" step="1">
                                                                    <i class="fa fa-star-o"></i>
                                                                </c:forEach>
                                                            </c:if>
                                                        </div>
                                                        <span class="product-price">￥${product.price}</span>

                                                        <div class="product-meta">
                                                            <a href="javascript:addCart(${product.productId})" class="btn btn-round btn-cart" title="加入购物车"><i
                                                                    class="fa fa-shopping-cart"></i></a>

                                                            <c:choose>
                                                                <c:when test="${product.isCollection==1}">
                                                                    <a href="javascript:void(0);" productId="${product.productId}" class="btn btn-round btn-cart btn-cancelCollection"
                                                                       title="取消收藏"><i class="fa fa-heart"></i></a>
                                                                </c:when>
                                                                <c:otherwise>
                                                                    <a href="javascript:void(0);" productId="${product.productId}" class="btn btn-round btn-cart btn-collection"
                                                                       title="加入收藏"><i class="fa fa-heart"></i></a>
                                                                </c:otherwise>
                                                            </c:choose>

                                                        </div>
                                                    </div>
                                                </div>
                                            </c:forEach>
                                        </c:when>
                                        <c:otherwise>

                                            <!-- Single Product Start -->
                                            <div class="single-product-item">
                                                <figure class="product-thumb">
                                                    <a href="product/home"><img src="assets/img/product-1.jpg"
                                                                                       alt="Product"></a>
                                                    <a href="product/home" class="btn btn-round btn-cart" title="Quick View"
                                                       data-toggle="modal" data-target="#quickView"><i class="fa fa-eye"></i></a>
                                                </figure>
                                                <div class="product-details">
                                                    <h2 class="product-title"><a href="product/home">Rival Field
                                                        Messenger</a></h2>
                                                    <div class="rating">
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                    </div>
                                                    <span class="product-price">$40.99</span>

                                                    <div class="product-meta">
                                                        <a href="product/home" class="btn btn-round btn-cart" title="Add to Cart"><i
                                                                class="fa fa-shopping-cart"></i></a>
                                                        <a href="wishlist.html" class="btn btn-round btn-cart"
                                                           title="Add to Wishlist"><i class="fa fa-heart"></i></a>
                                                        <a href="compare.html" class="btn btn-round btn-cart"
                                                           title="Add to Compare"><i class="fa fa-exchange"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Single Product End -->

                                            <!-- Single Product Start -->
                                            <div class="single-product-item">
                                                <figure class="product-thumb">
                                                    <a href="product/home"><img src="assets/img/product-2.jpg"
                                                                                       alt="Product"></a>
                                                    <a href="product/home" class="btn btn-round btn-cart" title="Quick View"
                                                       data-toggle="modal" data-target="#quickView"><i class="fa fa-eye"></i></a>
                                                </figure>
                                                <div class="product-details">
                                                    <h2 class="product-title"><a href="product/home">Compete Track
                                                        Tote</a></h2>
                                                    <div class="rating">
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                    </div>
                                                    <span class="product-price">$40.99</span>

                                                    <div class="product-meta">
                                                        <a href="product/home" class="btn btn-round btn-cart" title="Add to Cart"><i
                                                                class="fa fa-shopping-cart"></i></a>
                                                        <a href="wishlist.html" class="btn btn-round btn-cart"
                                                           title="Add to Wishlist"><i class="fa fa-heart"></i></a>
                                                        <a href="compare.html" class="btn btn-round btn-cart"
                                                           title="Add to Compare"><i class="fa fa-exchange"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Single Product End -->

                                            <!-- Single Product Start -->
                                            <div class="single-product-item">
                                                <figure class="product-thumb">
                                                    <a href="product/home"><img src="assets/img/product-3.jpg"
                                                                                       alt="Product"></a>
                                                    <a href="product/home" class="btn btn-round btn-cart" title="Quick View"
                                                       data-toggle="modal" data-target="#quickView"><i class="fa fa-eye"></i></a>
                                                </figure>
                                                <div class="product-details">
                                                    <h2 class="product-title"><a href="product/home">Voyage Yoga Bag</a>
                                                    </h2>
                                                    <div class="rating">
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                    </div>
                                                    <span class="product-price">$40.99</span>

                                                    <div class="product-meta">
                                                        <a href="product/home" class="btn btn-round btn-cart" title="Add to Cart"><i
                                                                class="fa fa-shopping-cart"></i></a>
                                                        <a href="wishlist.html" class="btn btn-round btn-cart"
                                                           title="Add to Wishlist"><i class="fa fa-heart"></i></a>
                                                        <a href="compare.html" class="btn btn-round btn-cart"
                                                           title="Add to Compare"><i class="fa fa-exchange"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Single Product End -->
                                        </c:otherwise>
                                    </c:choose>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Single Category Product End -->
                        
                        <%--vivo--%>
                    <!-- Single Category Product Start -->
                    <div class="col-lg-6">
                        <div class="category-product-wrap">
                            <h4 class="cate-pro-title"><a href="product/list">vivo</a></h4>
                            <figure class="cat-banner">
                                <a href="product/list"><img src="https://i.loli.net/2019/07/10/5d259c0c6657644082.png" alt="vivo"></a>
                            </figure>

                            <div class="products-wrapper">
                                <div class="cat-pro-carousel owl-carousel">
                                    <c:choose>
                                        <c:when test="${vivo!=null}">
                                            <c:forEach items="${vivo}" var="product">
                                                <div class="single-product-item">
                                                    <figure class="product-thumb">
                                                        <c:choose>
                                                            <c:when test="${(product.images)[0].url!=null}">
                                                                <a href="product/detail/${product.productId}" target="_blank">
                                                                    <img src="${(product.images)[0].url}" alt="Product"></a>
                                                            </c:when>
                                                            <c:otherwise>
                                                                <a href="product/detail/${product.productId}">
                                                                    <img src="assets/img/product-1.jpg" alt="Product"></a>
                                                            </c:otherwise>
                                                        </c:choose>
                                                        <a href="product/detail/${product.productId}" target="_blank" class="btn btn-round btn-cart" title="查看详情"><i
                                                                class="fa fa-eye"></i></a>
                                                    </figure>
                                                    <div class="product-details">
                                                        <h2 class="product-title"><a href="product/detail/${product.productId}" target="_blank">${product.name}</a></h2>
                                                        <div class="rating">
                                                            <c:if test="${product.xing!=null}">
                                                                <c:forEach var="i" begin="1" end="${product.xing/2}" step="1">
                                                                    <i class="fa fa-star"></i>
                                                                </c:forEach>
                                                                <c:if test="${product.xing%2 == 1}">
                                                                    <i class="fa fa-star-half"></i>
                                                                </c:if>
                                                                <c:forEach var="j" begin="${product.xing/2+product.xing%2}" end="4" step="1">
                                                                    <i class="fa fa-star-o"></i>
                                                                </c:forEach>
                                                            </c:if>
                                                        </div>
                                                        <span class="product-price">￥${product.price}</span>

                                                        <div class="product-meta">
                                                            <a href="javascript:addCart(${product.productId})" class="btn btn-round btn-cart" title="加入购物车"><i
                                                                    class="fa fa-shopping-cart"></i></a>

                                                            <c:choose>
                                                                <c:when test="${product.isCollection==1}">
                                                                    <a href="javascript:void(0);" productId="${product.productId}" class="btn btn-round btn-cart btn-cancelCollection"
                                                                       title="取消收藏"><i class="fa fa-heart"></i></a>
                                                                </c:when>
                                                                <c:otherwise>
                                                                    <a href="javascript:void(0);" productId="${product.productId}" class="btn btn-round btn-cart btn-collection"
                                                                       title="加入收藏"><i class="fa fa-heart"></i></a>
                                                                </c:otherwise>
                                                            </c:choose>

                                                        </div>
                                                    </div>
                                                </div>
                                            </c:forEach>
                                        </c:when>
                                        <c:otherwise>

                                            <!-- Single Product Start -->
                                            <div class="single-product-item">
                                                <figure class="product-thumb">
                                                    <a href="product/home"><img src="assets/img/product-1.jpg"
                                                                                       alt="Product"></a>
                                                    <a href="product/home" class="btn btn-round btn-cart" title="Quick View"
                                                       data-toggle="modal" data-target="#quickView"><i class="fa fa-eye"></i></a>
                                                </figure>
                                                <div class="product-details">
                                                    <h2 class="product-title"><a href="product/home">Rival Field
                                                        Messenger</a></h2>
                                                    <div class="rating">
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                    </div>
                                                    <span class="product-price">$40.99</span>

                                                    <div class="product-meta">
                                                        <a href="product/home" class="btn btn-round btn-cart" title="Add to Cart"><i
                                                                class="fa fa-shopping-cart"></i></a>
                                                        <a href="wishlist.html" class="btn btn-round btn-cart"
                                                           title="Add to Wishlist"><i class="fa fa-heart"></i></a>
                                                        <a href="compare.html" class="btn btn-round btn-cart"
                                                           title="Add to Compare"><i class="fa fa-exchange"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Single Product End -->

                                            <!-- Single Product Start -->
                                            <div class="single-product-item">
                                                <figure class="product-thumb">
                                                    <a href="product/home"><img src="assets/img/product-2.jpg"
                                                                                       alt="Product"></a>
                                                    <a href="product/home" class="btn btn-round btn-cart" title="Quick View"
                                                       data-toggle="modal" data-target="#quickView"><i class="fa fa-eye"></i></a>
                                                </figure>
                                                <div class="product-details">
                                                    <h2 class="product-title"><a href="product/home">Compete Track
                                                        Tote</a></h2>
                                                    <div class="rating">
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                    </div>
                                                    <span class="product-price">$40.99</span>

                                                    <div class="product-meta">
                                                        <a href="product/home" class="btn btn-round btn-cart" title="Add to Cart"><i
                                                                class="fa fa-shopping-cart"></i></a>
                                                        <a href="wishlist.html" class="btn btn-round btn-cart"
                                                           title="Add to Wishlist"><i class="fa fa-heart"></i></a>
                                                        <a href="compare.html" class="btn btn-round btn-cart"
                                                           title="Add to Compare"><i class="fa fa-exchange"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Single Product End -->

                                            <!-- Single Product Start -->
                                            <div class="single-product-item">
                                                <figure class="product-thumb">
                                                    <a href="product/home"><img src="assets/img/product-3.jpg"
                                                                                       alt="Product"></a>
                                                    <a href="product/home" class="btn btn-round btn-cart" title="Quick View"
                                                       data-toggle="modal" data-target="#quickView"><i class="fa fa-eye"></i></a>
                                                </figure>
                                                <div class="product-details">
                                                    <h2 class="product-title"><a href="product/home">Voyage Yoga Bag</a>
                                                    </h2>
                                                    <div class="rating">
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                    </div>
                                                    <span class="product-price">$40.99</span>

                                                    <div class="product-meta">
                                                        <a href="product/home" class="btn btn-round btn-cart" title="Add to Cart"><i
                                                                class="fa fa-shopping-cart"></i></a>
                                                        <a href="wishlist.html" class="btn btn-round btn-cart"
                                                           title="Add to Wishlist"><i class="fa fa-heart"></i></a>
                                                        <a href="compare.html" class="btn btn-round btn-cart"
                                                           title="Add to Compare"><i class="fa fa-exchange"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Single Product End -->
                                        </c:otherwise>
                                    </c:choose>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Single Category Product End -->
                </div>
            </div>
        </div>
    </section>
    <!-- End Product By Category -->






    <%-- 热门分类广告 --%>
    <!-- Start Category Banner -->
    <div class="category-banner-area">
        <div class="container">
            <div class="row row-5">
                <div class="col-md-3">
                    <a href="single-product-sale.html"><img src="assets/img/banner-masonry-1.jpg" alt="Banner" /></a>
                </div>

                <div class="col-md-9 ">
                    <div class="row row-5">
                        <div class="col-md-8">
                            <div class="row row-5">
                                <div class="col-md-6 mt-10 mt-sm-0 mt-lg-0">
                                    <a href="single-product-sale.html"><img src="assets/img/banner-masonry-2.jpg" alt="Banner" /></a>
                                </div>
                                <div class="col-md-6 mt-10 mt-sm-0 mt-lg-0">
                                    <a href="single-product-sale.html"><img src="assets/img/banner-masonry-3.jpg" alt="Banner" /></a>
                                </div>
                            </div>

                            <div class="row row-5">
                                <div class="col-md-12 mt-10">
                                    <a href="single-product-sale.html"><img src="assets/img/banner-masonry-4.jpg" alt="Banner" /></a>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 mt-10 mt-sm-0 mt-lg-0">
                            <a href="single-product-sale.html"><img src="assets/img/banner-masonry-5.jpg" alt="Banner" /></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Category Banner -->

    <%-- 热门分类 --%>

    <!-- Start Popular Categories -->
    <section id="popular-category">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="section-title">
                        <h2>Popular Categories</h2>
                    </div>
                </div>
            </div>

            <div class="popular-cate-wrap">
                <div class="row">
                    <!-- Single Popular Category -->
                    <div class="col-lg-6">
                        <div class="single-popular-category">
                            <dl class="popular-cat-list">
                                <dt>Computer</dt>
                                <dd><a href="product/home">Laptop</a></dd>
                                <dd><a href="product/home">Ram/Rom</a></dd>
                                <dd><a href="product/home">Monitors</a></dd>
                            </dl>
                        </div>
                    </div>
                    <!-- Single Popular Category End -->

                    <!-- Single Popular Category -->
                    <div class="col-lg-6">
                        <div class="single-popular-category pop-cat-2">
                            <dl class="popular-cat-list">
                                <dt>Smart Phones</dt>
                                <dd><a href="product/home">Headphone</a></dd>
                                <dd><a href="product/home">Earphone</a></dd>
                                <dd><a href="product/home">Charger</a></dd>
                            </dl>
                        </div>
                    </div>
                    <!-- Single Popular Category End -->

                    <!-- Single Popular Category -->
                    <div class="col-lg-6">
                        <div class="single-popular-category pop-cat-3">
                            <dl class="popular-cat-list">
                                <dt>Camera</dt>
                                <dd><a href="product/home">Memory</a></dd>
                                <dd><a href="product/home">Flash</a></dd>
                                <dd><a href="product/home">Lens</a></dd>
                            </dl>
                        </div>
                    </div>
                    <!-- Single Popular Category End -->

                    <!-- Single Popular Category -->
                    <div class="col-lg-6">
                        <div class="single-popular-category pop-cat-4">
                            <dl class="popular-cat-list">
                                <dt>Desktop</dt>
                                <dd><a href="product/home">Keyboard</a></dd>
                                <dd><a href="product/home">Speaker</a></dd>
                                <dd><a href="product/home">Ram</a></dd>
                            </dl>
                        </div>
                    </div>
                    <!-- Single Popular Category End -->
                </div>
            </div>
        </div>
    </section>
    <!-- End Popular Categories -->


    <%-- 帖子 --%>

    <!--== Start Recent Post & Reviews Area ==-->
    <div class="recent-post-testimonial">
        <div class="container">
            <div class="row">
                <!-- Recent Post Area Start -->
                <div class="col-lg-7">
                    <div class="recent-post-area-wrap">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="section-title">
                                    <h2>Recent Posts</h2>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="recent-post-content">
                                    <div class="recent-post-carousel owl-carousel">
                                        <!-- Single Recent Post Start -->
                                        <div class="single-blog-wrap">
                                            <figure class="blog-thumb">
                                                <a href="single-blog.html"><img src="assets/img/blog-thumb-1.jpg" alt="Blog" /></a>
                                                <figcaption class="blog-icon">
                                                    <a href="single-blog.html"><i class="fa fa-file-image-o"></i></a>
                                                </figcaption>
                                            </figure>

                                            <div class="blog-details">
                                                <h3><a href="single-blog.html">Mirum est notare quam</a></h3>
                                                <div class="post-meta">
                                                    <a href="single-blog.html">20 June, 2018</a>
                                                    <a href="single-blog.html">Post By: Tuntuni</a>
                                                </div>
                                                <p>Mirum est notare quam littera gothica, quam nunc putamus parum claram anteposuerit litterarum.</p>
                                            </div>
                                        </div>
                                        <!-- Single Recent Post End -->

                                        <!-- Single Recent Post Start -->
                                        <div class="single-blog-wrap">
                                            <figure class="blog-thumb">
                                                <a href="single-blog.html"><img src="assets/img/blog-thumb-2.jpg" alt="Blog" /></a>
                                                <figcaption class="blog-icon">
                                                    <a href="single-blog.html"><i class="fa fa-file-image-o"></i></a>
                                                </figcaption>
                                            </figure>

                                            <div class="blog-details">
                                                <h3><a href="single-blog.html">Headsets You Can Buy Right</a></h3>
                                                <div class="post-meta">
                                                    <a href="single-blog.html">20 June, 2018</a>
                                                    <a href="single-blog.html">Post By: Tuntuni</a>
                                                </div>
                                                <p>Mirum est notare quam littera gothica, quam nunc putamus parum claram,
                                                    anteposuerit
                                                    litterarum.</p>
                                            </div>
                                        </div>
                                        <!-- Single Recent Post End -->

                                        <!-- Single Recent Post Start -->
                                        <div class="single-blog-wrap">
                                            <figure class="blog-thumb">
                                                <a href="single-blog.html"><img src="assets/img/blog-thumb-3.jpg" alt="Blog" /></a>
                                                <figcaption class="blog-icon">
                                                    <a href="single-blog.html"><i class="fa fa-file-image-o"></i></a>
                                                </figcaption>
                                            </figure>

                                            <div class="blog-details">
                                                <h3><a href="single-blog.html">Headsets You Can Buy Right</a></h3>
                                                <div class="post-meta">
                                                    <a href="single-blog.html">20 June, 2018</a>
                                                    <a href="single-blog.html">Post By: Tuntuni</a>
                                                </div>
                                                <p>Mirum est notare quam littera gothica, quam nunc putamus parum claram,
                                                    anteposuerit
                                                    litterarum.</p>
                                            </div>
                                        </div>
                                        <!-- Single Recent Post End -->

                                        <!-- Single Recent Post Start -->
                                        <div class="single-blog-wrap">
                                            <figure class="blog-thumb">
                                                <a href="single-blog.html"><img src="assets/img/blog-thumb-4.jpg" alt="Blog" /></a>
                                                <figcaption class="blog-icon">
                                                    <a href="single-blog.html"><i class="fa fa-file-image-o"></i></a>
                                                </figcaption>
                                            </figure>

                                            <div class="blog-details">
                                                <h3><a href="single-blog.html">Headsets You Can Buy Right</a></h3>
                                                <div class="post-meta">
                                                    <a href="single-blog.html">20 June, 2018</a>
                                                    <a href="single-blog.html">Post By: Tuntuni</a>
                                                </div>
                                                <p>Mirum est notare quam littera gothica, quam nunc putamus parum claram,
                                                    anteposuerit
                                                    litterarum.</p>
                                            </div>
                                        </div>
                                        <!-- Single Recent Post End -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Recent Post Area End -->

                <!-- Client Reviews Area Start -->
                <div class="col-lg-5">
                    <div class="client-says-area-wrap">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="section-title">
                                    <h2>Client Says</h2>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="reviews-content">
                                    <div class="reviews-carousel owl-carousel">
                                        <!-- Single Reviews Start -->
                                        <div class="single-reviews">
                                            <a href="product/home" class="client-thumb">
                                                <img src="assets/img/client-1.jpg" alt="Cliebt" />
                                            </a>
                                            <a href="product/home" class="client-name">Stefano Colombarolli <span class="client-designation">Akamla Manager</span></a>

                                            <p class="client-quote">All Perfect !! I have three sites with magento , this theme is the best !! Excellent support , advice theme installation package , sorry for English, are Italian but I had no problem !! Thank you !</p>
                                        </div>
                                        <!-- Single Reviews End -->

                                        <!-- Single Reviews Start -->
                                        <div class="single-reviews">
                                            <a href="product/home" class="client-thumb">
                                                <img src="assets/img/client-2.jpg" alt="Cliebt" />
                                            </a>
                                            <a href="product/home" class="client-name">Alex Tuntuni <span class="client-designation">Product Manager</span></a>

                                            <p class="client-quote">All Perfect !! I have three sites with magento , this theme is the best !! Excellent support , advice theme installation package , sorry for English, are Italian but I had no problem !! Thank you !</p>
                                        </div>
                                        <!-- Single Reviews End -->

                                        <!-- Single Reviews Start -->
                                        <div class="single-reviews">
                                            <a href="product/home" class="client-thumb">
                                                <img src="assets/img/client-3.jpg" alt="Cliebt" />
                                            </a>
                                            <a href="product/home" class="client-name">Stefano Colombarolli <span class="client-designation">Akamla Manager</span></a>

                                            <p class="client-quote">All Perfect !! I have three sites with magento , this theme is the best !! Excellent support , advice theme installation package , sorry for English, are Italian but I had no problem !! Thank you !</p>
                                        </div>
                                        <!-- Single Reviews End -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Client Reviews Area End -->
            </div>
        </div>
    </div>
    <!--== End Recent Post & Reviews Area ==-->

    <%-- 轮播图标 --%>
    <!--== Start Brand Carousel Area ==-->
    <div class="brand-logo-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="brand-logo-carousel owl-carousel">
                        <!-- Single Brand Logo Start -->
                        <div class="single-brand-item">
                            <a href="product/home"><img src="assets/img/logo-1.png" alt="brand"></a>
                        </div>
                        <!-- Single Brand Logo End -->

                        <!-- Single Brand Logo Start -->
                        <div class="single-brand-item">
                            <a href="product/home"><img src="assets/img/logo-2.png" alt="brand"></a>
                        </div>
                        <!-- Single Brand Logo End -->

                        <!-- Single Brand Logo Start -->
                        <div class="single-brand-item">
                            <a href="product/home"><img src="assets/img/logo-3.png" alt="brand"></a>
                        </div>
                        <!-- Single Brand Logo End -->

                        <!-- Single Brand Logo Start -->
                        <div class="single-brand-item">
                            <a href="product/home"><img src="assets/img/logo-4.png" alt="brand"></a>
                        </div>
                        <!-- Single Brand Logo End -->

                        <!-- Single Brand Logo Start -->
                        <div class="single-brand-item">
                            <a href="product/home"><img src="assets/img/logo-5.png" alt="brand"></a>
                        </div>
                        <!-- Single Brand Logo End -->

                        <!-- Single Brand Logo Start -->
                        <div class="single-brand-item">
                            <a href="product/home"><img src="assets/img/logo-6.png" alt="brand"></a>
                        </div>
                        <!-- Single Brand Logo End -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--== End Brand Carousel Area ==-->



</div>



<%@include file="/view/front/foot.jsp"%>
</body>
<!--=======================Javascript============================-->
<!--=== Jquery Min Js ===-->
<script src="${pageContext.request.contextPath}/assets/js/vendor/jquery-3.3.1.min.js"></script>
<!--=== Jquery Migrate Min Js ===-->
<script src="${pageContext.request.contextPath}/assets/js/vendor/jquery-migrate-1.4.1.min.js"></script>
<!--=== Popper Min Js ===-->
<script src="${pageContext.request.contextPath}/assets/js/vendor/popper.min.js"></script>
<!--=== Bootstrap Min Js ===-->
<script src="${pageContext.request.contextPath}/assets/js/vendor/bootstrap.min.js"></script>
<!--=== Ajax Mail Js ===-->
<script src="${pageContext.request.contextPath}/assets/js/ajax-mail.js"></script>
<!--=== Plugins Min Js ===-->
<script src="${pageContext.request.contextPath}/assets/js/plugins.js"></script>
<!--=== Active Js ===-->
<script src="${pageContext.request.contextPath}/assets/js/active.js"></script>
</html>
<script>
    $(function () {
        $(".btn-collection").on("click",function () {
            if($(this).hasClass("btn-collection")){
                collection($(this));
            }else{
                cancelCollection($(this));
            }

        });
        $(".btn-cancelCollection").on("click",function () {
            if($(this).hasClass("btn-collection")){
                collection($(this));
            }else{
                cancelCollection($(this));
            }

        });

        function collection(btn_col){
            let productId = btn_col.attr("productId");
            $.ajax({
                url: "product/collection?productId=" + productId,
                type: "post",
                async: false,
                success: function (res) {
                    if (res == 1) {
                        btn_col.removeClass("btn-collection");
                        // console.log("allClass:"+btn_col[0].className);
                        // console.log("allClass:"+btn_col.attr("class"));
                        // console.log("btn-collection");
                        btn_col.attr("title","取消收藏");
                        // console.log($(this).attr("title"));
                        btn_col.addClass("btn-cancelCollection");
                        // console.log("btn-cancelCollection");

                        tipsbox("收藏成功","blue",2000);
                    } else if (res == 200) {
                        tipsbox("登录后才能收藏哦~~", "blue", 2000);
                    } else {
                        tipsbox("收藏失败", "red", 2000);
                    }
                },
                error: function () {
                    tipsbox("网络异常，收藏失败", "red", 2000);
                }
            });
        }

        function cancelCollection(btn_col){
            let productId = btn_col.attr("productId");
            $.ajax({
                url: "product/cancelCollection?productId=" + productId,
                type: "post",
                async: false,
                success: function (res) {
                    if (res == 1) {
                        btn_col.removeClass("btn-cancelCollection");
                        // console.log("btn-cancelCollection");
                        btn_col.attr("title","加入收藏");
                        // console.log($(this).attr("title"));
                        btn_col.addClass("btn-collection");
                        // console.log("btn-collection");

                        tipsbox("已取消收藏","blue",2000);
                    } else if (res == 200) {
                        tipsbox("登录后才能取消收藏哦~~", "blue", 2000);
                    } else {
                        tipsbox("取消收藏失败", "red", 2000);
                    }
                },
                error: function () {
                    tipsbox("网络异常，取消收藏失败", "red", 2000);
                }
            });
        }
    });
    function addCart(productId) {
        $.ajax({
            url:"cart/add?id="+productId,
            dataType:"json",
            success:function (res) {
                if(res.code=='0'){
                    tipsbox("加入购物车成功","blue",2000);
                    window.location.reload();
                }else{
                    tipsbox("加入购物车失败","red",2000);
                }
            },
            error:function () {
                tipsbox("网络异常,加入购物车失败","red",2000);
            }
        });
    }

    function tipsbox(msg,bgcolor,time) {
        // alert("收藏成功");
        $("body").append("<div id='tips' class='tipsbox'>" + msg + "</div>");
        var tips = $("#tips");
        tips.css("margin-left","-"+tips.width()/2+"px");
        if(bgcolor!=null){
            tips.css("background-color",bgcolor);
        }
        if(time == null){
            time = 2000;
        }
        tips.fadeIn("slow");
        setTimeout(function () {
            tips.fadeOut("slow");
            setTimeout(function () {
                tips.remove();
            },1000);
        },time)

    }
</script>