<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>
<head>
    <base href="<%=basePath%>">
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>

<div class="p-parameter">
    <ul class="parameter1 p-parameter-list">
        <li class="fore0">
            <i class="i-phone"></i>
            <div class="detail">
                <p title="2340*1080">分辨率：2340*1080</p></div>
        </li>
        <li class="fore1">
            <i class="i-camera"></i>
            <div class="detail">
                <p title="2400万+1600万+200万像素">后置摄像头：2400万+1600万+200万像素</p>
                <p title="800万像素">前置摄像头：800万像素</p></div>
        </li>
        <li class="fore2">
            <i class="i-cpu"></i>
            <div class="detail">
                <p title="八核">核&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;数：八核</p>
                <p title="4×Cortex A73 2.2GHz +4*Cortex A53 1.7GHz">频&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;率：4×Cortex A73
                    2.2GHz +4*Cortex A53 1.7GHz</p></div>
        </li>
    </ul>
    <ul id="parameter-brand" class="p-parameter-list">
        <li title="华为（HUAWEI）">品牌： <a href="//list.jd.com/list.html?cat=9987,653,655&amp;tid=655&amp;ev=exbrand_8557"
                                      clstag="shangpin|keycount|product|pinpai_1" target="_blank">华为（HUAWEI）</a>
            <!--a href="#none" class="follow-brand btn-def" clstag='shangpin|keycount|product|guanzhupinpai'><b>&hearts;</b>关注 -->
        </li>
    </ul>
    <ul class="parameter2 p-parameter-list">
        <li title="华为麦芒 8">商品名称：华为麦芒 8</li>
        <li title="100003599639">商品编号：100003599639</li>
        <li title="359.00g">商品毛重：359.00g</li>
        <li title="中国大陆">商品产地：中国大陆</li>
        <li title="薄（7mm-8.5mm）">机身厚度：薄（7mm-8.5mm）</li>
    </ul>
    <p class="more-par">
        <a href="#product-detail" class="J-more-param">更多参数<s class="txt-arr">&gt;&gt;</s></a>
    </p>
</div>

</body>
</html>
