<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%

    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<head>
    <base href="<%=basePath%>">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="meta description">
    <title>商品详情</title>
    <!--=== Favicon ===-->
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/assets/img/favicon.ico" type="image/x-icon"/>
    <!--== Google Fonts ==-->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,400i,500,600,700" rel="stylesheet">
    <!--=== Bootstrap CSS ===-->
    <link href="${pageContext.request.contextPath}/assets/css/vendor/bootstrap.min.css" rel="stylesheet">
    <!--=== Font-Awesome CSS ===-->
    <link href="${pageContext.request.contextPath}/assets/css/vendor/font-awesome.css" rel="stylesheet">
    <!--=== Plugins CSS ===-->
    <link href="${pageContext.request.contextPath}/assets/css/plugins.css" rel="stylesheet">
    <!--=== Helper CSS ===-->
    <link href="${pageContext.request.contextPath}/assets/css/helper.min.css" rel="stylesheet">
    <!--=== Main Style CSS ===-->
    <link href="${pageContext.request.contextPath}/assets/css/style.css" rel="stylesheet">
    <!-- Modernizer JS -->
    <script src="${pageContext.request.contextPath}/assets/js/vendor/modernizr-2.8.3.min.js"></script>

    <script type="text/javascript" src="${pageContext.request.contextPath}/assets/js/discount/jquery.min.js"></script>

    <link href="${pageContext.request.contextPath}/assets/css/discount/style.css" rel="stylesheet" type="text/css"/>
    <style>
        /*提示框*/
        .tipsbox{
            position: fixed;
            top: 40%;
            left: 50%;
            z-index: 10000;
            min-width: 200px;
            min-height: 40px;
            max-width: 500px;
            border-radius: 5px;
            text-align: center;
            padding: 10px;
            color: white;
            background-color: red;
            opacity: 0.6;
            display: none;
        }

        .btn-cancelCollection{
            background-color: #f5740a;
            color: #fff;
        }
        .color-other li{
            height: 50px;
            display: inline-block;
            padding-right: 4px;
            border:1px solid blue;
            margin: 5px 5px;
        }
        .color-other .color-item img{
            max-width: 50px;
            max-height: 48px;
        }
    </style>
</head>
<body>
<%@include file="/view/front/head.jsp"%>



<div>
    <!--== Start Page Breadcrumb ==-->
    <div class="page-breadcrumb-wrap">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="page-breadcrumb">
                        <ul class="nav">
                            <li><a href="product/home">首页</a></li>
                            <li><a href="product/list">商品列表</a></li>
                            <li><a href="#" class="active">${product.name}</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--== End Page Breadcrumb ==-->

    <!--== Page Content Wrapper Start ==-->
    <div id="page-content-wrapper">
        <div class="container">
            <div class="row">
                <!-- Single Product Page Content Start -->
                <div class="col-lg-12">
                    <div class="single-product-page-content">
                        <div class="row">
                            <!-- Product Thumbnail Start 图片区 -->
                            <div class="col-lg-5">
                                <div class="product-thumbnail-wrap">
                                    <div class="product-thumb-carousel owl-carousel">
                                        <c:forEach items="${product.images}" var="img">
                                            <div class="single-thumb-item">
                                                <a href="javascript:void(0);"><img class="img-fluid" src="${img.url}" alt="图片" /></a>
                                            </div>
                                        </c:forEach>

                                        <div class="single-thumb-item">
                                            <a href="javascript:void(0);"><img class="img-fluid" src="assets/img/single-pro-1.jpg" alt="Product" /></a>
                                        </div>

                                        <div class="single-thumb-item">
                                            <a href="javascript:void(0);"><img class="img-fluid" src="assets/img/single-pro-2.jpg" alt="Product" /></a>
                                        </div>

                                        <div class="single-thumb-item">
                                            <a href="javascript:void(0);"><img class="img-fluid" src="assets/img/single-pro-3.jpg" alt="Product" /></a>
                                        </div>

                                        <div class="single-thumb-item">
                                            <a href="javascript:void(0);"><img class="img-fluid" src="assets/img/single-pro-4.jpg" alt="Product" /></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Product Thumbnail End 图片区-->

                            <!-- Product Details Start 信息区-->
                            <div class="col-lg-7">
                                <div class="product-details">
                                    <h2>${product.name}</h2>

                                    <%-- 星星 --%>
                                    <div class="rating">
                                        <c:forEach var="i" begin="1" end="${product.xing/2}" step="1">
                                            <i class="fa fa-star"></i>
                                        </c:forEach>
                                        <c:if test="${product.xing%2 == 1}">
                                            <i class="fa fa-star-half"></i>
                                        </c:if>
                                        <c:forEach var="j" begin="${product.xing/2+product.xing%2}" end="4" step="1">
                                            <i class="fa fa-star-o"></i>
                                        </c:forEach>
                                        <b> ( ${product.avgScore} ) </b>
                                    </div>

                                    <span class="price">￥${product.price}</span>

                                    <div class="product-info-stock-sku">
                                        <span class="product-stock-status text-success">库存: ${product.stock}</span>
                                        <span class="product-sku-status">上架时间：<strong> ${product.createTime}</strong></span>
                                    </div>

                                    <p class="products-desc">${product.depict}</p>

                                    <div class="shopping-option-item">
                                        <h4>选择颜色</h4>
                                        <ul class="color-other ">
                                            <li class="color-item"  style="border-color: red;color: red;">
                                                <a href="javascript:void(0)" class="color-hvr">
                                                    <img class="img-fluid" src="${(product.images)[0].url}" alt="" />
                                                    <span class="color-name">${product.color}</span>
                                                </a>
                                                <%-- 同种其他颜色的商品 --%>
                                            <c:if test="${product.otherColorProducts!=null}">
                                                <c:forEach items="${product.otherColorProducts}" var="otherPro">
                                                    <li class="color-item">
                                                        <a href="product/detail/${otherPro.productId}" class="color-hvr">
                                                            <img class="img-fluid" src="${(otherPro.images)[0].url}" alt="" />
                                                            <span class="color-name">${otherPro.color}</span>
                                                        </a>
                                                    </li>

                                                </c:forEach>
                                            </c:if>
                                        </ul>
                                    </div>

                                    <div class="product-quantity d-flex align-items-center">
                                        <div class="quantity-field">
                                            <label for="qty">数量</label>
                                            <input type="number" id="qty" min="1" max="100" value="1" />
                                        </div>

                                        <a href="javascript:addCart(${product.productId})" class="btn btn-cart-large"><i class="fa fa-shopping-cart"></i> 加入购物车</a>
                                    </div>

                                    <div class="product-btn-group">
                                        <c:choose>
                                            <c:when test="${product.isCollection>0}">
                                                <span productId="${product.productId}" class="btn btn-round btn-cart btn-cancelCollection"
                                                   title="取消收藏"><i class="fa fa-heart"></i></span>
                                            </c:when>
                                            <c:otherwise>
                                                <span productId="${product.productId}" class="btn btn-round btn-cart btn-collection"
                                                   title="加入收藏"><i class="fa fa-heart"></i></span>
                                            </c:otherwise>
                                        </c:choose>
                                        <%--<a href="compare.html" class="btn btn-round btn-cart" title="加入对比"><i class="fa fa-exchange"></i></a>--%>
<%--                                        <a href="single-product-gruop.html" class="btn btn-round btn-cart"><i class="fa fa-envelope-o"></i></a>--%>
                                    </div>
                                </div>
                            </div>
                            <!-- Product Details End 信息区-->
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <!-- Product Full Description Start -->
                                <div class="product-full-info-reviews">
                                    <!-- Single Product tab Menu -->
                                    <div class="alert alert-success" role="alert" id="successMsg"
                                         style="display: none;position: absolute;height: 65px;width: 600px;text-align: center;padding: 18px;font-size: 30px;margin: 0 auto;" ></div>
                                    <nav class="nav" id="nav-tab">
                                        <a class="active" id="description-tab" data-toggle="tab" href="#description">详情信息</a>
                                        <a id="reviews-tab" data-toggle="tab" href="#reviews">评论</a>
                                    </nav>
                                    <!-- Single Product tab Menu -->

                                    <!-- Single Product tab Content -->
                                    <div class="tab-content" id="nav-tabContent">
                                        <%-- 详情面板 --%>
                                        <div class="tab-pane fade show active" id="description">
                                            <div class="tab-panel-item tab-active">
                                                <div class="tab-item" style="grid-template-columns: 500px 500px;">
                                                    <c:if test="${coupons!=null}">
                                                    <c:forEach var="coupon" items="${coupons}">
                                                        <c:choose>
                                                            <c:when test="${user!=null && coupon.userId == user.userId}">
                                                                <div class="tab-item-list tab-item-list-default">
                                                                    <div class="aui-flex">
                                                                        <div class="aui-left-change">
                                                                            <h2>
                                                                                <em>￥</em>
                                                                                    ${coupon.preferentialSum}

                                                                            </h2>
                                                                            <p>
                                                                                <c:choose>
                                                                                    <c:when test="${coupon.scope!=0}">
                                                                                        满${coupon.minimumConsumptionSum}元可用
                                                                                    </c:when>
                                                                                    <c:otherwise>
                                                                                        特殊商品除外
                                                                                    </c:otherwise>
                                                                                </c:choose>
                                                                            </p>
                                                                        </div>
                                                                        <div class="aui-flex-box aui-flex-box-two">
                                                        <span class="aui-icon-used">
                                                            <img src="${pageContext.request.contextPath}/assets/img/discount/icon-used.png" alt="">
                                                        </span>
                                                                            <h3>
                                                                                <em>券票</em>
                                                                                    ${coupon.target}

                                                                            </h3>
                                                                            <div class="aui-flex aui-flex-bor-bom">
                                                                                <div class="aui-flex-box aui-flex-box-flow">${coupon.startTime}-${coupon.deadline}</div>
                                                                            </div>
                                                                            <div class="aui-flex aui-flex-show">
                                                                                <div class="aui-flex-box">详细信息</div>
                                                                                <div class="aui-show-btn">
                                                                                    <i class="icon fa fa-angle1-right" data-placement="top" data-toggle="popover" data-content="${coupon.detail}" ></i>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="aui-show-box">
                                                                        <p>默认隐藏内容</p>
                                                                    </div>
                                                                </div>

                                                            </c:when>
                                                            <c:otherwise>
                                                                <c:choose>
                                                                    <c:when test="${coupon.scope!=0}">
                                                                        <div class="tab-item-list">
                                                                            <div class="aui-flex">
                                                                                <div class="aui-left-change">
                                                                                    <h2 >
                                                                                        <em>￥</em>
                                                                                            ${coupon.preferentialSum}

                                                                                    </h2>
                                                                                    <p>满${coupon.minimumConsumptionSum}元可用</p>
                                                                                    <div class="aui-icon-circle"></div>
                                                                                </div>
                                                                                <div class="aui-flex-box aui-flex-box-two">
                                                                                    <h3>
                                                                                        <em>券票</em>
                                                                                            ${coupon.target}

                                                                                    </h3>
                                                                                    <div class="aui-flex aui-flex-bor-bom">
                                                                                        <div class="aui-flex-box aui-flex-box-flow">${coupon.startTime}-${coupon.deadline}</div>
                                                                                        <a onclick="getCoupon(${coupon.couponId})"  class="aui-btn-use" >立即领取</a>
                                                                                    </div>
                                                                                    <div class="aui-flex aui-flex-show">
                                                                                        <div class="aui-flex-box">详细信息</div>
                                                                                        <div class="aui-show-btn">
                                                                                            <i class="icon fa fa-angle1-right" data-placement="top" data-toggle="popover" data-content="${coupon.detail}"></i>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="aui-show-box">
                                                                                <p>默认隐藏内容</p>
                                                                            </div>
                                                                        </div>
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                        <div class="tab-item-list tab-item-list-green">
                                                                            <div class="aui-flex">
                                                                                <div class="aui-left-change">
                                                                                    <h2>
                                                                                        <em>￥</em>
                                                                                            ${coupon.preferentialSum}

                                                                                    </h2>
                                                                                    <p>特殊商品除外</p>
                                                                                    <div class="aui-icon-circle"></div>
                                                                                </div>
                                                                                <div class="aui-flex-box aui-flex-box-two">
                                                                                    <h3>
                                                                                        <em>运费券</em>
                                                                                            ${coupon.target}

                                                                                    </h3>
                                                                                    <div class="aui-flex aui-flex-bor-bom">
                                                                                        <div class="aui-flex-box aui-flex-box-flow">${coupon.startTime}-${coupon.deadline}</div>
                                                                                        <a onclick="getCoupon(${coupon.couponId})"  class="aui-btn-use" >立即领取</a>
                                                                                    </div>
                                                                                    <div class="aui-flex">
                                                                                        <div class="aui-flex-box">详细信息</div>
                                                                                        <div class="aui-show-btn">
                                                                                            <i class="icon fa fa-angle1-right" data-placement="top" data-toggle="popover" data-content="${coupon.detail}"></i>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="aui-show-box">
                                                                                <p>默认隐藏内容</p>
                                                                            </div>
                                                                        </div>
                                                                    </c:otherwise>
                                                                </c:choose>
                                                            </c:otherwise>
                                                        </c:choose>

                                                    </c:forEach>
                                                        <a href="coupon/showCoupons?productId=${product.productId}"  target="_blank" style="color: blue"> 点击查看更多优惠卷 >></a>
                                                    </c:if>
                                                </div>
                                            </div>
                                            <hr>
                                            ${product.detail}

                                            <p>de wicks moisture to keep you dry.Stay comfortable and stay in the race no matter what the weather's up to. The Bruno Compete Hoodie's water-repellent exterior shields you from the elements, while advanced fabric technology inside wicks moisture to keep you dry.Stay comfortable and stay in the race no matter what the weather's up to. The Bruno Compete Hoodie's water-repellent exterior shields you from the elements, while advanced fabric technology inside wicks moisture to keep you dry.</p>

                                            <ul>
                                                <li>Adipisicing elitEnim, laborum.</li>
                                                <li>Lorem ipsum dolor sit</li>
                                                <li>Dolorem molestiae quod voluptatem! Sint.</li>
                                                <li>Iure obcaecati odio pariatur quae saepe!</li>
                                            </ul>
                                        </div>
                                            <%-- 详情面板 --%>

                                            <%--评论面板--%>
                                        <div class="tab-pane fade" id="reviews">
                                            <div class="row">
                                                <div class="col-lg-9">
                                                    <div class="product-ratting-wrap">
                                                        <%-- 商品平均评分 --%>
                                                        <div class="pro-avg-ratting">
                                                            <h4>综合评分：<span style="font-size: 30px"> ${product.avgScore} </span></h4>
                                                            <span>共 <b> ${product.productComments.size()} </b> 条评论</span>
                                                        </div>
                                                        <div class="ratting-list">
                                                                <div class="sin-list float-left">
                                                                    <i class="fa fa-star"></i>
                                                                    <i class="fa fa-star"></i>
                                                                    <i class="fa fa-star"></i>
                                                                    <i class="fa fa-star"></i>
                                                                    <i class="fa fa-star"></i>
                                                                    <span>( ${product.productCommentsByScore.get('5')} )</span>
                                                                </div>
                                                                <div class="sin-list float-left">
                                                                    <i class="fa fa-star"></i>
                                                                    <i class="fa fa-star"></i>
                                                                    <i class="fa fa-star"></i>
                                                                    <i class="fa fa-star"></i>
                                                                    <i class="fa fa-star-o"></i>
                                                                    <span>( ${product.productCommentsByScore.get('4')} )</span>
                                                                </div>
                                                            <div class="sin-list float-left">
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star-o"></i>
                                                                <i class="fa fa-star-o"></i>
                                                                <span>( ${product.productCommentsByScore.get('3')} )</span>
                                                            </div>
                                                            <div class="sin-list float-left">
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star-o"></i>
                                                                <i class="fa fa-star-o"></i>
                                                                <i class="fa fa-star-o"></i>
                                                                <span>( ${product.productCommentsByScore.get('2')} )</span>
                                                            </div>
                                                            <div class="sin-list float-left">
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star-o"></i>
                                                                <i class="fa fa-star-o"></i>
                                                                <i class="fa fa-star-o"></i>
                                                                <i class="fa fa-star-o"></i>
                                                                <span>( ${product.productCommentsByScore.get('1')} )</span>
                                                            </div>
                                                        </div>
                                                        <div class="rattings-wrapper">
                                                            <c:if test="${product.productComments != null}">
                                                                <c:forEach items="${product.productComments}" var="comment">
                                                                    <div class="sin-rattings">
                                                                        <div class="ratting-author">
                                                                            <h3>${comment.userId}</h3>
                                                                            <div class="ratting-star">
                                                                                <c:forEach var="i" begin="1" end="${comment.score/2}" step="1">
                                                                                    <i class="fa fa-star"></i>
                                                                                </c:forEach>
                                                                                <c:forEach var="j" begin="${comment.score/2}" end="4" step="1">
                                                                                    <i class="fa fa-star-o"></i>
                                                                                </c:forEach>
                                                                                <span>( ${comment.score/2} )</span>
                                                                            </div>
                                                                        </div>
                                                                        <p>${comment.context}</p>
                                                                        <p>${comment.createTime}</p>
                                                                    </div>
                                                                </c:forEach>
                                                            </c:if>
                                                            <div class="sin-rattings">
                                                                <div class="ratting-author">
                                                                    <h3>鸡你太美</h3>
                                                                    <div class="ratting-star">
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <span>(5)</span>
                                                                    </div>
                                                                </div>
                                                                <p>enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia res eos qui ratione voluptatem sequi Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci veli</p>
                                                            </div>

                                                            <div class="sin-rattings">
                                                                <div class="ratting-author">
                                                                    <h3>卡布奇洛</h3>
                                                                    <div class="ratting-star">
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <span>(5)</span>
                                                                    </div>
                                                                </div>
                                                                <p>enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia res eos qui ratione voluptatem sequi Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci veli</p>
                                                            </div>

                                                            <div class="sin-rattings">
                                                                <div class="ratting-author">
                                                                    <h3>练习时长。。。</h3>
                                                                    <div class="ratting-star">
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <span>(5)</span>
                                                                    </div>
                                                                </div>
                                                                <p>enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia res eos qui ratione voluptatem sequi Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci veli</p>
                                                            </div>

                                                        </div>

                                                        <%-- 评论表单 --%>
                                                        <%--<div class="ratting-form-wrapper">
                                                            <h3>Add your Comments</h3>
                                                            <form action="#" method="post">
                                                                <div class="ratting-form row">
                                                                    <div class="col-12 mb-4">
                                                                        <h5>Rating:</h5>
                                                                        <div class="ratting-star fix">
                                                                            <i class="fa fa-star-o"></i>
                                                                            <i class="fa fa-star-o"></i>
                                                                            <i class="fa fa-star-o"></i>
                                                                            <i class="fa fa-star-o"></i>
                                                                            <i class="fa fa-star-o"></i>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6 col-12 mb-4">
                                                                        <label for="name">Name:</label>
                                                                        <input id="name" placeholder="Name" type="text">
                                                                    </div>
                                                                    <div class="col-md-6 col-12 mb-4">
                                                                        <label for="email">Email:</label>
                                                                        <input id="email" placeholder="Email" type="text">
                                                                    </div>
                                                                    <div class="col-12 mb-4">
                                                                        <label for="your-review">Your Review:</label>
                                                                        <textarea name="review" id="your-review" placeholder="Write a review"></textarea>
                                                                    </div>
                                                                    <div class="col-12">
                                                                        <input value="add review" type="submit">
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>--%>
                                                        <%-- 评论表单 --%>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <%--评论面板--%>
                                    </div>
                                    <!-- Single Product tab Content -->
                                </div>
                                <!-- Product Full Description End -->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Single Product Page Content End -->
            </div>
        </div>
    </div>
    <!--== Page Content Wrapper End ==-->
</div>

<script>
    $(function (){
        $("[data-toggle='popover']").popover();
    });

    function showSuccess(msg) {
        $("#successMsg").html(msg);
        $("#successMsg").fadeIn();
        setTimeout(function () {
            $("#successMsg").fadeOut();
        },800);
    }

    function getCoupon(couponId) {
        console.log(${productId});
        if (${user==null}) {
            //console.log("ojbk!");
            showSuccess("客官别急，请登录取后再领取哦！");
        }else {
            window.location.href = "/coupon/getCoupon/"+couponId+"/${product.productId}?page=detail";

        }
    }
</script>

<%@include file="/view/front/foot.jsp"%>
</body>
<!--=======================Javascript============================-->
<!--=== Jquery Min Js ===-->
<script src="${pageContext.request.contextPath}/assets/js/vendor/jquery-3.3.1.min.js"></script>
<!--=== Jquery Migrate Min Js ===-->
<script src="${pageContext.request.contextPath}/assets/js/vendor/jquery-migrate-1.4.1.min.js"></script>
<!--=== Popper Min Js ===-->
<script src="${pageContext.request.contextPath}/assets/js/vendor/popper.min.js"></script>
<!--=== Bootstrap Min Js ===-->
<script src="${pageContext.request.contextPath}/assets/js/vendor/bootstrap.min.js"></script>
<!--=== Ajax Mail Js ===-->
<script src="${pageContext.request.contextPath}/assets/js/ajax-mail.js"></script>
<!--=== Plugins Min Js ===-->
<script src="${pageContext.request.contextPath}/assets/js/plugins.js"></script>
<!--=== Active Js ===-->
<script src="${pageContext.request.contextPath}/assets/js/active.js"></script>
</html>
<script>
    console.log("用户：${user.userId}");

    $(function () {
        $(".btn-collection").on("click",function () {
            if($(this).hasClass("btn-collection")){
                collection($(this));
            }else{
                cancelCollection($(this));
            }

        });
        $(".btn-cancelCollection").on("click",function () {
            if($(this).hasClass("btn-collection")){
                collection($(this));
            }else{
                cancelCollection($(this));
            }

        });

        function collection(btn_col){
            let productId = btn_col.attr("productId");
            $.ajax({
                url: "product/collection?productId=" + productId,
                type: "post",
                async: false,
                success: function (res) {
                    if (res == 1) {
                        btn_col.removeClass("btn-collection");
                        // console.log("allClass:"+btn_col[0].className);
                        // console.log("allClass:"+btn_col.attr("class"));
                        // console.log("btn-collection");
                        btn_col.attr("title","取消收藏");
                        // console.log($(this).attr("title"));
                        btn_col.addClass("btn-cancelCollection");
                        // console.log("btn-cancelCollection");

                        tipsbox("收藏成功","blue",2000);
                    } else if (res == 200) {
                        tipsbox("登录后才能收藏哦~~", "blue", 2000);
                    } else {
                        tipsbox("收藏失败", "red", 2000);
                    }
                },
                error: function () {
                    tipsbox("网络异常，收藏失败", "red", 2000);
                }
            });
        }

        function cancelCollection(btn_col){
            let productId = btn_col.attr("productId");
            $.ajax({
                url: "product/cancelCollection?productId=" + productId,
                type: "post",
                async: false,
                success: function (res) {
                    if (res == 1) {
                        btn_col.removeClass("btn-cancelCollection");
                        // console.log("btn-cancelCollection");
                        btn_col.attr("title","加入收藏");
                        // console.log($(this).attr("title"));
                        btn_col.addClass("btn-collection");
                        // console.log("btn-collection");

                        tipsbox("已取消收藏","blue",2000);
                    } else if (res == 200) {
                        tipsbox("登录后才能取消收藏哦~~", "blue", 2000);
                    } else {
                        tipsbox("取消收藏失败", "red", 2000);
                    }
                },
                error: function () {
                    tipsbox("网络异常，取消收藏失败", "red", 2000);
                }
            });
        }




    });
    function addCart(productId) {
        $.ajax({
            url: "cart/add?id=" + productId,
            dataType: "json",
            success: function (res) {
                if (res.code == '0') {
                    tipsbox("加入购物车成功", "blue", 2000);
                    window.location.reload();
                } else {
                    tipsbox("加入购物车失败", "red", 2000);
                }
            },
            error: function () {
                tipsbox("网络异常,加入购物车失败", "red", 2000);
            }
        });
    }


    function tipsbox(msg, bgcolor, time) {
        // alert("收藏成功");
        $("body").append("<div id='tips' class='tipsbox'>" + msg + "</div>");
        var tips = $("#tips");
        tips.css("margin-left", "-" + tips.width() / 2 + "px");
        if (bgcolor != null) {
            tips.css("background-color", bgcolor);
        }
        if (time == null) {
            time = 3000;
        }
        tips.fadeIn("slow");
        setTimeout(function () {
            tips.fadeOut();
            setTimeout(function () {
                tips.remove();
            }, 1000);
        }, time)

    }
</script>