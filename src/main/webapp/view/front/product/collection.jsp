<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%

    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<head>
    <base href="<%=basePath%>">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="meta description">
    <title>我的收藏</title>
    <!--=== Favicon ===-->
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/assets/img/favicon.ico" type="image/x-icon"/>
    <!--== Google Fonts ==-->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,400i,500,600,700" rel="stylesheet">
    <!--=== Bootstrap CSS ===-->
    <link href="${pageContext.request.contextPath}/assets/css/vendor/bootstrap.min.css" rel="stylesheet">
    <!--=== Font-Awesome CSS ===-->
    <link href="${pageContext.request.contextPath}/assets/css/vendor/font-awesome.css" rel="stylesheet">
    <!--=== Plugins CSS ===-->
    <link href="${pageContext.request.contextPath}/assets/css/plugins.css" rel="stylesheet">
    <!--=== Helper CSS ===-->
    <link href="${pageContext.request.contextPath}/assets/css/helper.min.css" rel="stylesheet">
    <!--=== Main Style CSS ===-->
    <link href="${pageContext.request.contextPath}/assets/css/style.css" rel="stylesheet">
    <!-- Modernizer JS -->
    <script src="${pageContext.request.contextPath}/assets/js/vendor/modernizr-2.8.3.min.js"></script>

    <style>
        /*提示框*/
        .tipsbox{
            position: fixed;
            top: 40%;
            left: 50%;
            z-index: 10000;
            min-width: 200px;
            min-height: 40px;
            max-width: 500px;
            border-radius: 5px;
            text-align: center;
            padding: 10px;
            color: white;
            background-color: red;
            opacity: 0.6;
            display: none;
        }

        .btn-cancelCollection{
            background-color: #f5740a;
            color: #fff;
        }
        .color-other li{
            height: 50px;
            display: inline-block;
            padding-right: 4px;
            border:1px solid blue;
            margin: 5px 5px;
        }
        .color-other .color-item img{
            max-width: 50px;
            max-height: 48px;
        }
    </style>
</head>
<body>
<%@include file="/view/front/head.jsp"%>



<div>
    <!--== Start Page 导航条 ==-->
    <div class="page-breadcrumb-wrap">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="page-breadcrumb">
                        <ul class="nav">
                            <li><a href="product/home">首页</a></li>
                            <li><a href="product/list" class="active">商品列表</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--== End Page 导航条 ==-->

    <!--== Page Content Wrapper Start ==-->
    <div id="page-content-wrapper">
        <div class="container">
            <div class="row">
                <!-- Start Shop Page Content -->
                <div class="col-lg-10 m-auto">
                    <div class="shop-page-content-wrap">
                        <div class="products-settings-option d-block d-md-flex">
                            <div class="product-cong-left d-flex align-items-center">
                                <ul class="product-view d-flex align-items-center">
                                    <li class="list current"><i class="fa fa-list-ul"></i></li>
                                    <li class="box-gird"><i class="fa fa-th"></i></li>
                                </ul>
                                <span class="show-items">
                                    <c:if test="${page != null}">
                                        您总共收藏了 <span style="color: red;"> ${page.totalRows} </span> 件商品
                                    </c:if>
                                </span>
                            </div>

                            <%--<div class="product-sort_by d-flex align-items-center mt-3 mt-md-0">
                                <label for="sort">排序: </label>
                                <select name="sort" id="sort">
                                    <option value="0" <c:if test="${queryProduct.orderBy==0}"> selected="selected" </c:if>  >综合</option>
                                    <option value="1" <c:if test="${queryProduct.orderBy==1}"> selected="selected" </c:if> >名字, A to Z</option>
                                    <option value="2" <c:if test="${queryProduct.orderBy==2}"> selected="selected" </c:if> >名字, Z to A</option>
                                    <option value="3" <c:if test="${queryProduct.orderBy==3}"> selected="selected" </c:if> >价格， 低到高</option>
                                    <option value="4" <c:if test="${queryProduct.orderBy==4}"> selected="selected" </c:if> >价格， 高到低</option>
                                </select>
                            </div>--%>
                        </div>

                        <div class="shop-page-products-wrap">
                            <div class="products-wrapper products-list-view">
                                <div class="row">
                                    <c:if test="${collections!=null}">
                                    <c:forEach var="product" items="${collections}">
                                        <div class="col-lg-4 col-sm-6">
                                            <div class="single-product-item">
                                                <figure class="product-thumb">
                                                    <c:choose>
                                                        <c:when test="${(product.images)[0].url!=null}">
                                                            <a href="product/detail/${product.productId}" target="_blank">
                                                                <img src="${(product.images)[0].url}" alt="Product"></a>
                                                        </c:when>
                                                        <c:otherwise>
                                                            <a href="product/detail/${product.productId}">
                                                                <img src="assets/img/product-1.jpg" alt="Product"></a>
                                                        </c:otherwise>
                                                    </c:choose>
                                                    <a href="product/detail/${product.productId}" target="_blank" class="btn btn-round btn-cart" title="查看详情"><i
                                                            class="fa fa-eye"></i></a>
                                                </figure>
                                                <div class="product-details">
                                                    <h2 class="product-title">
                                                        <a href="product/detail/${product.productId}" target="_blank">${product.name}</a>
                                                    </h2>
                                                    <div class="rating">
                                                        <c:if test="${product.xing!=null}">
                                                            <c:forEach var="i" begin="1" end="${product.xing/2}" step="1">
                                                                <i class="fa fa-star"></i>
                                                            </c:forEach>
                                                            <c:if test="${product.xing%2 == 1}">
                                                                <i class="fa fa-star-half"></i>
                                                            </c:if>
                                                            <c:forEach var="j" begin="${product.xing/2+product.xing%2}" end="4" step="1">
                                                                <i class="fa fa-star-o"></i>
                                                            </c:forEach>
                                                        </c:if>
                                                    </div>
                                                    <span class="product-price">￥${product.price}</span>

                                                    <p class="pro-desc">${product.depict}</p>

                                                    <div class="product-meta">
                                                        <a href="javascript:addCart(${product.productId})" class="btn btn-round btn-cart" title="加入购物车"><i
                                                                class="fa fa-shopping-cart"></i></a>
                                                        <c:choose>
                                                            <c:when test="${product.isCollection==1}">
                                                                <a href="javascript:void(0);" productId="${product.productId}" class="btn btn-round btn-cart btn-cancelCollection"
                                                                   title="取消收藏"><i class="fa fa-heart"></i></a>
                                                            </c:when>
                                                            <c:otherwise>
                                                                <a href="javascript:void(0);" productId="${product.productId}" class="btn btn-round btn-cart btn-collection"
                                                                   title="加入收藏"><i class="fa fa-heart"></i></a>
                                                            </c:otherwise>
                                                        </c:choose>
                                                            <%--<a href="compare.html" class="btn btn-round btn-cart"
                                                               title="加入对比"><i class="fa fa-exchange"></i></a>--%>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </c:forEach>
                                    </c:if>
                                </div>
                            </div>
                        </div>

                        <%--<div class="products-settings-option d-block d-md-flex">
                            <nav class="page-pagination">
                                <ul class="pagination">
                                    <c:choose>
                                        <c:when test="${page.curPage>1}">
                                            <li><a href="javascript:toPage(${page.curPage-1})" aria-label="上一页">&laquo;</a></li>
                                        </c:when>
                                        <c:otherwise>
                                            <li><a href="javascript:void(0);" aria-label="Previous" role="button" class="disabled" aria-disabled="true">&laquo;</a></li>
                                        </c:otherwise>
                                    </c:choose>

                                    <c:if test="${page.curPage-3>0}">
                                        <li><a href="javascript:toPage(${page.curPage-3})">${page.curPage-3}</a></li>
                                    </c:if>

                                    <c:if test="${page.curPage-2>0}">
                                        <li><a href="javascript:toPage(${page.curPage-2})">${page.curPage-2}</a></li>
                                    </c:if>

                                    <c:if test="${page.curPage-1>0}">
                                        <li><a href="javascript:toPage(${page.curPage-1})">${page.curPage-1}</a></li>
                                    </c:if>

                                    <li><a class="current" href="javascript:void(0);">${page.curPage}</a></li>

                                    <c:if test="${page.curPage+1<=page.totalPages}">
                                        <li><a href="javascript:toPage(${page.curPage+1})">${page.curPage+1}</a></li>
                                    </c:if>

                                    <c:if test="${page.curPage+2<=page.totalPages}">
                                        <li><a href="javascript:toPage(${page.curPage+2})">${page.curPage+2}</a></li>
                                    </c:if>

                                    <c:if test="${page.curPage+3<=page.totalPages}">
                                        <li><a href="javascript:toPage(${page.curPage+3})">${page.curPage+3}</a></li>
                                    </c:if>

                                    <c:choose>
                                        <c:when test="${page.curPage<page.totalPages}">
                                            <li><a href="javascript:toPage(${page.curPage+1})" aria-label="下一页">&raquo;</a></li>
                                        </c:when>
                                        <c:otherwise>
                                            <li><a href="javascript:void(0);" aria-label="下一页" class="disabled" aria-disabled="true">&raquo;</a></li>
                                        </c:otherwise>
                                    </c:choose>
                                </ul>
                            </nav>

                            <div class="product-per-page d-flex align-items-center mt-3 mt-md-0">
                                <label for="show-per-page">Show Per Page</label>
                                <select name="sort" id="show-per-page">
                                    <c:forEach var="i" begin="1" end="${page.totalPages}" step="1">
                                        <option value="${i}"><a href="">${i}</a></option>
                                    </c:forEach>
                                </select>
                            </div>
                        </div>--%>
                    </div>
                </div>
                <!-- End Shop Page Content -->
            </div>
        </div>
    </div>
    <!--== Page Content Wrapper End ==-->
</div>



<%@include file="/view/front/foot.jsp"%>
</body>
<!--=======================Javascript============================-->
<!--=== Jquery Min Js ===-->
<script src="${pageContext.request.contextPath}/assets/js/vendor/jquery-3.3.1.min.js"></script>
<!--=== Jquery Migrate Min Js ===-->
<script src="${pageContext.request.contextPath}/assets/js/vendor/jquery-migrate-1.4.1.min.js"></script>
<!--=== Popper Min Js ===-->
<script src="${pageContext.request.contextPath}/assets/js/vendor/popper.min.js"></script>
<!--=== Bootstrap Min Js ===-->
<script src="${pageContext.request.contextPath}/assets/js/vendor/bootstrap.min.js"></script>
<!--=== Ajax Mail Js ===-->
<script src="${pageContext.request.contextPath}/assets/js/ajax-mail.js"></script>
<!--=== Plugins Min Js ===-->
<script src="${pageContext.request.contextPath}/assets/js/plugins.js"></script>
<!--=== Active Js ===-->
<script src="${pageContext.request.contextPath}/assets/js/active.js"></script>
</html>
<script>
    $(function () {
        $(".btn-collection").on("click",function () {
            if($(this).hasClass("btn-collection")){
                collection($(this));
            }else{
                cancelCollection($(this));
            }

        });
        $(".btn-cancelCollection").on("click",function () {
            if($(this).hasClass("btn-collection")){
                collection($(this));
            }else{
                cancelCollection($(this));
            }

        });

        function collection(btn_col){
            let productId = btn_col.attr("productId");
            $.ajax({
                url: "product/collection?productId=" + productId,
                type: "post",
                async: false,
                success: function (res) {
                    if (res == 1) {
                        btn_col.removeClass("btn-collection");
                        // console.log("allClass:"+btn_col[0].className);
                        // console.log("allClass:"+btn_col.attr("class"));
                        // console.log("btn-collection");
                        btn_col.attr("title","取消收藏");
                        // console.log($(this).attr("title"));
                        btn_col.addClass("btn-cancelCollection");
                        // console.log("btn-cancelCollection");

                        tipsbox("收藏成功","blue",2000);
                    } else if (res == 200) {
                        tipsbox("登录后才能收藏哦~~", "blue", 2000);
                    } else {
                        tipsbox("收藏失败", "red", 2000);
                    }
                },
                error: function () {
                    tipsbox("网络异常，收藏失败", "red", 2000);
                }
            });
        }

        function cancelCollection(btn_col){
            let productId = btn_col.attr("productId");
            $.ajax({
                url: "product/cancelCollection?productId=" + productId,
                type: "post",
                async: false,
                success: function (res) {
                    if (res == 1) {
                        btn_col.removeClass("btn-cancelCollection");
                        // console.log("btn-cancelCollection");
                        btn_col.attr("title","加入收藏");
                        // console.log($(this).attr("title"));
                        btn_col.addClass("btn-collection");
                        // console.log("btn-collection");

                        tipsbox("已取消收藏","blue",2000);
                    } else if (res == 200) {
                        tipsbox("登录后才能取消收藏哦~~", "blue", 2000);
                    } else {
                        tipsbox("取消收藏失败", "red", 2000);
                    }
                },
                error: function () {
                    tipsbox("网络异常，取消收藏失败", "red", 2000);
                }
            });
        }




    });
    function addCart(productId) {
        $.ajax({
            url: "cart/add?id=" + productId,
            dataType: "json",
            success: function (res) {
                if (res.code == '0') {
                    tipsbox("加入购物车成功", "blue", 2000);
                    window.location.reload();
                } else {
                    tipsbox("加入购物车失败", "red", 2000);
                }
            },
            error: function () {
                tipsbox("网络异常,加入购物车失败", "red", 2000);
            }
        });
    }


    function tipsbox(msg, bgcolor, time) {
        // alert("收藏成功");
        $("body").append("<div id='tips' class='tipsbox'>" + msg + "</div>");
        var tips = $("#tips");
        tips.css("margin-left", "-" + tips.width() / 2 + "px");
        if (bgcolor != null) {
            tips.css("background-color", bgcolor);
        }
        if (time == null) {
            time = 3000;
        }
        tips.fadeIn("slow");
        setTimeout(function () {
            tips.fadeOut("slow");
            setTimeout(function () {
                tips.remove();
            }, 1000);
        }, time)

    }
</script>