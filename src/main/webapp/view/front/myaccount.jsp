<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="meta description">
    <title>我的账户</title>
    <!--=== Favicon ===-->
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/assets/img/favicon.ico" type="image/x-icon"/>
    <!--== Google Fonts ==-->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,400i,500,600,700" rel="stylesheet">
    <!--=== Bootstrap CSS ===-->
    <link href="${pageContext.request.contextPath}/assets/css/vendor/bootstrap.min.css" rel="stylesheet">
    <!--=== Font-Awesome CSS ===-->
    <link href="${pageContext.request.contextPath}/assets/css/vendor/font-awesome.css" rel="stylesheet">
    <!--=== Plugins CSS ===-->
    <link href="${pageContext.request.contextPath}/assets/css/plugins.css" rel="stylesheet">
    <!--=== Helper CSS ===-->
    <link href="${pageContext.request.contextPath}/assets/css/helper.min.css" rel="stylesheet">
    <!--=== Main Style CSS ===-->
    <link href="${pageContext.request.contextPath}/assets/css/style.css" rel="stylesheet">
    <!-- Modernizer JS -->
    <script src="${pageContext.request.contextPath}/assets/js/vendor/modernizr-2.8.3.min.js"></script>
</head>
<body>
<%@include file="head.jsp"%>



<!--== Page Content Wrapper Start ==-->
<div id="page-content-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <!-- My Account Page Start -->
                <div class="myaccount-page-wrapper">
                    <!-- My Account Tab Menu Start -->
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="myaccount-tab-menu nav" role="tablist">
                                <a href="#dashboad" class="active" data-toggle="tab"><i class="fa fa-dashboard"></i>
                                    个人中心</a>

                                <a href="#orders"  data-toggle="tab"><i class="fa fa-cart-arrow-down"></i> 我的订单</a>

                                <a href="${pageContext.request.contextPath}/coupon/queryCoupons/${sessionScope.user.userId}" target="_blank"><i class="fa fa-ticket"></i> 我的优惠卷</a>

                                <a href="#payment-method" data-toggle="tab"><i class="fa fa-credit-card"></i>支付方式</a>

                                <a href="#address-edit" data-toggle="tab"><i class="fa fa-map-marker"></i> 联系地址</a>

                                <a href="#account-info" data-toggle="tab"><i class="fa fa-user"></i>更改密码</a>

                                <a href="${pageContext.request.contextPath}/login/logout
"><i class="fa fa-sign-out"></i> 注销</a>
                            </div>
                        </div>
                        <!-- My Account Tab Menu End -->

                        <!-- My Account Tab Content Start -->
                        <div class="col-lg-9 mt-15 mt-lg-0">
                            <div class="tab-content" id="myaccountContent">
                                <!-- Single Tab Content Start -->
                                <div class="tab-pane fade show active" id="dashboad" role="tabpanel">
                                    <div class="myaccount-content">
                                        <h3>个人中心</h3>

                                        <div class="welcome">
                                            <p>您好, <strong>${sessionScope.user.nickName}</strong> (如果不是 <strong>${sessionScope.user.nickName} !</strong><a href="${pageContext.request.contextPath}/login/logout" class="logout"> 注销</a>)</p>
                                        </div>

                                        <p class="mb-0">从你的个人中心. 你可以查看你的历史订单, 管理你的收货地址和修改你的密码.</p>
                                    </div>
                                </div>
                                <!-- Single Tab Content End -->

                                <!-- Single Tab Content Start -->
                                <div class="tab-pane fade" id="orders" role="tabpanel">
                                    <div class="myaccount-content">
                                        <h3>我的订单</h3>

                                        <div class="myaccount-table table-responsive text-center">
                                            <table class="table table-bordered">
                                                <thead class="thead-light">
                                                <tr>
                                                    <th>订单号</th>
                                                    <th>订单日期</th>
                                                    <th>订单状态</th>
                                                    <th>总价</th>
                                                    <th>操作</th>
                                                </tr>
                                                </thead>

                                                <tbody>
                                                <c:forEach var="p" items="${requestScope.orderlist}">
                                                    <tr>
                                                        <td>${p.orderId}</td>
                                                        <td>${p.createtime1}</td>
                                                        <td>${p.status}</td>
                                                        <td>￥${p.total}</td>
                                                        <td><a href="${pageContext.request.contextPath}/order/detail?oid=${p.orderId}" target="_blank" class="btn">查看</a></td>
                                                    </tr>
                                                </c:forEach>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <!-- Single Tab Content End -->

                                <!-- Single Tab Content Start -->
                                <div class="tab-pane fade" id="payment-method" role="tabpanel">
                                    <div class="myaccount-content">
                                        <h3>支付方式</h3>

                                        <p class="saved-message">该功能开发中，敬请期待...</p>
                                    </div>
                                </div>
                                <!-- Single Tab Content End -->

                                <!-- Single Tab Content Start -->
                                <div class="tab-pane fade" id="address-edit" role="tabpanel">
                                    <div class="myaccount-content">
                                        <h3>收货地址</h3>

                                        <address>
                                            <p><strong>四川宜宾</strong></p>
                                            <p>四川轻化工大学 <br>
                                                A11-301工作室</p>
                                            <p>Mobile: (123) 456-7890</p>
                                        </address>

                                        <a href="#" class="btn d-inline-block"><i class="fa fa-edit"></i> 编辑地址</a>
                                    </div>
                                </div>
                                <!-- Single Tab Content End -->

                                <!-- Single Tab Content Start -->
                                <div class="tab-pane fade" id="account-info" role="tabpanel">
                                    <div class="myaccount-content">
                                        <h3>修改密码</h3>

                                        <div class="account-details-form">
                                            <form action="#">
                                                <fieldset>
                                                    <legend>修改密码</legend>
                                                    <div class="single-input-item">
                                                        <label for="current-pwd" class="required">当前密码</label>
                                                        <input type="password" id="current-pwd" placeholder="当前密码" />
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-lg-6">
                                                            <div class="single-input-item">
                                                                <label for="new-pwd" class="required">新密码</label>
                                                                <input type="password" id="new-pwd" placeholder="新密码" />
                                                            </div>
                                                        </div>

                                                        <div class="col-lg-6">
                                                            <div class="single-input-item">
                                                                <label for="confirm-pwd" class="required">确认新密码</label>
                                                                <input type="password" id="confirm-pwd" placeholder="确认新密码" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </fieldset>

                                                <div class="single-input-item">
                                                    <button class="btn">修改</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <!-- Single Tab Content End -->
                            </div>
                        </div>
                        <!-- My Account Tab Content End -->
                    </div>
                </div>
                <!-- My Account Page End -->
            </div>
        </div>
    </div>
</div>
<!--== Page Content Wrapper End ==-->



<%@include file="foot.jsp"%>
</body>
<!--=======================Javascript============================-->
<!--=== Jquery Min Js ===-->
<script src="${pageContext.request.contextPath}/assets/js/vendor/jquery-3.3.1.min.js"></script>
<!--=== Jquery Migrate Min Js ===-->
<script src="${pageContext.request.contextPath}/assets/js/vendor/jquery-migrate-1.4.1.min.js"></script>
<!--=== Popper Min Js ===-->
<script src="${pageContext.request.contextPath}/assets/js/vendor/popper.min.js"></script>
<!--=== Bootstrap Min Js ===-->
<script src="${pageContext.request.contextPath}/assets/js/vendor/bootstrap.min.js"></script>
<!--=== Ajax Mail Js ===-->
<script src="${pageContext.request.contextPath}/assets/js/ajax-mail.js"></script>
<!--=== Plugins Min Js ===-->
<script src="${pageContext.request.contextPath}/assets/js/plugins.js"></script>
<!--=== Active Js ===-->
<script src="${pageContext.request.contextPath}/assets/js/active.js"></script>
<script>
    $(document).ready(function () {
        //服务器对前端JS的调用
        ${runJs};
    });
</script>
</html>
