<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page pageEncoding="UTF-8"%>
<!--头部内容-->
<!--== Start Header Section ==-->

<header id="header-area">
    <!-- Start PreHeader Area -->
    <div class="preheader-area">
        <div class="container">
            <div class="row">
                <div class="col-md-5 col-lg-6">
                    <div class="preheader-contact-info d-flex align-items-center justify-content-between justify-content-md-start">
                        <div class="single-contact-info">
                            <span class="contact-icon">
                                <i class="fa fa-envelope"></i>
                            </span>
                            <a href="mailto:your@example.com" class="contact-text">
                                <strong>邮箱:</strong> your@email.here
                            </a>
                        </div>
                        <div class="single-contact-info">
                            <span class="contact-icon">
                                <i class="fa fa-phone"></i>
                            </span>
                            <a href="" class="contact-text">
                                <strong>电话:</strong> (+1) 866-540-3229
                            </a>
                        </div>
                    </div>
                </div>

                <div class="col-md-7 col-lg-6 mt-xs-10">
                    <div class="site-setting-menu">
                        <ul class="nav justify-content-center justify-content-md-end">
                            <c:choose >
                                <c:when test="${user!=null}">
                                    <li><strong><font color="#f0f8ff"> 您好，${sessionScope.user.nickName}</font></strong></li>
                                    <li><a href="${pageContext.request.contextPath}/user/account">我的账户</a></li>
                                    <li><a href="${pageContext.request.contextPath}/user/account?to=order">我的订单</a></li>
                                    <li><a href="${pageContext.request.contextPath}/login/logout">注销</a></li>
                                </c:when>
                                <c:otherwise>
                                    <li><a href="${pageContext.request.contextPath}/login/login">我的账户</a></li>
                                    <li><a href="${pageContext.request.contextPath}/login/login">我的订单</a></li>
                                    <li><a href="${pageContext.request.contextPath}/login/login">登录</a></li>
                                </c:otherwise>
                            </c:choose>


                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End PreHeader Area -->

    <!-- Start Header Middle Area -->
    <div class="header-middle-area">
        <div class="container">
            <div class="row">
                <!-- Logo Area Start -->
                <div class="col-4 col-md-2 col-xl-3 m-auto text-center text-lg-left">
                    <a href="${pageContext.request.contextPath}/product/home" class="logo-wrap d-block">
                        <img src="${pageContext.request.contextPath}/assets/img/logo.png" alt="Logo" class="img-fluid"/>
                    </a>
                </div>
                <!-- Logo Area End -->

                <!-- Search Box Area Start -->
                <div class="col-8 col-md-7 col- m-auto ">
                    <div class="search-box-wrap">
                        <form class="search-form d-flex" action="${pageContext.request.contextPath}/product/list" method="get">
                            <input type="search" name="name" placeholder="输入并搜索你想要的商品" value="<c:if test="${queryProduct!=null}">${queryProduct.name}</c:if>"/>
                            <button class="btn btn-search"><img src="${pageContext.request.contextPath}/assets/img/icons/icon-search.png"
                                                                alt="搜索"/>
                            </button>
                        </form>
                    </div>
                </div>
                <!-- Search Box Area End -->

                <!-- Mini Cart Area Start -->
                <div class="col-12 col-md-3 col-xl-2 m-auto text-center text-lg-right mt-xs-15">
                    <div class="minicart-wrapper">
                        <button class="btn btn-minicart">购物车<sup class="cart-count">${fn:length(sessionScope.cartlist)} </sup></button>
                        <div class="minicart-content">
                            <div class="mini-cart-body">
                                <c:forEach var="p" items="${sessionScope.cartlist}">
                                    <!-- Single Cart Item Start -->
                                    <div class="single-cart-item d-flex">
                                        <figure class="product-thumb">
                                            <a href="${pageContext.request.contextPath}/product/detail/${p.product_id}"><img src="${p.thumbnail}"
                                                                               alt="Products"/></a>
                                        </figure>

                                        <div class="product-details">
                                            <h2><a href="${pageContext.request.contextPath}/product/detail/${p.product_id}"> ${p.product_name}</a></h2>
                                            <div class="cal d-flex align-items-center">
                                                <span class="quantity">${p.count}</span>
                                                <span class="multiplication">X</span>
                                                <span class="price">${p.price}</span>
                                            </div>
                                        </div>

                                        <a href="javascript:void(0)" class="remove-icon" onclick="deleteById('${p.product_id}')"><i class="fa fa-trash-o"></i></a>
                                    </div>
                                    <!-- Single Cart Item End -->
                                </c:forEach>


                            <script>
                                function deleteById(id) {
                                    $.ajax({
                                        //请求方式
                                        type : "POST",
                                        //请求地址
                                        url : "${pageContext.request.contextPath}/cart/delete",
                                        //数据，json字符串
                                        data : {id:id},
                                        dataType:"json",
                                        //请求成功
                                        success : function(result) {
                                            if (result.code == 0){
                                                $.ajax({
                                                    //请求方式
                                                    type : "GET",
                                                    //请求地址
                                                    url : "${pageContext.request.contextPath}/cart/list"
                                                });
                                                window.location.reload();
                                            } else {
                                                console.log(result.msg);
                                            }
                                        },
                                        //请求失败，包含具体的错误信息
                                        error : function(e){
                                            console.log(e.status);
                                            console.log(e.responseText);
                                        }
                                    });


                                }
                            </script>

                            </div>
                            <div class="mini-cart-footer">
                                <a href="${pageContext.request.contextPath}/cart/list" class="btn">结算</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Mini Cart Area End -->
            </div>
        </div>
    </div>
    <!-- End Header Middle Area -->

    <!-- Start Main Menu Area -->
    <div class="navigation-area" id="fixheader">
        <div class="container">
            <div class="row">
                <!-- Categories List Start -->
                <div class="col-10 col-lg-3">
                    <div class="categories-list-wrap">
                        <button class="btn btn-category d-none d-lg-inline-block"><i class="fa fa-bars"></i> 类别
                        </button>
                        <ul class="category-list-menu">
                            <li class="category-item-parent dropdown-show">
                                <a href="#" class="category-item arrow-toggle">
                                    <img src="${pageContext.request.contextPath}/assets/img/icons/desktop.png" alt="Computer"/>
                                    <span>电脑</span>
                                </a>
                                <ul class="mega-menu-wrap dropdown-nav">
                                    <li class="mega-menu-item"><a href="#" class="mega-item-title">平板电脑</a>
                                        <ul>
                                            <li><a href="#"> 平板电脑 价格</a></li>
                                            <li><a href="#"> 平板电脑 内存</a></li>
                                            <li><a href="#"> 平板电脑 只读储存器</a></li>
                                            <li><a href="#"> 平板电脑 固寸</a></li>
                                        </ul>
                                    </li>

                                    <li class="mega-menu-item"><a href="#" class="mega-item-title">主机电脑</a>
                                        <ul>
                                            <li><a href="#">主机电脑 价格</a></li>
                                            <li><a href="#">主机电脑 内存</a></li>
                                            <li><a href="#">主机电脑 只读储存器</a></li>
                                            <li><a href="#">主机电脑 固寸</a></li>
                                        </ul>
                                    </li>

                                    <li class="mega-menu-item"><a href="#" class="mega-item-title">笔记本电脑</a>
                                        <ul>
                                            <li><a href="#">笔记本电脑 价格</a></li>
                                            <li><a href="#">笔记本电脑 内存</a></li>
                                            <li><a href="#">笔记本电脑 只读储存器</a></li>
                                            <li><a href="#">笔记本电脑 固寸</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>

                            <li class="category-item-parent dropdown-show">
                                <a href="#" class="category-item arrow-toggle">
                                    <img src="${pageContext.request.contextPath}/assets/img/icons/mobile.png" alt="Mobile"/>
                                    <span>手机</span>
                                </a>
                                <ul class="mega-menu-wrap dropdown-nav">
                                    <li class="mega-menu-item"><a href="#" class="mega-item-title">小米</a>
                                        <ul>
                                            <li><a href="#">三星 S6</a></li>
                                            <li><a href="#">三星 S7</a></li>
                                            <li><a href="#">三星 Prime</a></li>
                                            <li><a href="#">三星 A6</a></li>
                                        </ul>
                                    </li>

                                    <li class="mega-menu-item"><a href="#" class="mega-item-title">小米</a>
                                        <ul>
                                            <li><a href="#">小米 A2 Pro</a></li>
                                            <li><a href="#">小米 MIX 2</a></li>
                                            <li><a href="#">小米 F1</a></li>
                                            <li><a href="#">小米 Prime</a></li>
                                        </ul>
                                    </li>

                                    <li class="mega-menu-item"><a href="#" class="mega-item-title">iphone</a>
                                        <ul>
                                            <li><a href="#">iphone 6</a></li>
                                            <li><a href="#">iphone 7</a></li>
                                            <li><a href="#">iphone 8</a></li>
                                            <li><a href="#">iphone XR</a></li>
                                        </ul>
                                    </li>

                                    <li class="mega-menu-item"><a href="#" class="mega-item-title">诺基亚</a>
                                        <ul>
                                            <li><a href="#">诺基亚 820</a></li>
                                            <li><a href="#">诺基亚 888 Pro</a></li>
                                            <li><a href="#">诺基亚 81 Sirocco</a></li>
                                            <li><a href="#">诺基亚 Lamia 1520</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li class="category-item-parent">
                                <a href="#" class="category-item">
                                    <img src="${pageContext.request.contextPath}/assets/img/icons/camera.png" alt="Camera"/>
                                    <span>照相机</span>
                                </a>
                            </li>
                            <li class="category-item-parent">
                                <a href="#" class="category-item">
                                    <img src="${pageContext.request.contextPath}/assets/img/icons/fan.png" alt="Camera"/>
                                    <span>电器设备</span>
                                </a>
                            </li>
                            <li class="category-item-parent">
                                <a href="#" class="category-item">
                                    <img src="${pageContext.request.contextPath}/assets/img/icons/games.png" alt="Camera"/>
                                    <span>游戏设备</span>
                                </a>
                            </li>
                            <li class="category-item-parent">
                                <a href="#" class="category-item">
                                    <img src="${pageContext.request.contextPath}/assets/img/icons/tv.png" alt="Camera"/>
                                    <span>娱乐设施</span>
                                </a>
                            </li>
                            <li class="category-item-parent">
                                <a href="#" class="category-item">
                                    <img src="${pageContext.request.contextPath}/assets/img/icons/desktop.png" alt="Computer"/>
                                    <span>待添加</span>
                                </a>
                            </li>
                            <li class="category-item-parent hidden">
                                <a href="#" class="category-item">
                                    <img src="${pageContext.request.contextPath}/assets/img/icons/mobile.png" alt="Mobile"/>
                                    <span>待添加</span>
                                </a>
                            </li>
                            <li class="category-item-parent hidden">
                                <a href="#" class="category-item">
                                    <img src="${pageContext.request.contextPath}/assets/img/icons/camera.png" alt="Camera"/>
                                    <span>待添加</span>
                                </a>
                            </li>
                            <li class="category-item-parent hidden">
                                <a href="#" class="category-item">
                                    <img src="${pageContext.request.contextPath}/assets/img/icons/fan.png" alt="Camera"/>
                                    <span>待添加</span>
                                </a>
                            </li>
                            <li class="category-item-parent hidden">
                                <a href="#" class="category-item">
                                    <img src="${pageContext.request.contextPath}/assets/img/icons/games.png" alt="Camera"/>
                                    <span>待添加</span>
                                </a>
                            </li>
                            <li class="category-item-parent">
                                <a href="#" class="category-item btn-more">更多种类</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- Categories List End -->

                <!-- Main Menu Start -->
                <div class="col-2 col-lg-9 d-none d-lg-block">
                    <div class="main-menu-wrap">
                        <nav class="mainmenu">
                            <ul class="main-navbar clearfix">
                                <li class="dropdown-show"><a href="index.html" class="arrow-toggle">主页</a>
                                    <ul class="dropdown-nav sub-dropdown">
                                        <li><a href="index.html">主页布局 1</a></li>
                                        <li><a href="index2.html">主页布局 2</a></li>
                                        <li><a href="index3.html">主页布局 3</a></li>
                                        <li><a href="index4.html">主页布局 4</a></li>
                                    </ul>
                                </li>
                                <li><a href="about.html">关于我们</a></li>
                                <li class="dropdown-show"><a href="#" class="arrow-toggle">商店</a>
                                    <ul class="mega-menu-wrap dropdown-nav">
                                        <li class="mega-menu-item"><a href="shop.html" class="mega-item-title">全部商品</a>
                                            <ul>
                                                <li><a href="shop-3-grid.html">全部商品排列方式 1</a></li>
                                                <li><a href="shop-4-grid.html">全部商品排列方式 2</a></li>
                                                <li><a href="shop.html">全部商品排列方式 3</a></li>
                                                <li><a href="shop-list.html">全部商品排列方式 4</a></li>
                                                <li><a href="shop-right-sidebar.html">全部商品排列方式 5</a></li>
                                                <li><a href="shop-list-left-sidebar.html">全部商品排列方式 6</a></li>
                                                <li><a href="shop-list-right-sidebar.html">全部商品排列方式 7</a></li>
                                            </ul>
                                        </li>

                                        <li class="mega-menu-item"><a href="single-product.html" class="mega-item-title">单种风格商品</a>
                                            <ul>
                                                <li><a href="single-product-carousel.html">单种风格商品排列方式 1</a></li>
                                                <li><a href="single-product-sticky-left.html">单种风格商品排列方式 2</a></li>
                                                <li><a href="single-product-sticky-right.html">单种风格商品排列方式 3</a></li>
                                                <li><a href="single-product-gallery-left.html">单种风格商品排列方式 4</a></li>
                                                <li><a href="single-product-gallery-right.html">单种风格商品排列方式 5</a></li>
                                                <li><a href="single-product-tab-style-top.html">单种风格商品排列方式 6</a></li>
                                                <li><a href="single-product-tab-style-left.html">单种风格商品排列方式 7</a></li>
                                                <li><a href="single-product-tab-style-right.html">单种风格商品排列方式 8</a></li>
                                            </ul>
                                        </li>

                                        <li class="mega-menu-item"><a href="single-product.html" class="mega-item-title">单个商品</a>
                                            <ul>
                                                <li><a href="single-product.html">单个商品排列方式 1</a></li>
                                                <li><a href="single-product-sale.html">单个商品排列方式 2</a></li>
                                                <li><a href="single-product-group.html">单个商品排列方式 3</a></li>
                                                <li><a href="single-product-normal.html">单个商品排列方式 4</a></li>
                                                <li><a href="single-product-affiliate.html">单个商品排列方式 5</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>

                                <li class="dropdown-show"><a href="#" class="arrow-toggle">其他页面</a>
                                    <ul class="dropdown-nav">
                                        <li><a href="cart.html">购物车</a></li>
                                        <li><a href="checkout.html">结账</a></li>
                                        <li><a href="compare.html">商品对比</a></li>
                                        <li><a href="wishlist.html">愿望清单</a></li>
                                        <li><a href="login-register.html">登录或注册</a></li>
                                        <li><a href="my-account.html">我的账户</a></li>
                                        <li><a href="404.html">404 Error</a></li>
                                    </ul>
                                </li>

                                <li><a href="contact.html">联系我们</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <!-- Main Menu End -->
            </div>
        </div>
    </div>
    <!-- End Main Menu Area -->

</header>
<!--== End Header Section ==-->